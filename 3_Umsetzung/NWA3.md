# NWA3 - Netzwerktechnologien und Netzwerkarchitekturen 3

 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 3

## Fahrplan
| 8  | 4  | Labor 1: Multiarea OSPF                     |                |                                                             |
|----|----|---------------------------------------------|----------------|-------------------------------------------------------------|
| 4  | 2  | Grundlagen - Cisco - IPv6                                                  | c), d)        | Cisco Packet Tracer, Cisco IOS   |
| 12 | 6  | Labor 1: IPv6                                                              | c), d)        | GNS3, MikroTik, Cisco, WireShark |
| 8  | 4  | Labor 1: VRRP - Router redundanz im Layer 2 | g)             | GNS3, MikroTik, Cisco, WireShark                            |
| 12 | 6  | Labor 2: Klassisches Campus LAN Design      | a), c), d), g) | GNS3, MikroTik, Cisco, WireShark                            |
| 8  | 4  | Labor 3: VPN                                | e)             | GNS3, MikroTik, Cisco, WireShark, WireGuard, IPSEC, OpenVPN |
| 8  | 4  | Troubleshooting                             | b), c), d), e) | GNS3, MikroTik, Cisco, WireShark                            |
| **36** | **18** | **Total Lektionen**                             |                                                      |

## Lektionen

* Präsenz: 20
* Virtuell: 20
* Selbststudium: 20

## Voraussetzungen

NWA2

## Dispensation

Cisco CCNA-Zertifizierung

## Technologien

 - [GNS3 Labore TbzCloud](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3)

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

Layer 2, Switching, Layer 3 Routing, IPV4, IPV6 TCP, UDP, VLAN, 802.1Q, STP, RSTP, MSTP, DHCP, DNS, NTP, ICMP, ICMPV6, MNDP, CDP, LLDP, SSTP, Wireguard, OpenVPN, SNMP, Firewall, NAT, NAT-P, Cisco, MikroTik, Debian

## Lerninhalte
 - Dynamic Routing, OSPF
 - Routing Metriken
 - Neighbor Discovery Protokolle (MNDP, CDP, LLDP)
 - VPN Technologien (IPsec, SSTP, Wireguard, OpenVPN) und Architekturen (Site-To-Site, Roadwarrior)

## Übungen und Praxis
 - Statisch konfiguriertes Routing durch dynamisches Routing ersetzen
 - OSPF Protokoll Beobachtungen und Fehlerbehebung
 - Bestehende VPN Konfiguration analysieren und erweitern

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Laborübungen

## Lehrmittel

Repository

