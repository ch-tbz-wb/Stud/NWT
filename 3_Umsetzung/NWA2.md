# NWA2 - Netzwerktechnologien und Netzwerkarchitekturen 2

 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 2

## Fahrplan
| Lektionen | Selbststudium | Inhalt                                                                     | Kompetenzband | Tools                            |
|-----------|---------------|----------------------------------------------------------------------------|---------------|----------------------------------|
| 4         | 2             | Labor 1: Routing Repetition                                                | d), f)        | GNS3, MikroTik, Cisco, WireShark |
| 4         | 2             | Grundlagen - Cisco - Spanning-Tree + VLAN                                  | b)            | Cisco Packet Tracer, Cisco IOS   |
| 8         | 4             | Labor 2: Spanning-Tree + VLAN                                              | b)            | GNS3, MikroTik, Cisco, WireShark |
| 12        | 6             | Labor 3: Single Area OSPF - Vom Statischen Routing zum dynamischen Routing | d)            | GNS3, MikroTik, Cisco, WireShark |
| 4         | 2             | Cisco OSPF Basics                                                          | d)            | Cisco Packet Tracer, Cisco IOS   |
| 4         | 2             | Grundlagen - Cisco - IPv6                                                  | c), d)        | Cisco Packet Tracer, Cisco IOS   |
| 12        | 6             | Labor 3: IPv6                                                              | c), d)        | GNS3, MikroTik, Cisco, WireShark |
| **44**        | **22**            | **Total Lektionen**                                                            |

## Lektionen

* Präsenz: 20
* Virtuell: 20
* Selbststudium: 20

## Voraussetzungen

NWA1

## Dispensation

Cisco CCNA-Zertifizierung

## Technologien

 - [GNS3 Labore TbzCloud](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3)

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

Layer 2, Switching, Layer 3 Routing, IPV4, IPV6 TCP, UDP, VLAN, 802.1Q, STP, RSTP, MSTP, DHCP, DNS, NTP, ICMP, ICMPV6, MNDP, CDP, LLDP, SSTP, Wireguard, OpenVPN, SNMP, Firewall, NAT, NAT-P, Cisco, MikroTik, Debian

## Lerninhalte
 - Netzwerkarchitekturen (Campus LAN Design)
 - VLAN (Trunk ports, 802.1Q)
 - Spanning Tree Protokolle (RSTP, MSTP)
 - QoS 
 - Troubleshooting

## Übungen und Praxis
 - Bestehende IP-Firewall-Konfiguration analysieren und erweitern
 - Dokumentation eines bestehenden Netzwerkes
 - Systematische Ermittlung und Behebung von Störungen im Netzwerk
 - Bestehende IPv4/IPv6 Konfiguration analysieren, optimieren und entwickelte Lösung umsetzen
 - Layer 2 Konfigurationsfehler ermitteln und beheben
 - Netzwerkmonitoring Tools installieren und konfigurieren
 - Lasttests durchführen und Verhalten von QoS Einstellungen beobachten
 - Netzwerkkonfiguration automatisieren mit Python


## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Laborübungen

## Lehrmittel

Repository
