# NWA1 - Netzwerktechnologien und Netzwerkarchitekturen 1

 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 1

# Fahrplan
| Lektionen | Selbststudium | Inhalt                                                        | Kompetenzband | Tools                             |
|-----------|---------------|---------------------------------------------------------------|---------------|-----------------------------------|
| 4         | 2             | Grundlagen MikroTik, Zertifierzung MikroTik , Einführung GNS3  | c)            | GNS3, MikroTik, WinBox, Wireshark |
| 8         | 4             | Labor 1: Grundlagen MikroTik, Bridging, DNS, DHCP             | b), g)        | GNS3, MikroTik, WinBox, Wireshark |
| 8         | 4             | Labor 2: Routing & Firewall                                   | d), f)        | GNS3, MikroTik, WinBox, Wireshark, IP-Firewall |
| 8         | 4             | Labor 3: Wireless & QoS                                       | e)            | GNS3, MikroTik, WinBox, Wireshark, WLAN |
| 8         | 4             | Labor 4: PPP & VPN                                            | f)            | GNS3, MikroTik, WinBox, Wireshark, PPPoE, WireShark, SSTP |
| 4         | 2             | Abschluss & Zertifizierungsprüfung                            | h)            | GNS3, MikroTik, WinBox, Wireshark |
| **40**    | **20**        | **Total Lektionen**                                               |               |                                   |



## Lektionen

* Präsenz: 20
* Virtuell: 20
* Selbststudium: 20

## Voraussetzungen

ICT-Module 117, 123, 129, 145 aus der Grundbildung

## Dispensation

Cisco CCNA-Zertifizierung

## Technologien

 - [GNS3 Labore TbzCloud](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3)

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

Layer 2, Switching, Layer 3 Routing, IPV4, IPV6 TCP, UDP, VLAN, 802.1Q, STP, RSTP, MSTP, DHCP, DNS, NTP, ICMP, ICMPV6, MNDP, CDP, LLDP, SSTP, Wireguard, OpenVPN, SNMP, Firewall, NAT, NAT-P, Cisco, MikroTik, Debian

## Lerninhalte
 - Switching (MAC Adressen, MAC Tabelle)
 - Netzwerk Architekturen (Two-tief, three-tier, usw.)
 - Wichtigste Eigenschaften von TCP/UDP
 - IPv4/IPv6 Adressierung, Subnetting, Static Routing
 - Wichtigste Netzwerkprotokolle: DHCP, DNS, NTP, ICMP, ICMPv6
 - Network Address Translation NAT-P
 - IP-Firewall Grundlagen (MikroTik Firewall, iptables)

## Übungen und Praxis
 - Repetition Netzwerkgrundlagen
 - MikroTik MTCNA Zertifizierung
 - Betriebssysteme: Debian, Pfsense/Opnsense, Cisco CLI, MikroTik Cli


## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Laborübungen

## Lehrmittel

Repository
