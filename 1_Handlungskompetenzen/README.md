# Handlungskompetenzen

Die nachfolgende Kompetenzmatrix gilt für das gesamte NWA Modul (NWA1, NWA2, NWA3)

## Kompetenzmatrix

| **Kompetenzband**                                    | **RP**   | **HZ** | **Novizenkompetenz**                                           | **Fortgeschrittene Kompetenz**                                  | **Kompetenz professionellen Handelns**                          | **Kompetenzexpertise**                                         |
|------------------------------------------------------|----------|--------|----------------------------------------------------------------|-----------------------------------------------------------------|-----------------------------------------------------------------|----------------------------------------------------------------|
| **a) Analyse und Planung von Netzwerkarchitekturen**     | B12.1, B12.2 | 1      | Grundlegende Kenntnisse der Netzwerkarchitekturen (Two-Tier, Three-Tier, Campus LAN Design) und der zusammenhang zur Schichtenarchitektur. | Kann bestehende Netzwerke analysieren und einfache Architekturen planen, einschliesslich Campus LAN Design. | Plant und bewertet komplexe Netzwerkarchitekturen, definiert Soll-Architekturen für mehrere Standorte, nutzt Campus LAN Designs. | Führt detaillierte Analysen komplexer Netzwerke durch, identifiziert Schwachstellen und entwickelt Optimierungspläne, um Netzwerke schrittweise mit minimaler Downtime zu verbessern. |
| **b) Grundlagen der Ethernet-Technologien und Netzwerksegmentierung**| B12.2, B12.3 | 2      | Grundkenntnisse in Ethernet-Netzwerken, MAC-Adressen und -Tabellen sowie grundlegender Netzwerksegmentierung. | Kann VLANs konfigurieren, Spanning Tree Protokolle (STP, RSTP, MSTP) verstehen und in kleinen Netzwerken anwenden. | Optimiert VLAN-Strukturen und setzt RSTP oder MSTP zur Vermeidung von Schleifen ein, plant effiziente Layer-2-Segmente für grössere Netzwerke. Erkennt und behebt Layer-2-Fehler. | Identifiziert und behebt systematisch komplexe Fehler, die über mehrere Netzwerkkomponenten hinweg auftreten und eine gründliche Analyse und Fehlerbehebung erfordern. |
| **c) Subnetzplanung (IPv4/IPv6)**                       | B12.2, B12.3 | 3      | Grundlegendes Verständnis von IPv4, IPv6 und Subnetting. Kennt grundlegende Adressierungsregeln und Subnetzmasken. | Kann Subnetze planen und einfache IPv4- und IPv6-Adressierungsmodelle umsetzen. | Führt Subnetting für komplexe Netzwerke und mehrere Standorte aus, plant effiziente Adressierungsmodelle mit Berücksichtigung von Netzwerkgrössen und Anforderungen. | Entwickelt flexible Subnetzpläne, passt gewachsene Adressierungsschemata schrittweise an und verbessert sie, ohne eine vollständige Neustrukturierung. |
| **d) Statisches und dynamisches Routing**                     | B12.2, B12.3 | 4      | Grundkenntnisse von statischem Routing und den grundlegenden Konzepten des dynamischen Routings (z.B. OSPF). | Kann statisches Routing in einfachen Netzwerken implementieren und erste dynamische Routing-Protokolle wie OSPF anwenden. | Ersetzt statisches Routing durch dynamisches, kennt das Verhalten von Routing-Einträgen (z.B. Metriken) und passt diese zur Optimierung an. | Beurteilt, wann statisches oder dynamisches Routing eingesetzt werden sollte, und kann komplexe Routing-Probleme systematisch analysieren und beheben. |
| **e) Standortvernetzung und sicherer Zugriff auf interne Netzwerkressourcen mit VPN-Technologien** | B12.3, B12.4 | 5      | Grundkenntnisse über VPN-Konzepte und verschiedene VPN-Protokolle. | Kann einfache VPN-Verbindungen mit standardisierten Protokollen konfigurieren. | Konfiguriert und verwaltet VPN-Lösungen für die Vernetzung von Standorten (Site-to-Site) und ermöglicht den sicheren Zugriff auf interne Netzwerkressourcen. | Entwickelt skalierbare VPN-Architekturen, setzt automatisierte Bereitstellungsmethoden ein und implementiert passgenaue VPN-Lösungen für sichere Standortverbindungen und Ressourcenzugriff. |
| **f) Netzwerksicherheit und Firewall-Management**        | B12.3   | 6      | Kennt grundlegende Firewall-Konzepte (Herstellerspezifisch, Linux, Windows) und NAT. | Kann einfache NAT- und Firewall-Regeln konfigurieren und anwenden. Analysiert bestehende Firewall-Konfigurationen und optimiert diese. | Plant und konfiguriert umfassende Firewall- und NAT-P-Regeln, um Netzwerksicherheit zu gewährleisten. Nutzt Monitoring-Tools zur Wartung und führt gezielte Fehleranalysen durch. | Entwickelt massgeschneiderte Sicherheitslösungen, optimiert Firewalls für verschiedene Bedrohungsszenarien und führt kontinuierliche Überwachung, Automatisierung und Debugging durch. |
| **g) Netzwerkdienste und ihre Konfiguration**            | B12.3, B12.4 | 7      | Kennt den Einsatzzweck und die Funktion grundlegender Netzwerkdienste wie DHCP, DNS, NTP, VRRP, SNMP, sowie Protokolle für Nachbarschaftserkennung wie MNDP, CDP, LLDP. | Kann diese Dienste auf verschiedenen Plattformen konfigurieren und anpassen. | Versteht die Interaktion dieser Dienste im Netzwerk, konfiguriert sie effizient und optimiert deren Betrieb nach Bedarf. | Führt detaillierte Fehleranalysen bei komplexen Problemen mit Netzwerkdiensten und Protokollen zur Nachbarschaftserkennung durch und behebt diese gezielt. |
| **h) Konfigurationsmanagement und Netzwerkautomatisierung** | B12.4   | 8      | Kennt die Grundlagen eines Konfigurationsmanagementsystems. | Kann einfache Konfigurationsmanagementsysteme implementieren und grundlegende Netzwerkkonfigurationen automatisieren. | Entwickelt und verwaltet Konfigurationsmanagementsysteme, die mehrere Standorte und Netzwerkdienste umfassen (VPN, DNS, NTP). Kann grundlegende Automatisierungen zur Netzwerkkonfiguration implementieren. | Entwickelt und implementiert automatisierte Netzwerkverwaltungslösungen für mittelgrosse Infrastrukturen und vereinfacht die Netzwerkkonfiguration mithilfe von Automatisierungstools. |


## Modulspezifische Handlungskompetenzen

* B12: System- und Netzwerkarchitektur bestimmen
  * B12.1: Die bestehende Systemarchitektur beurteilen und weiterentwickeln 
  * B12.2: Die bestehende Netzwerk-Architektur analysieren, Umsetzungsvarianten definieren und eine Soll-Architektur entwickeln
  * B12.3: Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln
  * B12.4: Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen

## Allgemeine Handlungskompetenzen

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren

 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten
   * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren
   * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln
   * A3.5 Das eigene Denken, Fühlen und Handeln reflektieren und geeignete persönliche Entwicklungsmassnahmen definieren und umsetzen

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
