![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# NWA - Netzwerktechnologien und Netzwerkarchitekturen 1 bis 3

## Kurzbeschreibung des Moduls 


Das Modul Netzwerktechnologien und Netzwerkarchitekturen beinhaltet fortgeschrittene Konzepte und Technologien im Bereich des Netzwerkdesigns und der Implementierung von Netzwerktechnologien. Die Studenten lernen, wie sie komplexe Netzwerk-Topologien entwickeln und implementieren können, um die Anforderungen der Organisation zu erfüllen. Sie erlernen auch, wie sie verschiedene Netzwerkprotokolle und -technologien, einschließlich Layer2 und Layer3, VPN Technologien, konfigurieren und verwalten können. Das Modul beinhaltet auch die Verwendung von Automatisierungswerkzeugen und Skriptsprachen, um Netzwerke effizienter zu konfigurieren und zu verwalten. Durch Fallstudien und praktische Übungen erhalten die Schüler die Möglichkeit, ihre Kenntnisse in einem realistischen Umfeld anzuwenden.

 - Handlungskompetenzen aus B12 "System- und Netzwerkarchitektur" gemäss Rahmenlehrplan 
 - Inhaltliche Orientierung an Prüfungsthemen der MikroTik MTCNA und Cisco CCNA Zertifizierungen. 
 - Modul erstreckt sich drei Semester (NWA1 bis 3). 
 - Didaktische Umsetzung beinhaltet mehrheitlich praktische Übungen mit GNS3

## Angaben zum Transfer der erworbenen Kompetenzen 

GNS3 ermöglicht die Simulation einfacher und komplexer Netzwerke mit Betriebssystemen verschiedener Netzwerkgeräte-Hersteller. Da GNS3 alle simulierten Geräte in Form virtueller Maschinen ausführt, bietet es einen praxisnahen Hands-On-Ansatz. Durch praktische Übungen können Studierende ihre Kenntnisse in der Netzwerktechnik vertiefen.

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

Die Module NWA1, NWA2 und NWA3 bauen aufeinander auf. Vorangehende Module sind jeweils die Voraussetzung für die anschliessenden Module. Für NWA3 setze Kenntnisse aus den Modulen IAC (Programmierkenntnisse) und FAAS (Function as a Service) voraus. 

## Dispensation

Gemäss Reglement HF Lehrgang

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
