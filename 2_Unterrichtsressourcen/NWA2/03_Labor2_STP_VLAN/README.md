# Labor 2: Spanning-Tree und VLANs  <!-- omit in toc -->

# Lernziele
 - Unterschiede und Einsatzmöglichkeiten von Spanning-Tree-Protokollen verstehen.
 - Netzwerkkonfiguration analysieren und dokumentieren.
 - Root-Switches gezielt festlegen und Layer-2-Traffic optimieren.
 - Sicherheit durch BPDU Guard und PortFast gewährleisten.
 - VLANs und DHCP für IPv4-Adressen und Internetzugang konfigurieren.
 - Schutz vor Rogue-DHCP-Servern durch DHCP Guard sicherstellen.

# Wichtige Hinweise
 - Die Konsolenverbindung zu den Cisco IOS MultiLayer Switche bricht nach einer gewissen Zeit ab. Ein Neustart des Switches löst das Problem. **Jedoch gehen dann alle nicht gespeicherten Änderungen verloren.** Es empfiehlt sich die Konfiguration regelmässig abzuspeichern (Befehl: `write memory`)

# Vorbereitungen
![Labor Übersicht](media/Overview.png)
<br>*Abbildung 1: GNS3 Labor Ausgangslage*

Das Labor liegt als `.gns3project` Datei zum Import in GNS3 zur Verfügung. 

TBZ Studenten können auf https://cloud.edu.tbz.ch/gns3/ eine GNS3 Instanz beziehen. 

1. `.gns3project` [hier herunterladen](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/_read-only/Unterlagen/NWA/NWA2?csf=1&web=1&e=VDGqQ7). 
2. GNS3 starten 
3. Initiales Fenster schliessen.
4. `File` => `Import portable project` => `.gns3project` auswählen und Projekt importieren. 
5. Auf dem Router R1 folgende Start-Konfiguration einspielen:
```mikrotik
/interface bridge add name=bridge priority=0xA000 vlan-filtering=yes
/interface vlan add interface=bridge name=vlan10 vlan-id=10
/interface vlan add interface=bridge name=vlan20 vlan-id=20
/interface vlan add interface=bridge name=vlan30 vlan-id=30
/interface bridge port add bridge=bridge interface=ether2
/interface bridge port add bridge=bridge interface=ether3
/interface bridge vlan add bridge=bridge tagged=bridge,ether2,ether3 vlan-ids=10,20,30
/ip address add address=192.168.10.1/24 interface=vlan10 network=192.168.10.0
/ip dhcp-client add add-default-route=no interface=ether8 use-peer-dns=no use-peer-ntp=no
/ip dns set allow-remote-requests=yes
/ip firewall nat add action=masquerade chain=srcnat out-interface=ether1
/system identity set name=R1
```

# Spanning-Tree Protokolle - Übersicht
| Feature                | PVST (Per-VLAN Spanning Tree)             | Rapid-PVST (Rapid Per-VLAN Spanning Tree)     | MST (Multiple Spanning Tree)                 | RSTP (Rapid Spanning Tree Protocol)          |
|------------------------|------------------------------------------|----------------------------------------------|----------------------------------------------|----------------------------------------------|
| **Protocol Basis**     | Traditional **IEEE 802.1D STP**         | Enhanced **IEEE 802.1w RSTP**                | **IEEE 802.1s MSTP**                         | **IEEE 802.1w RSTP**                |
| **Convergence Speed**  | Slow (50 seconds default)               | Faster (near-instant, <10 seconds)           | Faster (depends on the topology)             | Faster (near-instant, <10 seconds)           |
| **VLAN Handling**      | Separate STP instance for each VLAN     | Separate RSTP instance for each VLAN         | Groups VLANs into instances (1 STP per group)| Single instance for all VLANs               |
| **Resource Usage**     | High (1 STP instance per VLAN)          | High (1 RSTP instance per VLAN)              | Low (fewer instances for multiple VLANs)     | Low (1 instance for the entire switch)       |
| **Scalability**        | Limited for large networks              | Limited for large networks                   | Highly scalable                               | Moderate (no VLAN differentiation)           |
| **Cisco Proprietary**  | Yes                                     | Yes                                          | No                                           | No                                           |

# Teil 1 - Netzwerk analysieren und dokumentieren
 
**💪 Challenge 1:** Verwende passende `show` Befehle auf den Cisco Switche und entsprechende print Befehle auf dem MikroTik Router R1 um mehr über die Konfiguration des Netzwerkes herauszufinden. Dokumentiere deine Ergebnisse direkt im GNS3 Labor. Beantworte die unten aufgeführten Fragen:
 - Welches Spanning-Tree Protokoll ist aktiv?
 - Welcher Switch ist ROOT?
 - Gibt es mehrere unterschiedliche ROOT Switche und wenn ja, weshalb?
 - Wie viele VLANs gibt es im Netzwerk?

# Teil 2 - Spanning-Tree Version
**💪 Challenge 2:** Passe die Spanning-Tree Version auf allen Geräten auf Rapid-PVST an.
 - Was ist der Vorteil dieser Spanning-Tree Version? 
 - Wie kann auf der Konsole die Spanning-Tree Version überprüft werden?

# Teil 2 - Sicherstellen das immer einer der Distribution Switche Root ist
In einem klassischen Campus-Design sollten die Distribution-Switches immer die Spanning Tree Root sein, da sie das zentrale Layer-2-Datenverkehrsmanagement übernehmen. Dies gewährleistet, dass der Datenverkehr über die leistungsstärkeren Verbindungen zwischen Access- und Distribution-Switches fliesst. Wenn ein anderer Switch, z. B. ein Access-Switch, zum Root wird, können suboptimale Pfade entstehen, was zu erhöhten Latenzen und potenziellen Engpässen führt.

**💪 Challenge 3:** Stelle sicher, dass immer einer der beiden Distribution Switche (SWD1 und SWD2) ROOT ist. 

# Teil 3 - BPUGuard & PortFast - Das böse Spiel des RogueSwitch stoppen!
**💪 Challenge 4:** Aktiviere [BPDU Guard](https://www.cisco.com/c/de_de/support/docs/lan-switching/spanning-tree-protocol/10586-65.html) und *PortFast* auf allen Ports, welche ausserhalb des Core Netzwerkes liegen, um zu verhindern, dass an access Ports angeschlossene Switche Teil des Spanning-Trees werden. Ist die Konfiguration erfolgreich wird auf dem SWA-1 der Port `Gi0/2` an dem der `RogueSwitch` angeschlossen ist auf err-disabled gehen. 

![Console output showing error disabled port](media/ErrDisabled.png)
<br/>*Abbildung 1: Showing error disabled port in console output*

# Teil 5 - Router on a Stick
**💪 Challenge 5:** Die Layer 2 Konfiguration auf dem Router `R1` ist mehrheitlich fertig, sowie die IPv4-Konfiguration für den Uplink. Konfiguriere alle VLANs so, dass die drei Endgeräte jeweils eine IPv4-Adresse aus einem anderen Subnetz per DHCP erhalten und Zugriff auf das Internet haben. 

# Teil 6 - DHCP Guard / DHCP Snooping
Eine weitere Attacke auf ein Netzwerk kann mit einem rogue DHCP-Server durchgeführt werden. Dafür haben viele Switch Hersteller entsprechende Massnahmen implementiert, damit nur der autorisierte DHCP-Server die Erlaubnis hat auf DHCP Requests zu antworten.

DHCP Snooping ist eine Sicherheitsfunktion, die unzuverlässige DHCP-Nachrichten filtert und eine DHCP-Bindungstabelle erstellt, um Netzwerkanomalien und Angriffe zu verhindern. Vertrauenswürdige Schnittstellen empfangen ausschließlich Nachrichten aus dem internen Netzwerk, während unzuverlässige Schnittstellen externe Quellen wie Endgeräte oder Netzwerke außerhalb der Firewall betreffen. Die Bindungstabelle enthält Informationen wie MAC- und IP-Adressen, Lease-Zeiten und Schnittstellendetails unzuverlässiger Verbindungen. Um DHCP Snooping korrekt zu nutzen, müssen DHCP-Server-Schnittstellen als vertrauenswürdig konfiguriert werden, da unzuverlässige Nachrichten nur an vertrauenswürdige Schnittstellen weitergeleitet werden.
Eine mögliche Störungsursache, welche durch DHCP Snooping verhindert werden kann, ist, wenn User "WLAN-Router" mit aktiven DHCP-Server mit einem Firmennetzwerk verbinden. Ohne DHCP-Snooping sind dann plötzlich zwei DHCP-Server in einem VLAN aktiv. Das führt dazu, dass Endgeräte ungültige IP-Adressen erhalten und Störungen melden. DHCP-Snooping kann dies verhindern, weil es alle ungültigen DHCP-Requests filtert. 

DHCP Snooping schützt das Netzwerk vor unerwünschten DHCP-Nachrichten und erstellt eine Bindungstabelle, während DHCP Guard gezielt unautorisierte DHCP-Server auf unzuverlässigen Ports blockiert.

**💪 Challenge 6:** Aktiviere mithilfe [dieser Anleitung von Cisco](https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst4500/12-2/31sga/configuration/guide/config/dhcp.html#wp1073418) DHCP Guard im Netzwerk. Gehe dabei in zwei Schritte vor. Aktiviere es zuerst standardmässig auf allen Ports. Versuche anschliessend mit einem der VPCS per DHCP eine IP-Adresse zu erhalten. Ist DHCP Guard korrekt aktiviert sollte das nicht mehr funktionieren. Konfiguriere anschliessend die vertrauenswürdigen Ports auf allen der vier Switches. Der Erfolg der Konfiguration ist dann eingetroffen, wenn alle drei PCs per DHCP wieder IPv4-Adressen erhalten.

# Nützliche Befehle

```
show running-config
show spanning-tree
show etherchannel summary
debug spanning-tree events
ip dhcp snooping
ip dhcp snooping vlan 
spanning-tree portfast bpduguard default
spanning-tree portfast default
show ip dhcp snooping 
```