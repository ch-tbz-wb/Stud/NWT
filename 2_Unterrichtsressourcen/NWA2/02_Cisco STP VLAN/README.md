# Cisco Packet Tracer Labore - STP / VLAN

# Theorie
## 📄 Text
 - Auf dem [Sharepoint NWA - CCNA](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/_read-only/Unterlagen/NWA?csf=1&web=1&e=8ZHhcx) stehen weitere Dokumente zur Verfügung:
   - *STP VLAN RSTP - Cisco Press CCNA 200-301.pdf*
 - [Mikrotik Help - Spanning Tree Protocol](https://help.mikrotik.com/docs/spaces/ROS/pages/21725254/Spanning+Tree+Protocol)
 - [MikroTik Help - VLAN](https://help.mikrotik.com/docs/spaces/ROS/pages/88014957/VLAN)
 - [Cisco - Troubleshoot LAN Switching Environments](https://www.cisco.com/c/en/us/support/docs/lan-switching/ethernet/12006-chapter22.html)
 - [Cisco - Understand Rapid Spanning Tree Protocol (802.1w)](https://www.cisco.com/c/en/us/support/docs/lan-switching/spanning-tree-protocol/24062-146.html)
 - [Cisco - Understand the Multiple Spanning Tree Protocol (802.1s)](https://www.cisco.com/c/en/us/support/docs/lan-switching/spanning-tree-protocol/24248-147.html)
 - [Cisco - Troubleshoot LAN Switching Environments](https://www.cisco.com/c/en/us/support/docs/lan-switching/ethernet/12006-chapter22.html)
 

## 🎥 Videos
 - [Youtube - CertBros - Cisco - Spanning Tree Protocol Explained | Step by Step ](https://www.youtube.com/watch?v=japdEY1UKe4)
 - [Youtube - CertBros - Cisco - PortFast + BPDU Guard | STP Optional Features ](https://www.youtube.com/watch?v=n3en4EcHms0)
 - [Youtube - CertBros - Cisco - VLANs Explained](https://www.youtube.com/watch?v=A9lMH0ye1HU)
 - [Youtube - CertBros - Cisco - How to Configure VLANs Cisco ](https://www.youtube.com/watch?v=hWp1CumPE9E)



# Praxis

Unter [Sharepoint NWA - CCNA](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/_read-only/Unterlagen/NWA?csf=1&web=1&e=8ZHhcx) sind folgende Labore abgelegt:

## `3.2.8 Packet Tracer - Investigate a VLAN Implementation`
Lockerer Einstieg ins VLAN Thema: VLAN Konfiguration an einem kleinen Netzwerk beobachten
 - Broadcast-Traffic in einer VLAN-Implementierung beobachten
 - Broadcast-Traffic ohne VLANs beobachten

## `3.3.12 Packet Tracer - VLAN Configuration`
Praktischer Einstieg ins VLAN-Thema: Standard-VLAN-Konfiguration überprüfen, VLANs konfigurieren und auf Ports zuweisen.
 - Standard-VLAN-Konfiguration überprüfen
 - VLANs konfigurieren
 - VLANs auf Ports zuweisen

## `3.6.1 Packet Tracer - Implement VLANs and Trunking`
Kombination aus VLAN und Trunking: VLANs konfigurieren, Ports zuweisen und Trunking einrichten.
 - VLANs konfigurieren
 - Ports zu VLANs zuweisen
 - Statisches Trunking konfigurieren
 - Dynamisches Trunking konfigurieren

## `4.2.7 Packet Tracer - Configure Router-on-a-Stick Inter-VLAN Routing`
VLAN und Routing: VLANs hinzufügen, Subinterfaces konfigurieren und Inter-VLAN-Routing testen.
 - VLANs zu einem Switch hinzufügen
 - Subinterfaces konfigurieren
 - Konnektivität mit Inter-VLAN-Routing testen

## `5.1.9 Packet Tracer - Investigate STP Loop Prevention`
Einführung ins Thema Spanning Tree: Portzustände beobachten und den Konvergenzprozess nachvollziehen.
 - Funktionsweise des Spanning Tree Protocols (STP) beschreiben
 - Erklären, wie das Spanning Tree Protocol Netzwerkschleifen verhindert und gleichzeitig Redundanz in geswitchten Netzwerken ermöglicht