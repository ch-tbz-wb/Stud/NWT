# Labor 1: IPv4 Routing  <!-- omit in toc -->

NWA1 deckte mit der MTCNA-Zertifizierung die wichtigsten Netzwerkgrundlagen ab. Neben den herstellerspezifischen Kenntnissen zu MikroTik umfasste dies allgemeine Themen wie DHCP, virtuelles Bridging, IPv4-Routing, Wireless, IPv4-Firewall, NAT, QoS, PPP, PPPoE, WireGuard sowie die Nutzung verschiedener Diagnose-Tools.

Dieses Labor dient einerseits als Vorbereitung für das Thema dynamische Routing Protokolle (OSPF), sowie als Repetition von IPv4-Routing und GNS3. 

# Lernziele
 - Grundlegendes Verständnis von IPv4, IPv6 und Subnetting. 
 - Grundkenntnisse von statischem Routing.
 - Kann grundlegende Netzwerkdienste auf verschiedenen Plattformen konfigurieren und anpassen.

# Hinweise zum Vorgehen
Diese Aufgabe ist als Troubleshooting Aufgabe zur Repetition von Kenntnissen aus vorherigen Laboren konzipiert.  Schritt-für-Schritt-Anleitungen stehen nicht zur Verfügung. Es wird erwartet, dass der/die Studierende fehlende Informationen selbstständig recherchiert. Links zu hilfreichen Ressourcen sind an in den jeweiligen Abschnitten oder am Ende des Dokumentes aufgeführt. 

# Weitere Hinweise zur Aufgabe
 - Login MikroTik: *admin* Passwort: *admin*

# Vorbereitungen
![Labor Übersicht](media/Overview.png)
<br>*Abbildung 1: GNS3 Labor Ausgangslage*

Das Labor liegt als `.gns3project` Datei zum Import in GNS3 zur Verfügung. 

TBZ Studenten können auf https://cloud.edu.tbz.ch/gns3/ eine GNS3 Instanz beziehen. 

1. `.gns3project` [hier herunterladen](media/NWA2_Routing.gns3project). 
2. GNS3 starten 
3. Initiales Fenster schliessen.
4. `File` => `Import portable project` => `.gns3project` auswählen 

# Teil 1 - Zugang erhalten und Überblick verschaffen

**💪 Challenge 1:**  Zugriff auf das Monitoring und die Konfiguration aller Geräte erhalten. 

## Tipps 
Eine der ersten Fragen (zumindest für Personen, die gleich loslegen wollen), die sich stellt:

Um was für Geräte handelt es sich und wie erhalte ich Zugriff darauf?

In diesem Labor werden Sie unter anderem [RouterOS](https://help.mikrotik.com/docs/) von MikroTik antreffen. 

Auf diese können Sie einerseits über die Serielle Konsole zugreifen oder über [WinBox RoMON](https://help.mikrotik.com/docs/spaces/ROS/pages/8978569/RoMON). Für Letzteres müssen zwei zusätzliche Geräte ins Labor einfügen, damit der Zugriff über die Winbox Applikation auf dem eigenen Laptop ins Labornetzwerk klappt: Die *Cloud* für den Zugriff auf `br0` bzw. das `192.168.23.0/24` Host-Subnetz und einen Switch. Folgende Verbindungen werden benötigt: `R6,ether2 <--> Switch <--> br0`. Der Router `R6` hat die statische IPv4-Adresse `192.168.23.16/24`. Anschliessend kann eine Verbindung zu `R6` aufgebaut werden (*Connect to RoMON*) und dann auf alle direkt miteinander verbundene MikroTik Geräte. 

# Teil 2 - Fehler finden und beheben

**💪 Challenge 2:** Finde und behebe vier Konfigurationsfehler und stelle sicher, dass nachfolgende Kriterien erfüllt werden:
 - Alle PCs erhalten per DHCP ihre Netzwerkkonfiguration zugewiesen
 - Alle Netzwerkgeräte können alle anderen Netzwerkgeräte anpingen
 - Alle Netzwerkgeräte haben Zugriff auf Ziele im Internet (z.B. 9.9.9.9)
 - Alle PCs können Ziele im Internet per FQDN anpingen (z.B. ping tbz.ch)
 - Die Verkabelung des Labors soll nicht verändert werden. Ziel ist es rein durch Konfigurationsanpassungen die Fehler zu beheben. 

## Tipps
 - Die IPv4-Konfiguration zuerst direkt in GNS3 zu dokumentieren hilft, sich einen Überblick zu verschaffen und offensichtliche Fehler zu finden.
 - Nicht zu weit suchen: Themenbereiche: IPv4 Adresse, Routing, Interfaces, DHCP. 
 - Diagnose-Tools verwenden: ping und traceroute sind deine besten Freunde. 

<details>
<summary>Tipp 1 - Challenge 2 - Spoiler Warning</summary>
<p>Wer sucht der findet. 😎</p>
</details>
<br/>

<details>
<summary>Tipp 2 - Challenge 2 - Spoiler Warning</summary>
<p>Du kannst das💪! Du brauchst keine Tipps. </p>
</details>
<br/>

<details>
<summary>Tipp 3 - Challenge 2 - Spoiler Warning</summary>
<p>Mindestens eine statische Route auf einem Router ist ungültig.</p>
</details>
<br/>
  
# Teil 3 - Netzwerk erweitern

**💪 Challenge 3:** Erweitere das Netzwerk um ein weiteres `/24` Subnetz an Router `R1`. Bei der Planung des IPv4-Subnetzes wurde berücksichtigt, dass an den Routern `R1`, `R4` , `R5` möglicherweise in Zukunft weitere Subnetze dazukommen. Konfiguriere analog zu den anderen `/24` Subnetzen auch einen DHCP Server und teste die Konfiguration mit einem VPC. Wenn dein VPC beliebige Ziele im Internet und alle anderen Host im Netzwerk anpingen kann, ist der Challenge erfolgreich gelöst. 

# Ressourcen
 - [MikroTik Dokumentation](https://help.mikrotik.com/docs/)
 - [MikroTik Youtube Channel](https://www.youtube.com/mikrotik)
 - [Basic Cisco und MikroTik Netzwerk Labor Beispiele](https://gitlab.com/alptbz/network-labs)
 - [NWA1 Labore](../../NWA1_MTCNA/)