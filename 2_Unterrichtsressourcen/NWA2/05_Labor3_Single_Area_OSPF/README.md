# OSPF - Open Shortest Path First   <!-- omit in toc -->

OSPF läuft auf jedem Router und sendet sowie empfängt OSPF-Nachrichten mit benachbarten Routern. Diese Nachrichten geben OSPF die Möglichkeit, Daten über das Netzwerk auszutauschen und IP-Version-4 (IPv4)-Routen zur IPv4-Routingtabelle jedes Routers hinzuzufügen und zu erlernen. OSPF wird für dynamisches Routing verwendet, was bedeutet, dass es Netzwerkänderungen (Ausfall eines Gerätes, Hinzukommen von neuen Netzwerken) automatisch erkennt und Routen entsprechend anpasst. Dies reduziert den Konfigurationsaufwand für Netzwerkadministratoren erheblich, da Anpassungen an den Routen nicht manuell für jeden Router vorgenommen werden müssen, sondern durch OSPF automatisch im Netzwerk propagiert werden.

Im Rahmen des Moduls schauen wir OSPF lediglich in der Version 2 (OSPFv2) an. 

## Inhaltsverzeichnis  <!-- omit in toc -->
- [1. Lernziele](#1-lernziele)
  - [1.1. Nützliche Ressourcen](#11-nützliche-ressourcen)
    - [1.1.1. 📄 Text](#111--text)
    - [1.1.2. 🎥 Videos](#112--videos)
- [2. Routing Protokoll Funktion](#2-routing-protokoll-funktion)
- [3. Interior and Exterior Routing Protocols](#3-interior-and-exterior-routing-protocols)
- [4. 🥼 Labor 1 - Single Area OSPF with MirkoTik](#4--labor-1---single-area-ospf-with-mirkotik)
  - [4.1. Vorbereitung](#41-vorbereitung)
  - [4.2. Starter Konfiguration für R1](#42-starter-konfiguration-für-r1)
  - [4.3. Starter Konfiguration für R2](#43-starter-konfiguration-für-r2)
  - [4.4. Starter Konfiguration für R3](#44-starter-konfiguration-für-r3)
  - [4.5. Repetition statisches Routing](#45-repetition-statisches-routing)
  - [4.6. Repetition NAT und DHCP](#46-repetition-nat-und-dhcp)
  - [4.7. Minimale OSPF Konfiguration](#47-minimale-ospf-konfiguration)
  - [4.8. 💡 Input - Routing Distanzen](#48--input---routing-distanzen)
  - [4.9. Default Route Propagation](#49-default-route-propagation)
  - [4.10. Minimale Sicherheit](#410-minimale-sicherheit)
  - [4.11. Backup Routen](#411-backup-routen)
  - [4.12. Use Case: Failover](#412-use-case-failover)
  - [4.13. Best Practice - OSPF ID](#413-best-practice---ospf-id)


## 1. Lernziele
 - Versteht die Grundlagen und den Aufbau des OSPF-Protokolls, einschliesslich seiner Funktionsweise und Hauptkomponenten.
 - Erkennt die Bedeutung und Funktion des Hello-Protokolls innerhalb des OSPF für die Etablierung und Aufrechterhaltung von Nachbarschaftsbeziehungen.
 - Kann die verschiedenen Arten von Link-State-Advertisements (LSAs) und deren Rolle bei der Datenbankverteilung im OSPF-Netzwerk erklären.
 - Versteht die OSPF-Netzwerkstruktur, einschliesslich der Definition von Bereichen (Areas), Router-Typen und der Konzept der Area Border Router (ABR) und Autonomous System Boundary Router (ASBR).
 - Kann die Methoden zur externen Routenverteilung und die Unterschiede zwischen Typ-1- und Typ-2-External-Routen erläutern.
 - Kann OSPFv2 auf Routern konfigurieren, einschliesslich der Planung erforderlicher IPv4-Subnetze.
 - Kann OSPF-Routen Verteilung (Propagation) steuern und OSPF-Filter konfigurieren, um den Netzwerkverkehr und die Routenverteilung gemäss den Sicherheits- und Leistungsanforderungen des Netzwerks zu steuern.

### 1.1. Nützliche Ressourcen

#### 1.1.1. 📄 Text
 - [MikroTik Help - OSPF](https://help.mikrotik.com/docs/display/ROS/OSPF)
 - [MikroTik Help - Route Selection and Filters](https://help.mikrotik.com/docs/display/ROS/Route+Selection+and+Filters)
 - [Cisco - Understand Open Shortest Path First (OSPF) - Design Guide](https://www.cisco.com/c/en/us/support/docs/ip/open-shortest-path-first-ospf/7039-1.html)
 - [Befehle und Beispiele für MikroTik und Cisco](https://gitlab.com/alptbz/network-labs/)

#### 1.1.2. 🎥 Videos
 - [MikroTik YouTube - OSPF configuration and use cases](https://www.youtube.com/watch?v=9ziSn19h680)
 - [YouTube - The Network Berg - Full MikroTik MTCRE - OSPF](https://www.youtube.com/watch?v=ypWdDnBxJdk)
 - [Youtube - CertBros - Cisco - OSPF Explained](https://www.youtube.com/watch?v=kfvJ8QVJscc)
 - [IT System Administrator - Song](https://www.youtube.com/watch?v=5kXMnIU1jaY)

## 2. Routing Protokoll Funktion

 1. Routing-Informationen über IP-Subnetze von benachbarten Routern lernen.
 2. Routing-Informationen über IP-Subnetze an benachbarte Router weitergeben.
 3. Wenn mehr als eine mögliche Route existiert, um ein Subnetz zu erreichen, wähle die beste Route basierend auf einem Metric aus.
 4. Wenn sich die Netzwerk-Topologie ändert – zum Beispiel, wenn eine Verbindung ausfällt – reagiere, indem du bekannt gibst, dass einige Routen ausgefallen sind und wähle eine neue, aktuell beste Route. (Dieser Prozess wird als Convergence bezeichnet.)


## 3. Interior and Exterior Routing Protocols

 - IGP: Ein Routing-Protokoll, das für den Einsatz innerhalb eines einzigen autonomen Systems (AS) konzipiert und vorgesehen war.
 - EGP: Ein Routing-Protokoll, das für den Einsatz zwischen verschiedenen autonomen Systemen konzipiert und vorgesehen war.

**💪 Challenge 1:** Recherchiere unterschiedliche Routing Protokolle und bestimme ob sie als IGP oder als EGP verwendet werden. Ziel ist es den Unterschied zwischen EGP und IGP zu verstehen.

## 4. 🥼 Labor 1 - Single Area OSPF with MirkoTik

### 4.1. Vorbereitung

Die nachfolgenden Challenges bauen auf dem nachfolgenden Laborsetup auf. 
**Tools:**
 - GNS3
 - MikroTik 7.*

![Lab](./media/Lab.png)
</br><i>Abbildung 1: Screenshot des Labors in GNS3</i>

### 4.2. Starter Konfiguration für R1

```routeros
/system/identity/set name=R1
/ip address/add interface=ether2 address=10.1.0.1/24
/ip address/add interface=ether3 address=10.1.1.1/24
/ip address/add interface=ether4 address=10.1.255.1/30
/ip route/add gateway=10.1.255.2 dst-address=10.1.64.0/24
/ip route/add gateway=10.1.255.2 dst-address=10.1.65.0/24
```

### 4.3. Starter Konfiguration für R2

```routeros
/system/identity/set name=R2
/ip address/add interface=ether4 address=10.1.255.2/30
/ip address/add interface=ether5 address=10.1.255.5/30
/ip route/add gateway=10.1.255.1 dst-address=10.1.0.0/24
/ip route/add gateway=10.1.255.1 dst-address=10.1.1.0/24
/ip route/add gateway=10.1.255.6 dst-address=10.1.64.0/24
/ip route/add gateway=10.1.255.6 dst-address=10.1.65.0/24
```

### 4.4. Starter Konfiguration für R3

```routeros
/system/identity/set name=R3
/ip address/add interface=ether2 address=10.1.64.1/24
/ip address/add interface=ether3 address=10.1.65.1/24
/ip address/add interface=ether5 address=10.1.255.6/30
/ip route/add gateway=10.1.255.5 dst-address=10.1.0.0/24
/ip route/add gateway=10.1.255.5 dst-address=10.1.1.0/24
```

### 4.5. Repetition statisches Routing

![Failed ping to 10.1.1.1](./media/PingNotWorking.png)
</br><i>Abbildung 2: Screenshot RouterOS CLI: Ping nach 10.1.1.1 auf R3 schlägt fehl.</i>

**💪 Challenge 2:** Mit der aktuellen Konfiguration funktioniert der Ping von Router `R3` zum Router `R1` nicht. Wie kann ein erfolgreicher Ping von Router `R3` nach Router `R1` durchgeführt werden, ohne dass die Konfiguration geändert werden muss und keine zusätzlichen Geräte involviert werden?

**💪 Challenge 3:** Löse Challenge 2 mit Konfigurationsänderungen ohne dynamisches Routing einzusetzen. 

**💪 Challenge 4:** Mindestens drei Routing-Einträge werden nicht benötigt. Aggregiere so viele Routen wie möglich.

### 4.6. Repetition NAT und DHCP

![](./media/NAT_MGMT_Topology.png)
<br>
<i>Abbildung 3: Screenshot aus GNS3 zeigt NAT1-Wolke (verbunden mit R1) und Cloud1-Wolke (br0 verbunden mit R2)</i>

**💪 Challenge 5:** Realisiere für alle Geräte Zugriff ins Internet über R1.
<details>
<summary>Tipps - Challenge 5 - Spoiler Warning</summary>
<ul>
<li>`NAT1` einfügen und mit `ether1` von `R1` verbinden. </li>
<li>DHCP-Client ist auf `ether1` bereits aktiv, eine default Route wird automatisch hinzugefügt</li>
<li>Masquerade NAT for Out-Interface `ether1` konfigurieren</li>
<li>DNS konfigurieren auf Routern</li>
<li>Statische Routen konfigurieren auf `R2` und `R3`</li>
</details>
<br/>

**💪 Challenge 6:** Realisiere Managementzugriff über R2, sodass auf alle Netzwerkgeräte von eigenen Laptop erreicht werden können. Wenn die Web-Interfaces von allen drei Routern über den eigenen Browser aufgerufen werden können, ist das Ziel erreicht. 

### 4.7. Minimale OSPF Konfiguration

Um OSPF besser verstehen zu können kann es helfen die Pakete, welche übermittelt werden mithilfe von *WireShark* zu beobachten. 

Starte *Paket Capture* zwischen `R1` und `R2` (oder `R2` und `R3`) und führe anschliessend die nachfolgenden Befehle bzw. Konfigurationen auf allen drei Konfigurationen ein. 

```routeros
/routing ospf instance add disabled=no name=ospf-instance-1
/routing ospf area add disabled=no instance=ospf-instance-1 name=ospf-area-1
/routing ospf interface-template add area=ospf-area-1 disabled=no
```

Sobald wir die letzte Zeile eingefügt haben, ist OSPF sofort aktiv. In der WebGui können nachfolgende Dinge beobachtet werden:

![RouterOS Gui OSPF Neighbors](./media/OSPF%20Neighbors.png)
<br>
*Abbildung 4: Mit den beiden anderen Routern konnte eine `fully adjacent` Beziehung eingegangen werden. Die beiden Routern tauschen ab jetzt LSA untereinander auf.*

![RouterOS Gui OSPF LSA](./media/OSPF%20LSA.png)
<br>
<i>Abbildung 5: RouterOS LSA in der GUI</i>

![RouterOS GUI OSPF](./media/WireSharkLSAMatch.png)
<br>
<i>Abbildung 6: *Die einzelnen OSPF LSAs können mit den abgefangenen Paketen im WireShark abgeglichen werden.*</i>

![OSPF](./media/RoutingTableOSPF.png)
In der Routing Tabelle sind die OSPF Einträge, sowie die statischen Routing Einträge zu finden. Aufgrund der höheren Distanz werden die statischen Einträge bevorzugt. 

### 4.8. 💡 Input - Routing Distanzen

Typischerweise haben Routen in einer Routing-Tabelle eine Administrative Distanz. Wenn mehrere Routen mit derselben Prefix für eine IP-Adresse in Frage kommen, wird die Route mit der kleinsten Distanz bevorzugt. 

Die nachfolgende Tabelle zeigt die Standardwerte für [administrative Distanzen](https://en.wikipedia.org/wiki/Administrative_distance) in Cisco IOS Routern. Die administrativen Distanzen sind nicht standardisiert und können sich zwischen den Herstellern unterscheiden. 

| Route Source                                                     | Default Distance Values |
|------------------------------------------------------------------|-------------------------|
| Connected interface                                              | 0                       |
| Static route                                                     | 1                       |
| Enhanced Interior Gateway Routing Protocol (EIGRP) summary route | 5                       |
| External Border Gateway Protocol (BGP)                           | 20                      |
| Internal EIGRP                                                   | 90                      |
| IGRP                                                             | 100                     |
| OSPF                                                             | 110                     |
| Intermediate System-to-Intermediate System (IS-IS)               | 115                     |
| Routing Information Protocol (RIP)                               | 120                     |
| Exterior Gateway Protocol (EGP)                                  | 140                     |
| On Demand Routing (ODR)                                          | 160                     |
| External EIGRP                                                   | 170                     |
| Internal BGP                                                     | 200                     |
| Unknown*                                                         | 255                     |

**CCNA Exam Tip:** Diese Standardwerte müssen die Kandidaten an der CCNA Zertifizierungsprüfung auswendig kennen. 

### 4.9. Default Route Propagation

![Routing Table](./media/RoutingTableOSPF.png)
<br>
<i>Abbildung 7: *Routing Tabelle mit statischen und dynamischen Routen*</i>

OSPF hat aus den erhaltenen `LSA`s die Routen ermittelt und in der Routing Tabelle hinterlegt (Siehe Abbildung 7). Die von der OSPF Instanz kommenden Einträge können durch die Buchstaben in der zweiten Spalte erkannt werden: `Do = Dynamic OPSF`. 

**💪 Challenge 7:** Entferne von allen drei Routern die nicht mehr benötigten statischen Routen. 

In *Abbildung 7* ist erkennabar, dass die default Route von Router `R1` nicht standardmässig via OSPF im Netzwerk propagiert wird. 

**💪 Challenge 8:** Passe die Konfiguration von Router `R1` so an, dass dieser seine Default Route über OSPF propagiert. Wie kannst du prüfen, ob die Konfiguration erfolgreich war. 

### 4.10. Minimale Sicherheit

Sicherheitstechnisch müssen OSPF Netzwerke gegen verschiedene Angriffe geschützt werden. Einige Angriffe können durch eine korrekte Konfiguration verhindert werden. Mit der richtigen Konfiguration kann unter anderem verhindert werden:
 - Bogus Router dürfen nicht als OSPF-Neighbors joinen
 - Nur Netzwerke bzw. IPv4-Subnetze, die zugelassen sind, dürfen im Netzwerk propagiert werden. 
Damit wird verhindert, dass die mit falschen Informationen das Routing manipuliert werden kann. 

![OSPF Area](./media/OSPFAreaOnTopology.png)
<br>
<i>Abbildung 8: Topology auf welche die OSPF Area Gelb markiert ist.</i>

**💪 Challenge 9a:** Passe die OSPF-Instanz so an, dass OSPF nur noch auf den Interfaces aktiv ist auf denen OSPF Beziehungen benötigt werden. Wie kannst du testen, ob die Konfiguration auch tatsächlich funktioniert?

Sobald OSPF eingeschränkt wurde, werden verschwinden die vier `/24` Access Netzwerke aus den Routing-Tabellen. Nun müssen wir `R1` und `R3` anweisen, die `connected` Routen zu *announce*. 

**💪 Challenge 9b:** Passe die OSPF-Instanz so an, dass OSPF die Subnetze `10.1.0.0/24`, `10.1.1.0/24`, `10.1.64.0/24` und `10.1.65.0/24` im Netzwerk propagiert. 

![Unwanted entry in R2a](./media/R2RoutingTableWithUnwantedEntry.png)
<br>
*Abbildung 9: Routing Tabelle von `R2a` mit einem hervorgehobenen Routing-Eintrag.*

Die Konfigurationsänderung in *Challenge 9b* kann dazu führen, dass das Subnetz `192.168.122.0/24` auch im Netzwerk propagiert wird. Angenommen wir würden das nicht wollen (z.B. Weil keiner der Clients auf dieses Netzwerk zugreifen muss), können wir das mithilfe eines OSPF Filters kontrollieren. Dazu können wir OSPF Filterfunktion verwenden. 

**💪 Challenge 9c:** Lege für `R1` und für `R3` unter `/routing/filter/rule/` eine oder mehrere Filterfunktionen an, welche nur die beiden Client Access Netzwerke zulässt und verknüpfe die Liste mit der OSPF-Instanz. Es ist ein "Out-filter" zu konfigurieren. Die Challenge ist erfüllt wenn die vier Client-Access-Netzwerke propagiert werden, aber das "NAT" Netzwerk `192.168.122.0/24` nicht. Beachte, dass `R1` die Default Route weiterhin propagieren sollte!

### 4.11. Backup Routen

Wenn OSPF ausfällt, verschiedenen alle von der OSPF Instanz eingefügten dynamischen Routen aus der Routing Tabelle. 

Wenn sich der Netzwerkadministrator nur aus dynamische Routen verlässt, wird die Störungsbehebung mühsamen werden, da er sich von Router zu Router einloggen muss, um das dynamische Routing anzupassen. 

**💪 Challenge 10:** Lege statischen Routen an, sodass bei Ausfall von OSPF vom Management Netzwerk weiterhin auf alle Router zugegriffen werden kann. Die Routen sollen jedoch nur dann aktiv sein, wenn OSPF ausfällt. Tipp: Diese Challenge erfordert nur Änderung an der Routing Tabelle. 

### 4.12. Use Case: Failover

OSPF bietet gegenüber statischem Routing zahlreiche Vorteile. Ein wesentlicher ist die Fähigkeit der Router, bei einem Ausfall ihre Routing-Tabellen neu zu berechnen und, vorausgesetzt es existieren alternative Pfade, neue Routen zu allen Zielen zu etablieren.

In unserer aktuellen Topologie führt ein Ausfall eines Routers automatisch dazu, dass das Netzwerk *degraded* ist. 

Im ersten Schritt möchten wir das Netzwerk so erweitern, dass der Ausfall von `R2` nicht mehr zu einer Unterbrechung der Kommunikation zwischen `PC1` und `PC3` führt.

![Adding R2b to Network](./media/AddingR2bTopology.png)
<br>
*Abbildung 10: Netzwerk wurde um den Router `R2b` erweitert und Router `R2` unbenannt zu Router `R2a`*

**💪 Challenge 11:** Füge einen zusätzlichen Router `R2b` ein und konfiguriere ihn so, dass er Teil der OSPF Area wird. Die Challenge ist dann abgeschlossen, wenn ein Ausfall eines der beiden Routern `R2a` und `R2b` nicht mehr zu einer Unterbrechung der Kommunikation zwischen `PC1` und `PC3` führt. (Siehe Abbildung 10) 

![Ping von PC1 nach PC2](./media/PingRouterFailure.png)
<br>
*Abbildung 11: Ping ausgehend von PC1 nach PC3. Ein paar der Pings ging verloren, kurz nachdem einer der Router ausgefallen ist.*

Schalten wir einer der beiden Router aus über den gerade Pakete von `PC1` nach `PC3` geroutet werden, führt das zu einem kurzen Unterbruch, wie in *Abbildung 11* sichtbar. Um die Zeit zu verkürzen bis das Netzwerk wieder konvergiert hat, können wir den *Hello Interval* und *Dead Interval* auf kleinere Werte setzen. Wichtig: Hello und Dead Interval sollte im ganzen OSPF Netzwerk gleich sein. 

**💪 Challenge 12:** Setze mithilfe des Befehls `/routing/ospf/interface-template/set hello-interval=2 dead-interval=8 numbers=0` den Hello und Dead Timer auf 2 respektive 8 Sekunden und prüfe, ob der Ausfall schneller detektiert wird. Tipp: Anstatt einen Router auszuschalten, kann auch ein Link mit "Suspend" unterbrochen werden. 

### 4.13. Best Practice - OSPF ID

Die Verwendung einer gültigen IP-Adresse, die auf dem Loopback-Interface eines Routers für die OSPF-ID konfiguriert wird, gilt als Best Practice, weil das Loopback-Interface immer verfügbar ist, solange der Router eingeschaltet ist, unabhängig vom Status der physischen Schnittstellen. Dadurch wird eine stabilere OSPF-Identität sichergestellt, da physische Schnittstellen ausfallen können, das Loopback-Interface jedoch nicht. Ausserdem ermöglicht es eine eindeutige Identifikation des Routers im OSPF-Netzwerk, was die Netzwerkverwaltung und Fehlersuche vereinfacht. Die Loopback-Adresse ist aus allen Teilen des Netzwerks erreichbar, was eine konsistente Erreichbarkeit der OSPF-ID gewährleistet. Schliesslich vereinfacht es die OSPF-Konfiguration und das Netzwerkdesign, da die OSPF-ID nicht geändert werden muss, falls physische Interfaces hinzugefügt oder entfernt werden (Siehe [Cisco Community - OSPF Design Best Practices](https://community.cisco.com/t5/networking-knowledge-base/ospf-design-best-practices/ta-p/3112070))

In älteren MikroTik Version (<7.14.0) musste dafür eine Bridge angelegt werden. Ab Version 7.14.x gibt Standardmässig ein Loopback Interface `lo`. 

**Todo** 🛠️ Aktualisiere alle Router auf Version 7.14.x oder höher. 

**💪 Challenge 13:** Ziel dieser Challenge ist, dass alle Router über ihre Router ID erreichbar sind. Dafür muss auf jedem Router auf dem *Loopback*-Interface eine IP-Adresse konfiguriert werden, welche wiederrum als Router-ID in der OSPF Instanz konfiguriert werden werden muss. Empfehlung. Verwende IP-Adressen aus dem Netzwerk `10.255.255.0/24`. Bevor du die IP-Adressen und Router Konfigurierst, beantworte die nachfolgende Frage: Welches Netzgrösse (Subnetzmaske) eignet sich für ein Loopback Netzwerk am besten und weshalb?
