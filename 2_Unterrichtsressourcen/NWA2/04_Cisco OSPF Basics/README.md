# Cisco Packet Tracer Labore - OSPF

![Symbolbild](./media/Symbolbild.png)

Als Vorbereitung für Labor 2 - Multi Area OSPF dienen die nachfolgenden Cisco Packet Tracer Labore. Sie decken alle OSPF Single Area Grundlagen ab. 

# Theorie
#### 1.1.1. 📄 Text
 - [Cisco - Understand Open Shortest Path First (OSPF) - Design Guide](https://www.cisco.com/c/en/us/support/docs/ip/open-shortest-path-first-ospf/7039-1.html)
 - Auf dem [Sharepoint NWA - CCNA - OSPF](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/itcne23/_read-only/NWA/NWA2/CCNA/Theorie%20und%20Cisco%20Packet%20Tracer%20Labs/04_OSPF?csf=1&web=1&e=84AeH7) stehen weitere Dokumente zur Verfügung:
   - *OSPF - Cisco Press CCNA.pdf*
   - *Cisco PPT - OSPF Single Area.pdf* und *Cisco PPT - OSPF Single Area Teil 2.pdf*
   - *OSPF Routing.pdf*

#### 1.1.2. 🎥 Videos
 - [Youtube - CertBros - Cisco - OSPF Explained](https://www.youtube.com/watch?v=kfvJ8QVJscc)
 - [IT System Administrator - Song](https://www.youtube.com/watch?v=5kXMnIU1jaY)



# Praxis

Unter [Sharepoint NWA - CCNA - OSPF](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/itcne23/_read-only/NWA/NWA2/CCNA/Theorie%20und%20Cisco%20Packet%20Tracer%20Labs/04_OSPF?csf=1&web=1&e=84AeH7) sind folgende Labore abgelegt:

## `2.2.13 Packet Tracer - Point-to-Point Single-Area OSPFv2 Configuration`

Dieses Labor beinhaltet die OSPF Grundlagen:
 - Netzwerke für OSPF-Routing mit Network-Befehlen und Wildcard-Masken konfigurieren.
 - Netzwerke für OSPF-Routing mit Interface-IP-Adressen und Quad-Zero-Masken konfigurieren.
 - OSPF-Routing auf Router-Interfaces konfigurieren.
 - Passive Interfaces konfigurieren.

## `2.3.11 Packet Tracer - Determine the DR and BDR`

Selektion des BDR/DR Routen, wenn mehrere OSPF Router im selben Layer 2 Segment miteinander verbunden sind. 

## `2.4.11 Packet Tracer - Modify Single-Area OSPFv2`

Einfluss wichtiger Parameter auf das Verhalten von OSPF:
 - Hello, Dead-Interval Timer
 - Bandwidth Einstellungen


## `2.5.3 Packet Tracer - Propagate a Default Route in OSPFv2`

Default Route propagieren in ein OSPF Netzwerk. 


## `2.6.6 Packet Tracer - Verify Single-Area OSPFv2`

OSPF Status überprüfen und den OSPF Prozess steuern. 

## `2.7.1 Packet Tracer - Single-Area OSPFv2 Configuration`

Selbstständig OSPF für ein Netzwerk konfigurieren. 