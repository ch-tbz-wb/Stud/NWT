# Labor 3 - VPN Technologien

## Einleitung

![Labor](./media/Lab.png)

Sie haben als Netzwerkingenieur den Auftrag erhalten in einem KMU einen zusätzlichen Geschäftsstandort per VPN zu erschliessen. Das KMU hat keinen eigenen Betriebsinformatiker. Jedes Mal, wenn eine Erweiterung des Netzwerkes benötigt wurde, hat das KMU eine andere IT Firma beauftrag. Das hat zur Folge, dass überall unterschiedliche Geräte und VPN Technologien eingesetzt wurden. Das ist eine optimale Gelegenheit, um neue VPN Technologien zu kennenzulernen und zu vergleichen!
Die Wahl des Gerätes für Lausanne steht Ihnen frei. Alle anderen Standorte haben einen funktionierenden Internetanschluss und eine funktionierende VPN Verbindung an den Hauptstandort in Zürich. In der Mitte finden Sie ein Netzwerk eines fiktiven ISP. Der verwendet OSPF als internes Routing-Protokoll (iBGP eignet sich für ISP besser). Über den ISP können sie auch auf das Internet zugreifen. 

## Lernziele
 - Kann VPN für die Verbindung eines Geschäftsstandorts mit dem Hauptstandort planen und umsetzen.
 - Versteht die verschiedenen VPN-Technologien (OpenVPN, WireGuard, IPSEC) und ihre Einsatzmöglichkeiten.
 - Erkennt die Herausforderungen und Vorteile der Integration unterschiedlicher VPN-Technologien und Netzwerkgeräte in einem heterogenen Netzwerkumfeld.
 - Kann die Konfiguration und Implementierung von OpenVPN, WireGuard und IPSEC auf verschiedenen Netzwerkgeräten (MikroTik RouterOS, Cisco IOS, Pfsense) durchführen.

## Allgemeine Instruktionen
Bitte lesen Sie diese Instruktionen sorgfältig durch und fragen Sie bei Unklarheiten den Kursleiter. 
In diesem Modul arbeiten Sie mit der Netzwerksimulationssoftware GNS3, mit der Sie per Drag-and-Drop Topologien in Sekundenschnelle aufbauen können. Im Hintergrund arbeitet GNS3 mit Qemu-KVM VMs für die Switche und Router und verbindet diese bezüglich Ihrer Topologie mit Linux-Bridges.	 Weitere Informationen zum Aufbau der TBZ Cloud Infrastruktur finden Sie im Repository [ch-tbz-it/Stud/allgemein/tbzcloud-gns3](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3).

## Labor

Das VPN Labor kann [hier](https://tbzedu.sharepoint.com/:u:/s/campus/students/ETp8qiVr449FlU327or8jlcB8fTD5H7kkmDdXdezcyLBgQ?e=ypDuIb) heruntergeladen werden. 

## Standard-Zugangsdaten
 - MikroTik: Benutzername: admin, Passwort: tbz1234
 - Debian: Benutzername: debian, Passwort: debian
 - Cisco: Passwort: tbz1234
 - Standardpasswörter zum Ausprobieren: cisco, debian, admin1234, admin

## Regeln
 - Es dürfen keine Geräte entfernt oder hinzugefügt werden (Ausnahme: Geräte welche für den neuen Standard benötigt werden)
 - Es ist erlaubt zu Testzwecken z.B. ein Ubuntu Desktop an einem Port anzuschliessen. (Wie wenn Sie ein Laptop an einem «echten» Router oder Switch einstecken).
 - Alle Kabelverbindungen zwischen den Geräten sind fix. Erstellen Sie keine neuen. (Ausser die den neuen Standort (siehe Hinweis im GNS3 Projekt))

## Teil 1 - VPN Technologien per Packet Capture analysieren

Das Ziel dieser Übung ist es Erfahrungen im Bereich "Analyse mit Wireshark" zu sammeln und die Unterschiede der einzelnen VPN Protokolle WireGuard, OpenVPN und IPSec kennenzulernen. 

Die drei verwendeten VPN Technologien bauen auf unterschiedlichen Netzwerkstacks auf. 

**💪 Challenge 1:** Analysieren Sie die unterschiedlichen Protokolle mithilfe von Wireshark Captures und halten Sie unterschiede in den Netzwerkstacks fest. Beantworten Sie anschliessend die nachfolgenden Fragen und besprechen sie diese mit ihren Kollegen.  

 - Welches Layer 3 und 4 Protokolle kommen bei den einzelnen Technologien zum Einsatz?
 - Bei IPSEC kann der Netzwerkstack unterschiedlich aussehen. Weshalb? Wann kommt welche "Verpackung" zum Einsatz?
 - Weshalb ist es einfacher WireGuard durch eine "Firewall" oder "NAT" "zu bringen" als IPSEC?

### TCP Meltdown
VPN-Technologien, die TCP zur Übertragung verwenden, haben einen entscheidenden Nachteil, der als 'TCP Meltdown' bekannt ist. Aus Leistungsgründen ist es daher vorteilhaft, im VPN keine Protokolle zu verwenden, die die Übertragung des Streams garantieren. 

📕 Lese Empfehlung:
 - [OpenVPN - What is TCP Meltdown?](https://openvpn.net/faq/what-is-tcp-meltdown/)

Von den drei VPN Technologien unterstützt nur OpenVPN ein TCP-Modus. 

## Teil 2 - Dokumentation erstellen

Leider haben die verschiedenen Netzwerkspezialisten nie eine Dokumentation erstellt. 

**💪 Challenge 2:** Halte die logische Topologie auf einem Plan ([draw.io](https://app.diagrams.net/) oder Microsoft Visio (bezug über Azure Education mit TBZ Account: [Azure Education](https://portal.azure.com/#view/Microsoft_Azure_Education/EducationMenuBlade/~/overview))) fest. Logge dich in die Netzwerkgeräte ein, um relevante Parameter (z.B. VPN-Konfiguration) auszulesen. Die Dokumentation sollte sich auf eine A3 Seite begrenzen. Vergleiche anschliessend deine Dokumentation mit der deiner Kollegen. 

## Teil 3 - Neuen Standort anbinden

**💪 Challenge 3:** Der Standort Lausanne soll mithilfe eines Site-To-Site VPNs mit dem Firmenstandort Zürich verbunden werden. Es ist ein entsprechendes Netzwerkgeräte einzufügen und dieses mit allen notwendigen Services zu konfigurieren. Dazu muss zuerst ermittelt werden, welche Services überhaupt benötigt werden. Bei dieser Fragen kann die Konfiguration der bestehenden Standorte helfen. Der Challenge ist abgeschlossen, wenn ich alle Endgeräte in Lausanne die Endgeräte in Zürich anpingen können. 

<details>
<summary>Tipp 1 - Challenge 3 - Spoiler Warning</summary>
<p>Folgende Konfigurationen müssen mindestens vorgenommen werden:</p>
<ul>
<li>IPv4-Adresse und Route auf WAN Seite</li>
<li>IPv4-Adresse auf LAN Seite</li>
<li>DHCP-Server</li>
<li>Firewall für INPUT und FORWARD Chain (je nach gewähltem Hersteller unterschiedlich bezeichnet)</li>
<li>NAT - Masquerade</li>
<li>DNS-Server</li>
<li>Site-To-Site VPN</li>
</ul>
</details>
<br/>

## Teil 4 - Worker1 anbinden

VPN wird nicht nur zur Verbindung zweier Standorte (Site-to-Site) genutzt, sondern auch, um mobilen Mitarbeitern über Remote-Zugriff sicheren Zugang zu internen Ressourcen und Services zu ermöglichen. Diese VPN-Verwendungsart wird gerne auch Roadwarrior VPN genannt. Road Warriors (englisch für Strassenkämpfer) gehören zu jener Gruppe von Mitarbeitern, die viel ausserhalb des eigenen Unternehmens arbeiten und für die ein fest eingerichteter, fixer Arbeitsplatz in einem stationären Büro kaum bis gar keine Bedeutung hat. Die Terminologie für VPN, wie Roadwarrior und Site-to-Site, wird von verschiedenen Herstellern und Softwareprodukten uneinheitlich verwendet, was zu Verwirrung und Missverständnissen führen kann. Insbesondere mit dem aufkommen von VPN Produkten wie "NordVPN" gibt es häufig ein falsches Verständnis darüber was VPN genau ist. Inzwischen gibt es unzählige Youtuber, welche Videos zu diesem Thema gemacht haben (siehe [Linus - Why I’ll be Taking VPN Sponsorships Again… and](https://www.youtube.com/watch?v=n2MuwWgKZ7Q), [MikroTik - What is VPN? And what it is not.](https://www.youtube.com/watch?v=s35X5bKH4Yo)). 

**💪 Challenge 4:** Worker1 möchte Zugriff auf LAN von Lausanne erhalten (damit er auf ein NAS zugreifen kann). Konfiguriere einen Roadwarrior Zugriff für Worker1. Es gibt verschiedene Möglichkeiten dies umzusetzen. 

## Abschluss
In diesem Labor wurde gezeigt, wie man Site-To-Site und Roadwarrior VPNs für in einem Multi-Site-Netzwerk plant und umsetzt, insbesondere durch die Konfiguration und den Vergleich von OpenVPN, WireGuard und IPSEC. Ausgangspunkt war ein bereits bestehendes, aber undokumentiertes Multi-Site-Netzwerk. Es wurden Fähigkeiten zur Netzwerkanalyse mit Wireshark geübt, um die Unterschiede zwischen den VPN-Protokollen zu verstehen. Zudem wurde die Erstellung von Netzwerktopologien und die Einbindung neuer Standorte in das bestehende Netzwerk geübt.
