# Labor 2 - Default Gateway Redundanz mit VRRP  <!-- omit in toc -->

Endgeräte in einem LAN benötigen üblicherweise ein Default-Gateway, um ins Internet zu gelangen. Beim Routen von Datenpaketen durch Netzwerke wird normalerweise dem Ausfall von Teilstrecken durch dynamisches Routing entgegengewirkt. Fällt jedoch der erste Router auf einer Strecke aus, kann dieser Fehler oft nicht durch dynamisches Routing behoben werden, da ein Host in der Regel nur ein statisches Standardgateway kennt und unterstützt. Um die Verfügbarkeit des Gateways zu gewährleisten, kann das Virtual Router Redundancy Protocol (VRRP) eingesetzt werden. Dieses ermöglicht es zwei Routern, sich eine IP-Adresse als virtuelle Gateway-Adresse sowie eine MAC-Adresse zu teilen. Sollte der aktive Router ausfallen, beginnt der Standby Router automatisch, auf die virtuelle Gateway-Adresse zu antworten (Hot-Standby).

In dieser Laborübung erweitern wir ein einfaches Netzwerk um VRRP und das Spanning Tree Protocol (STP), um diese Redundanz des ersten Hops zu gewährleisten.  

![Start Labor](./media/start-lab.png)
<br>
*Abbildung 1: Screenshot der Topologie, welche als Ausgangslage für dieses Labor dient in GNS3*

## Inhaltsverzeichnis  <!-- omit in toc -->
- [1. Lernziele](#1-lernziele)
- [2. Ressourcen](#2-ressourcen)
- [3. Laboraufbau](#3-laboraufbau)
  - [3.1. R1](#31-r1)
  - [3.2. VPCS1 PC1 und PC2](#32-vpcs1-pc1-und-pc2)
- [4. Layer 2 Redundanz - STP default](#4-layer-2-redundanz---stp-default)
  - [4.1. Physische Erweiterung](#41-physische-erweiterung)
  - [4.2. Spanning-Tree](#42-spanning-tree)
- [5. Der zweite Router](#5-der-zweite-router)
- [6. VRRP](#6-vrrp)
- [7. Und was ist jetzt mit dem DHCP Server?](#7-und-was-ist-jetzt-mit-dem-dhcp-server)
- [8. VRRP auf Seite WAN](#8-vrrp-auf-seite-wan)
- [9. Technische Anforderungen](#9-technische-anforderungen)
- [10. Connection Tracking](#10-connection-tracking)
- [11. Server mit redundanten Interfaces](#11-server-mit-redundanten-interfaces)
  - [11.1. Proxmox importieren](#111-proxmox-importieren)
  - [11.2. Proxmox active-backup Netzwerkkonfiguration](#112-proxmox-active-backup-netzwerkkonfiguration)
  - [11.3. Proxmox - LXC Container starten](#113-proxmox---lxc-container-starten)
  - [11.4. Proxmox Server Redundanz](#114-proxmox-server-redundanz)
- [12. Abschluss](#12-abschluss)



## 1. Lernziele
 - Versteht die Funktion und Notwendigkeit eines Default-Gateways in einem LAN sowie die Auswirkungen eines Router-Ausfalls auf die Netzwerkkonnektivität.
 - Erkennt die Rolle des Virtual Router Redundancy Protocol (VRRP) zur Gewährleistung der Verfügbarkeit von Netzwerkgateways und versteht die Operationen von VRRP, einschließlich der Konfiguration von virtuellen IP- und MAC-Adressen.
 - Kann die Konfiguration und Implementierung von VRRP in einem Netzwerk durchführen, um die Hochverfügbarkeit von Gateway-Diensten sicherzustellen.
 - Kann STP in einem Netzwerk konfigurieren und verstehen, wie STP in Layer 2 die Ausfallsicherheit erhöht. 

## 2. Ressourcen
 - 📕 [VRRP Wikipedia](https://de.wikipedia.org/wiki/Virtual_Router_Redundancy_Protocol)
 - 📕 [Cisco VRRP](https://www.cisco.com/c/en/us/td/docs/routers/crs/software/crs_r4-0/addr_serv/configuration/guide/ic40crs1book_chapter10.html)
 - 📕 [MikroTik VRRP Help](https://help.mikrotik.com/docs/display/ROS/VRRP)
 - 📕 [Cisco Press Article about STP](https://www.ciscopress.com/articles/article.asp?p=2832407&seqNum=6)

## 3. Laboraufbau

Baue das Labor gemäss *Abbildung 1* in GNS3 auf und spiele die nachfolgende Konfiguration in Router R1 ein. 

### 3.1. R1 

```mikrotik
/system/identity/set name=R1
/ip dhcp-client/add interface=ether2 add-default-route=no use-peer-dns=no use-peer-ntp=no
/ip address add address=192.168.10.1/24 interface=ether3 network=192.168.10.0 
/ip firewall/nat/add action=masquerade chain=srcnat out-interface=ether1
/ip pool add name=dhcp_pool0 ranges=192.168.10.128-192.168.10.254
/ip dhcp-server add address-pool=dhcp_pool0 interface=ether3 name=dhcp1
/ip dhcp-server network add address=192.168.10.0/24 dns-server=192.168.10.1 gateway=192.168.10.1
/ip dns/set allow-remote-requests=yes
```

### 3.2. VPCS1 PC1 und PC2

**💪 Challenge 1:** Verwende die Konfigurationsbefehle `sleep` und `dhcp` um in den VPCS eine Konfiguration zu hinterlegen (Hint: *Edit config*), welche nach dem Start der VPCS 30 Sekunden wartet, bevor versucht wird über DHCP eine Adresse zu beziehen. Weshalb ist diese Konfiguration sinnvoll?

**Abschluss Laboraufbau:** Nach Abschluss des Laboraufbaus sollten PC1 und PC2 in der Lage sein Hosts ausserhalb des Netzwerkes zu erreichen (z.B. ping nach `tbz.ch`)

## 4. Layer 2 Redundanz - STP default

![STP Symbolbild](./media/stp-symbolbild.webp)
<br>
*Abbildung A: STP Symbolbild erstellt mithilfe von ChatGPT (DALL-E) im Jahre 2024*

Beginnen wir mit dem Ausbau und der Erhöhung der Ausfallsicherheit auf Layer 2, respektive im LAN 192.168.10.0/24. Das Ziel ist, dass bei einem Ausfall eines der beiden Switches der Zugriff auf die anderen Hosts weiterhin gewährleistet ist.

### 4.1. Physische Erweiterung

**💪 Challenge 2:** Erweitere das Netzwerk um zwei Cisco IOSvL2 L3-Switches. Die beiden Endgeräte PC1 und PC2 sollen nicht am selben Switch angeschlossen werden. Die Challenge ist dann abgeschlossen, dass bei Ausfall eines Switches nur die Endgeräte bzw. VPCS, welche direkt an diesem Switch angeschlossen sind vom Ausfall betroffen sind und die Endgeräte am anderen Switch weiterhin Zugriff aufs Internet haben. Es dürfen maximal zwei physische Switche eingefügt werden. 

<details>
<summary>Tipp 1 - Challenge 2 - Spoiler Warning</summary>
<img src="./media/LabWithSTP.png">
<p>Die Verbindung zwischen <i>SW1</i> und <i>SW2</i> ist nicht in jedem Anwendungsfall sinnvoll. Wenn die Verbindung zwischen den Switchen eingesetzt wird, ist es wichtig, dass SW1 <i>root</i> im Spanning-Tree wird, damit der Traffic, welcher zwischen den Geräten im LAN fliesst nicht über den Router geleitet werden. Anwendungsfall: Server im LAN.</p>
</details>
<br/>

<details>
<summary>Tipp 2 - Challenge 2 - Spoiler Warning</summary>
<ul>
<li>R1 Bridge hinzufügen mit ether3 und ether4 als Member.</li>
<li>LAN-IP und DHCP Server der neuen Bridge zuordnen</li>
</ul>
</details>
<br/>

### 4.2. Spanning-Tree

Fliesst Traffic zwischen SW1 und SW2 soll dieser aus Gründen der Performance nicht über den Router R1 fliessen. Da Spanning-Tree sicherstellt, dass es keine Loops gibt indem es Interfaces deaktiviert, müssen wir sicherstellen, dass der Link zwischen *SW1* und *SW2* nicht deaktiviert wird. 

**💪 Challenge 3:** Wie kann sichergestellt werden, dass der Traffic zwischen *SW1* und *SW2* nicht über *R1* fliesst? Prüfe die aktuelle Spanning-Tree status und passe die Konfiguration auf `SW1` und `SW2` so an, dass die Anforderung jederzeit gewährleistet wird (auch wenn der Router R1 ausgetauscht wird oder ein zweiter Router hinzukommt).

<details>
<summary>Tipp 1 - Challenge 3 - Spoiler Warning</summary>
<ul>
<li>show spanning-tree</li>
</ul>
</details>
<br/>

<details>
<summary>Tipp 2 - Challenge 3 - Spoiler Warning</summary>
<ul>
<li>Auf <i>SW1</i> und <i>SW2</i> muss lediglich eine Zeile Konfiguration hinterlegt werden.</li>
<li>Denk daran die Konfiguration auf den Switchen mit <pre>write memory</pre> zu persistieren.</li>
</ul>
</details>
<br/>

![STP R1](./media/stpr1.png)
</br>
*Abbildung 2: Output CLI R1 - Wenn `R1` einer seiner Ports auf den STP Port-Zustand `alternate` (aka. `blocked`) setzt, ist sichergestellt, dass kein Traffic von `SW1` nach `SW2` über `R1` fliesst.*

## 5. Der zweite Router

Bei VRRP teilen sich zwei (oder mehrere) Router eine virtuelle IP und eine MAC-Adressen (nachfolgend IP-MAC-Adresse genannt). Damit auch der Standby-Router für management Zwecke erreicht werden kann, empfiehlt es sich jedem Router eine eindeutige IP-Adresse zuzuteilen.

**💪 Challenge 4:** Plane zwei eindeutige IPv4-Adressen im LAN für den bestehenden Router und für den neuen Standby-Router. Konfiguriere auf der in Challenge 2 angelegten Bridge die zusätzliche IPv4-Adresse (Du hast in Challenge 2 keine Bridge angelegt? Dann gehe zurück und überprüfe deine Schritte.).

**💪 Challenge 5:** Füge den neuen Standby Router `R2` hinzu. Konfiguriere ihn wie `R1`, beachte jedoch folgende Bedingungen:
 - Der DHCP-Server ist konfiguriert, jedoch deaktiviert. 
 - Die Gateway IP (192.168.10.1) ist nicht auf `R2` konfiguriert.

<details>
<summary>Tipp 1 - Challenge 5 - Spoiler Warning</summary>
<p><img src="./media/LabWithTwoRouters.png"></p>
</details>
<br/>

**Hintergrundfrage:** Weshalb sollte der DHCP-Server auf dem Standby Router deaktiviert sein? Weshalb wurde überhaupt ein DHCP-Server konfiguriert, wenn er deaktiviert bleibt?

## 6. VRRP

VRRP (Virtual Router Redundancy Protocol) wird von verschiedenen Netzwerkgeräteherstellern unterstützt. Die Konfiguration unterscheidet sich je nach Gerätetyp ein wenig. Bei MikroTik wird ein virtuelles VRRP-Interface angelegt, auf dem einerseits die VRRP-relevanten Parameter festgelegt und andererseits die gemeinsame IPv4-Adresse zugewiesen wird. Im Fall unseres Labors muss das VRRP-Interface an die virtuelle Bridge angeschlossen werden.

**💪 Challenge 6:** Konfiguriere auf `R1` und `R2` mithilfe der Einleitung zum Abschnitt und der MikroTik Dokumentation zu [VRRP](https://help.mikrotik.com/docs/display/ROS/VRRP)  für das Gateway des LAN Netzwerkes. Ob das Setup funktioniert ist am Status des Interfaces (Active oder Standby) sichtbar. Die Challenge ist abgeschlossen, wenn ein beliebiger Router ausgeschaltet werden kann und die Endgeräte `PC1` und `PC2` weiterhin ein Host im Internet anpingen können. 

![VRRP Master](./media/vrrp-lan-master.png)
<br>
*Abbildung 3: Web-GUI RouterOS: Das aktive VRRP-Interface wird mit mit `master` bezeichnet.*

![VRRP Master](./media/vrrp-lan-backup.png)
<br>
*Abbildung 4: Web-GUI RouterOS: Das standby VRRP-Interface wird mit mit `backup` bezeichnet.*

![IP-Adresse Zuweisung](./media/vrrp-ip-inactive.png)
<br>
*Abbildung 5: Web-GUI RouterOS: IP-Adressen Zuweisung auf für das VRRP-Interface zeigt beim Backup VRRP Interface `invalid` an.*

In *Abbildung 3 bis 5* ist sichtbar, welche Zustände einzelnen Objekte einnehmen. Auf dem Router dessen VRRP-Interface den Zustand `backup` eingenommen hat (siehe *Abbildung 4*) setzt der Router die IP-Adressen Zuweisung auf den Zustand `invalid`. Das kann auf den ersten Moment verwirren, entspricht jedoch dem SOLL-Zustand. Die dahinterliegende Überlegung wird sein, dass auf einem inaktiven Interface keine IP-Adresse zugewiesen werden kann, daher ist die Zuweisung ungültig aka. `invalid`. 

**💪 Challenge 7:** Mithilfe von Wireshark kann VRRP gut analysiert werden. Fange mithilfe von Wireshark VRRP Pakete ab. 

![VRRP Packet Trace](./media/vrrp-wireshark.png)
<br>
*Abbildung 6: Screenshot Wireshark: VRRP Pakete auf dem LAN Interface*

## 7. Und was ist jetzt mit dem DHCP Server?

In der aktuellen Laborkonfiguration ist der DHCP nur auf Router R1 konfiguriert. Das hat zur Folge, dass wenn `R1` ausfällt neue Endgeräte keine IP-Konfiguration per DHCP beziehen können. 

Mit MikroTik Geräten gibt es keine Built-In Möglichkeit einen redundanten DHCP-Server zu konfigurieren. Eine redundante DHCP-Server Lösung muss grundsätzlich zwei Dinge sicherstellen:
 - Nur ein DHCP-Server antwortet auf einen DHCP-Request
 - Die Leases zwischen den Server ist synchronisiert, um die doppelte Vergabe zu verhindern. 

Die Bereitstellung eines hochverfügbaren DHCP-Servers wird in Firmennetzwerken häufig mit dem DHCP-Server von Microsoft sichergestellt ([DHCP Failover, Microsoft Learn](https://learn.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-r2-and-2012/hh831385(v=ws.11))). In diesem Fall wird der DHCP-Server auf dem Router deaktiviert. Eine alternative "Bastellösung" für MikroTik RouterOS ist es die Leases per Skript zu synchronisieren (siehe [MikroTik Forum](https://forum.mikrotik.com/viewtopic.php?t=161489)). 
Eine weitere einfache Lösung ist es den Routern unterschiedliche IP-Pools zu geben und damit die Notwendigkeit der kontinuierlichen Synchronisierung aufzuheben. Falls dadurch der IP-Pool in einem /24-Subnetz zu knapp wird (253 Adressen => 126 pro Pool) kann stattdessen ein grösseres Subnet (/23) verwendet werden. 

**💪 Challenge 8:** Passe die IP-Pools der beiden DHCP-Servern so an, dass sich die Bereiche nicht mehr überschneiden und jeweils mindestens 100 Adressen zur Verfügung stehen. 

Mit der Anpassung des IP-Pools wurde die Problemstellung der doppelten Vergabe gelöst. Eine Synchronisierung der dynamischen Leases ist nicht mehr notwendig. Jetzt muss nur noch sichergestellt werden, dass nur ein DHCP-Server antwortet. 

![VRRP Scripts](./media/vrrp-script.png)
<br>
*Abbildung 7: Screenshot Winbox - VRRP Interface - Section Scripts. Die Lösung ist der Challenge 9 ist verpixelt.*

**💪 Challenge 9:** Füge in der VRRP-Interface-Konfiguration auf beiden Routern ein Script bzw. einen Befehl ein, der den DHCP-Server aktiviert, wenn das VRRP-LAN-Interface Aktiv ist (`On Master`) und deaktiviert, wenn das VRRP-LAN-Interface auf Standby (`On Backup`) ist. 

## 8. VRRP auf Seite WAN

Genauso wie auf LAN Seite, kann VRRP auf WAN Seite eingesetzt werden. Voraussetzung sind genügend ausreichend verfügbare öffentliche IPv4-Adressen, damit der Router auch Zugang ins Internet hat, wenn er nicht die Master-Rolle hat.

**💪 Challenge 10:** Konfiguriere VRRP auf WAN Seite und stelle mithilfe eines VRRP Parameters sicher, dass immer nur ein Router auf allen VRRP-Interfaces die Master Rolle hat. 

<details>
<summary>Tipp 1 - Challenge 10 - Spoiler Warning</summary>
<ul>
<li>Priority</li>
</ul>
</details>
<br/>

## 9. Technische Anforderungen

 - Der Uplink in das Internet für Endgeräte (VPCS) ist sichergestellt, selbst wenn ein Router und/oder ein Switch ausfällt. 
 - Endgeräte erhalten bei Ausfall eines Routers weiterhin IP-Adressen per DHCP

**💪 Challenge 11:** Definiere Tests mit welchen sichergestellt werden kann, dass die technischen Anforderungen erfüllt sind. Halte die Tests schriftlich fest. Führe anschliessend alle Tests durch und dokumentiere die Resultate. Wenn ein Feedback zu den Tests gewünscht ist, kann die Kursleiter*in zugegangen werden. **Wichtig:** Definiere deine Tests [SMART](https://de.wikipedia.org/wiki/SMART_(Projektmanagement)) (Specific Measurable Achievable Reasonable Time-bound).

## 10. Connection Tracking

**💪 Challenge 12:** Ziel dieser Challenge ist es zu verstehen, weshalb TCP und UDP "Verbindungen" (in Anführungs- und Schlusszeichen, weil UDP grundsätzlich kein Verbindung sicherstellt) unterbrochen werden, wenn der *VRRP Master* ausfällt und das *VRRP Backup* übernimmt. 

![Connection Tracking](./media/conntrack.png)
<br>
*Abbildung 8: Screenshot Winbox - Firewall - Connections*

**💪 Challenge 13:** MikroTik *RouterOS v7 supports VRRP connection tracking synchronization*. Aktiviere die Connection Tracking Synchronisation und baue ein Testsetup um zu testen, ob TCP-Verbindungen tatsächlich nicht unterbrochen werden, wenn der VRRP-Master wechselt. 

<details>
<summary>Tipp 1 - Challenge 13 - Spoiler Warning</summary>
<ul>
<li>Debian VM auf Seite LAN einfügen -> SSH zu server im Internet aufbauen</li>
<li>Zwei Debian VMs einfügen: Einer als SSH Client und einer als SSH Server</li>
<li>Anstatt SSH: Netcat verwenden (Da der SSh möglicherweise automatisch eine neue TCP Verbindung aufbaut).</li>
</ul>
</details>
<br/>

## 11. Server mit redundanten Interfaces

Ein wichtiges Kriterium für die Bewertung der Effektivität von Redundanzmassnahmen ist die Identifizierung von Single Points of Failure. Um Single Points of Failure zu vermeiden, müssen alle Komponenten redundant vorhanden sein. Viele Server- und Hypervisor-Betriebssysteme bieten dafür entsprechende Konfigurationsmöglichkeiten.

### 11.1. Proxmox importieren
Als Serverbetriebssystem setzt diese Laborübung Proxmox ein. Proxmox ist eine Open-Source-Servervirtualisierungsumgebung, die auf Debian GNU/Linux basiert und den KVM-Hypervisor sowie LXC-Containertechnologien integriert. Es bietet eine leistungsstarke und flexible Lösung zur Verwaltung virtualisierter Umgebungen. In der aktuellen Version (Stand 21.05.2024) ist Proxmox 8.2 nicht in GNS3aaS integriert und muss deshalb importiert werden. 

![new template](./media/newtemplate.png)
<br>
*Abbildung 9: New Template button in GNS3 GUI*

**💪 Challenge 14:** Ziel dieser Challenge ist die Installation des Appliance Templates *Proxmox 8.2* in GNS3 (Sofern noch nicht vorhanden). Die benötigte gns3a und das Image kann unter https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3/-/tree/main/images heruntergeladen werden. 

### 11.2. Proxmox active-backup Netzwerkkonfiguration 

Proxmox unterstützt die Verwendung von [Linux Bond](https://wiki.linuxfoundation.org/networking/bonding). Der Linux-Bonding-Treiber ermöglicht die Aggregation mehrerer Netzwerkschnittstellen zu einer einzelnen logischen Bonding-Schnittstelle, die je nach Modus entweder Hot-Standby- oder Lastverteilungsdienste bietet und die Integrität der Verbindungen überwachen kann.

![new template](./media/proxmox-networking.png)
<br>
*Abbildung 10: Netzwerkinterface-Konfiguration in der Proxmox Web GUI*

![GNS3 Link Suspend](./media/gns3-suspend.png)
<br>
*Abbildung 11: Damit ein Link von der Proxmox VM als inaktiv bzw. down erkannt wird, muss diese suspended werden.*

![Bond0 interface down](./media/proxmox-bond-cli.png)
<br>
*Abbildung 12: Mit dem abgebildeten Befehl kann der aktuelle Zustand des Linux Bonds in der CLI überprüft werden.*

**💪 Challenge 15:** Ziel dieser Challenge ist die Konfiguration des Linux Bonds auf einem Proxmox Server. Dafür muss eine Proxmox Appliance in das Labor eingefügt, korrekt angeschlossen werden und gestartet werden. Das Ziel ist erreicht, wenn Proxmox erreichbar ist, egal, ob eines der beiden Interfaces ausser Betrieb ist. Zur Lösungsüberprüfung und Unterstützung dienen Die Abbildungen 10 bis 12. 

### 11.3. Proxmox - LXC Container starten

Nicht nur das Host-Betriebssystem ist dank dem Linux Bond redundant erreichbar, sondern auch alle VMs, welche an der virtuellen Bridge angeschlossen sind. 

![LXC Container](./media/lxc-container.png)
<br>
*Abbildung 13: Zugriff auf LXC Container CLI über die Proxmox WebGUI*

**💪 Challenge 16:** Ziel dieser Challenge ist ein Debian LXC Container zu konfigurieren und Tests durchzuführen, welche zeigen, dass die Redundanz in Layer 2 und Layer 3 gewährleistet ist. Es kann gezeigt werden, das selbst wenn ein Switch oder Router ausfällt die VMs auf dem Proxmox Server weiterhin erreichbar sind. 

### 11.4. Proxmox Server Redundanz

Wenn ein Switch oder/und ein Router ausfällt funktioniert ist der Zugriff auf den Server weiterhin gewährleistet. Wenn jedoch der Proxmox Server selbst ausfällt, ist der Zugriff nicht mehr möglich. 

**Frage:** Wie kann mit Proxmox gewährleistet werden, dass die VMs selbst bei einem Serverraum erreichbar bleiben?

## 12. Abschluss
In der Laborübung wurde das Verständnis für die Rolle und Notwendigkeit eines Default-Gateways in lokalen Netzwerken vertieft, ebenso wie die Auswirkungen eines Router-Ausfalls auf die Netzwerkkonnektivität behandelt. Die Lernenden setzten sich intensiv mit dem Virtual Router Redundancy Protocol (VRRP) auseinander, um die Verfügbarkeit von Netzwerkgateways zu gewährleisten und erlernten die Konfiguration und Implementierung desselben zur Sicherung der Gateway-Hochverfügbarkeit. Zusätzlich wurde die Konfiguration des Spanning Tree Protocol (STP) erarbeitet, um die Redundanz und Ausfallsicherheit auf Netzwerkebene zu erhöhen.

Die Teilnehmenden bewältigten diverse Herausforderungen, darunter die Erweiterung eines Netzwerks um VRRP und STP, das Konfigurieren zweier Router für die Nutzung einer gemeinsamen virtuellen IP-Adresse und die Sicherstellung, dass bei einem Ausfall des aktiven Routers der Standby-Router nahtlos übernimmt. Sie führten auch komplexe Konfigurationsaufgaben durch, wie das Einrichten von DHCP-Servern auf beiden Routern und das Anpassen der IP-Pools, um Konflikte zu vermeiden und die Netzwerkdienste auch bei einem Geräteausfall aufrechtzuerhalten.

