# NWA3

# Themen
 - Dynamisches Routing mit OSPF mit Cisco und MikroTik
 - Single und Multi-Area OSPF
 - Default Gateway Failover mit VRRP
 - VPN Technologien: IPsec, OpenVPN, WireGuard mit MikroTik, pfsense und Cisco

## Labore
- [Cisco IPv6 Basics](./01_Cisco%20IPv6%20Basics/)
- [Labor 1: IPv6](./02_Labor1_IPv6/)
- [Labor 2: VRRP](./03_Labor2_VRRP/)
- [Labor 3: VPN](./04_Labor3_VPN/)


