# 3. 🥼 Labor 1 - Multi Area OSPF with MirkoTik und Cisco
![Lab](./media/MultiAreaLab.png)
</br><i>Abbildung 1: Screenshot des Labors in GNS3</i>

## Vorbereitung

Als Ausgangslage für dieses Labor dient das Endergebnis aus Labor 1. 

Da sich viele Netzwerkgeräte-Hersteller an der CLI und Konfigurationsweise von Cisco orientieren, ist es eine gute Übung OSPF mit Cisco Routern zu konfigurieren. 

## Ziele
 - Labor um zusätzliche Area 1 erweitert
 - Area besteht aus drei Cisco IOS Routern
 - An Router GR-R3 ist ein Access Netzwerk `10.2.1.0/24` konfiguriert
 - An Router GR-R2 ist ein Access Netzwerk `10.2.2.0/24` konfiguriert
 - Alle Netzwerkgeräte sind von allen anderen Netzwerkgeräten erreichbar
 - OSPF wird für dynamisches Routing eingesetzt

## IPv4-Subnetz-Planung und Konfiguration

Für die neue `Area 1` wird ein IPv4-Subnetz verwendet, dass ausserhalb des Ranges von `Area 0` liegt. 

**💪 Challenge 1:** Plane die benötigen IPv4-Subnetze für die neue Area. 

<details>
<summary>Tipps - Challenge 1 - Spoiler Warning</summary>
<ul>
<li>In der Area 0 (Backbone) verwenden wir /30 Subnetze aus dem Bereich 10.1.255.0/24</li>
<li>Für die Access Netzwerke ist 10.2.1.0/24</li>
<li>Welches Subnetz eignet sich am besten und ist ein optimaler Nachfolger für 10.1.255/24?</li>
</details>
<br/>

**💪 Challenge 2:** Konfiguriere die IPv4 Adressen auf den drei Routern.

<details>
<summary>Tipps - Challenge 2 - Spoiler Warning</summary>
<ul>
Insgesamt müssen 6 IPv4 Adressen und aus drei /30 Subnetze konfiguriert werden.
</details>
<br/>

Hilfe: [Cisco Basics](https://gitlab.com/alptbz/network-labs/-/blob/master/labs/cisco-basic/README.md)

## Cisco OSPF Area für Standort GR

Um die OSPF auf Cisco IOS zu aktivieren, benötigen wir nur zwei Befehle (Im Config Modus):
 - `router ospf 1` 
 - `network NETWORK WILDCARD area AREA-NUM`

### OSPF - Cisco - Wildcard

Vereinfacht ist eine Wildcard eine umgekehrte Subnetzmaske. Aus `255.255.0.0` wird `0.0.255.255`, aus `255.128.0.0` wird `0.127.255.255`, usw. 

### OPSF Area Konfigurieren

![Cisco OSPF](./media/CiscoOSPF.png)
</br><i>Abbildung 2: OSPF Area 1 auf dem Router GR-R1 konfiguriert</i>

**💪 Challenge 3:** Konfiguriere die OSPF Area 1 auf allen drei Routern. 

### Vertiefungsfrage
 - Weshalb wird OSPF Area 1 und nicht OSPF Area 0 konfiguriert?

### OSPF Status überprüfen

Wenn OSPF korrekt konfiguriert ist, bilden die Router *Adjacencies*. 


![Cisco OSPF Adjacencies](./media/showospfneighbor.png)
</br>*Abbildung 3: `show ospf neighbor` führt eine Liste mit allen Neighbors auf mit denen der Router eine Adjacencies eingegangen ist und deren Status.*

In *Abbildung 3* sehen wir mit welchen *Neighbors* der Router erfolgreich *Adjacencies* eingegangen ist. FULL steht für einen erfolgreichen Abschluss. Sofern in einem Layer 2 Segment nur zwei Router sind, ist das der gewünschte Zustand.

Sobald die Router den Zustand `FULL` haben, beginnen Sie LSA auszutauschen. 

![Cisco OSPF Database](./media/ospfdatabase.png)
</br><i>Abbildung 4: Output von show ip ospf database</i>

Empfangene LSAs werden in der *OSPF Database* abgelegt (siehe *Abbildung 4*). Der Router leitet davon die Routen ab und trägt diese in der Routing Tabelle ein (Siehe *Abbildung 5*).

![Cisco OSPF Routes](./media/routesospf.png)
</br><i>Abbildung 5: Nachdem auf allen Routern OSPF aktiviert sind, finden sich durch den OSPF Prozess definierte Routen in der Routing Tabelle. </i>

## OSPF Areas miteinander verbinden

### OSPF Areas

Jeder Non-Backbone-Bereich (Area) muss direkt mit der Backbone Area (Area 0) verbunden sein, wobei diese Verbindung entweder physisch über einen Area Border Router (ABR) oder logisch durch einen virtuellen Link erfolgen kann, falls keine direkte physische Verbindung möglich ist.




# Weitere Inhalte
 - Inter-Area vs Intra-Area




