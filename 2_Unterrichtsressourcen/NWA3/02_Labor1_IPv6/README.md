# Labor 4: IPv6  <!-- omit in toc -->

# Lernziele
 - Lokales Subnetting mit Public-IPv6 
 - IPv6 Prefix via DHCPv6 zugeteilt bekommen
 - IPv6 Hosts mit Firewall schützen
 - IPv6 Adressen mit SLAAC an Endhosts zuteilen.

# Empfehlenswerte Lektüre!

IPv6 hätte IPv4 ablösen sollen, doch bis heute ist IPv4 weiterhin dominierend und IPv6 spielt eine nette Nebenrolle. Dieser [Artikel mit dem Titel "The IPv6 Transition"](https://www.potaroo.net/ispcol/2024-10/ipv6-transition.html) vermittelt zentrale Konzepte des Internets und beschäftigt sich gleichzeit mit der Frage wie es mit der *IPv6 Adoption* weitergehen wird. 

In diesem Artikel hat jemand versucht einen Monat ohne IPv4 auszukommen und hat dabei die technischen Details dazu dokumentiert: [No NAT November: My Month Without IPv4](https://blog.infected.systems/posts/2024-12-01-no-nat-november/). 

# Wichtige Hinweise zum Vorgehen
 - Wichtig: In diesem Labor werden *public routable IPv6 addresses* verwendet. Es ist keine Firewall dazwischengeschaltet! Verwenden Sie ein sicheres Passwort auf Geräten in Ihrem Labor, welche ungeschützt sind.
 - Eine Sammlung von nützlichen Befehlen, welche in den Challenges benötigt werden, sind am Ende des Dokumentes zu finden. 

# Vorbereitungen
![Labor Übersicht](media/Overview.png)
<br>*Abbildung 1: Übersicht über das Labor*

Das Labor ist wie in *Abbildung 1* aufzubauen. Es wird empfohlen zu beginn `Desktop01` und `Desktop02` (Ubuntu Desktop Guest) **NICHT** einzuschalten.

# Teil 1 - Obtaining a Publicly Routable IPv6 Prefix

IPv6 ist in modernen Netzwerken unverzichtbar, insbesondere für den Zugriff auf öffentliche IPv6-Dienste oder zur Implementierung eines Dual-Stack-Betriebs (IPv4 + IPv6), der heute als Standard gilt. In der Regel stellen Internetanbieter wie Init7 ihren Kunden einen /48-IPv6-Adressblock per DHCPv6 bereit. Dieser ermöglicht die Aufteilung in bis zu 65.536 Subnetze mit jeweils einem /64-Präfix, was für die meisten Anwendungsfälle ausreichend ist und die automatische Adresskonfiguration mit SLAAC erforderlich ist.

Im Gegensatz zu IPv4 sind IPv6-Adressen in großer Menge verfügbar. Daher erheben Internetanbieter – zumindest in der Schweiz – keine zusätzlichen Gebühren für die Bereitstellung von IPv6-Adressen. Sollte ein Anbieter jedoch keine IPv6-Unterstützung bieten, gibt es alternative Optionen, beispielsweise die Nutzung des [6to4](https://de.wikipedia.org/wiki/6to4)-Protokolles, um IPv6 über IPv4 zu übertragen. 

Unsere GNS3 Labore können über NAT auf Ressourcen im Internet zugreifen, jedoch wird für einen 6to4-Tunnel eine öffentliche IPv4 Adresse benötigt. Das steht in den GNS3 Labore nicht zur Verfügung. Damit in den Laboren trotzdem mit öffentlichen IPv6-Adressen gearbeitet werden kann, wurde ein Router im **TBZ-Cloud** Netzwerk bereitgestellt, welches das 6to4 Tunnel terminiert und IPv6-Prefixe über einen EOIP Tunnel zur Verfügung stellt. 

Da der EOIP-Tunnel nicht verbindungsorientiert ist und somit die beiden NATs zwischen dem GNS3-Labor und dem Showcase-Router nicht passieren kann, wird WireGuard als Transportmedium eingesetzt, um innerhalb dieses Tunnels den EOIP-Tunnel zu betreiben. Dieses Konstrukt ist in produktiven Szenarien, deren Ziel die Bereitstellung von IPv6 für Endgeräte ist, nicht praktikabel. In solchen Fällen sollte vorrangig ein IPv6-Präfix beim Internetprovider beantragt werden. Falls dies nicht möglich ist, kann ein IPv6-Tunnel-Broker genutzt werden, um einen entsprechenden Präfix zu erhalten. Das vorgestellte Setup dient jedoch der Demonstration, wie verschiedene Protokolle kombiniert werden können, um Herausforderungen oder Einschränkungen im Netzwerk zu überwinden.

```
┌────────────────────────────┐  
│ Public IPv6 Internet       │  
└─────────┬──────────────────┘  
          │       
┌─────────┴──────────────────┐  
│ ipv6.he.net Tunnel Broker  │  
└─────────┬──────────────────┘  
          │                     
          │                     
┌─────────┴─────────────┐       
│ Init7-Managed-Router  │       
└─────────┬─────────────┘       
          │                     
          │                     
┌─────────┴─────────────────┐   
│ MikroTik Router - Showcase│   
└─────────┬─────────────────┘   
          │                     
          │  WireGuard +        
          │  EoIP-Tunnel        
          │                     
┌─────────┴────────────────────┐
│ MikroTik Router - GNS3 Labor │
└──────────────────────────────┘
```
*Abbildung 2: Topologie des Laborsetups*

**💪 Challenge 1:** Erhalten Sie eine öffentliche IPv6-Adresse auf Ihrem MikroTik Router. Die Aufgabe ist erfüllt, wenn auf über die öffentliche IPv6-Adresse des Routers auf das Management zugegriffen werden kann. 

Vorgehen für **Challenge 1**:
1. Zuerst sind Private-Key, EOIP Tunnel-ID, IPv4 Adresse für Tunnel, Wireguard Endpoint IPv4 + Port + Public-Key beim Kursleiter*in zu beziehen. Üblicherweise steht eine entsprechende Excel-Tabelle bereit. 
2. Alle nachfolgenden Schritte sind auf dem Entry-Point Router R1 durchzuführen. 
3. WireGuard Verbindung einrichten und mit Konnektivität mit Ping testen (siehe [MikroTik Help - Wireguard](https://help.mikrotik.com/docs/spaces/ROS/pages/69664792/WireGuard) und [NWA1 - VPN](../../NWA1_MTCNA/04_Labor_4_PPP_VPN/))
4. EOIP Tunnel hinzufügen (siehe [MikroTik Help - EOIP](https://help.mikrotik.com/docs/spaces/ROS/pages/24805521/EoIP))
5. DHCPv6-Client hinzufügen (siehe [MikroTik Help - DHCPv6Client](https://help.mikrotik.com/docs/spaces/ROS/pages/24805500/DHCP#DHCP-DHCPv6Client))
6. Virtuelle Bridge im Router anlegen.
7. Aus dem durch den DHCPv6-Client angelegten Pool eine IPv6-Adresse dem Interface zuweisen.
8. Ping test durchführen an Google (2a00:1450:400a:800::2003) oder (2001:4860:4860::8888).
9. Via Winbox, Webbrowser oder SSH auf den Router über die öffentliche IPv6-Adresse zugreifen vom eigenen Laptop aus (vorausgesetzt der eigene Laptop hat eine IPv6-Adresse!). 

<details>
<summary>Tipp 1 - Challenge 1 - Spoiler Warning</summary>
<p>Schritte: 1. Wireguard-Interface hinzufügen mit dem zugewiesenen Private-Key.<br/>2. Wireguard-Peer hinzufügen.<br/>3. IPv4-Adresse hinzufügen.<br/>4. Tunnel Status überprüfen (TX und RX Bytes Counter gehen kontinuierlich hoch)<br/>5. Ping test durchführen.</p>
</details>
<br/>

<details>
<summary>Tipp 2 - Challenge 1 - Spoiler Warning</summary>
<p>Das EOIP-Interface benötigt nur die Remote-IP und die Tunnel-Id. Im Status kann im Feld <i>Last Link Up Time</i>überprüft werden, ob der Tunnel erfolgreich aufgebaut wurde.<br/></p>
</details>
<br/>

<details>
<summary>Tipp 3 - Challenge 1 - Spoiler Warning</summary>
<p>Der DHCPv6-Pool Name kann frei gewählt werden. Die Prefix-Length Wert spezifiziert, wie gross die einzelnen Netzwerke sein sollen, welche aus dem Pool bezogen werden können.</p>
</details>
<br/>

# Teil 2 - SLAAC
Die einfachste Art IPv6-Adressen in einer Broadcast-Domäne zu verteilen ist durch den Einsatz von SLAAC (Stateless Address Autoconfiguration). Im deutschen [Wikipedia-Artikel zu IPv6](https://de.wikipedia.org/wiki/IPv6#Ablauf) ist der Ablauf wie die IPv6-Adresse dem Host zugewiesen wird gut zusammengefasst: *Mittels Stateless Address Autoconfiguration (SLAAC) kann ein Host automatisch eine IPv6-Adresse und Netzwerkverbindung konfigurieren, indem er über das Neighbor Discovery Protocol (NDP) mit Routern kommuniziert, Präfixinformationen empfängt und mithilfe von Duplicate Address Detection (DAD) sicherstellt, dass keine Adresse doppelt vergeben wird.*

Für SLAAC ist ein ::/64-Subnetz erforderlich, da die Adressgenerierung clientseitig auf der Kombination eines vom Router bereitgestellten Präfixes und eines Interface Identifiers basiert, der mithilfe von Algorithmen wie EUI-64 ([RFC 4291](https://www.rfc-editor.org/rfc/rfc4291.html)) oder Privacy Extensions ([RFC 4941](https://datatracker.ietf.org/doc/html/rfc4941)) erzeugt wird, um eine eindeutige und routbare IPv6-Adresse zu gewährleisten.

**💪 Challenge 2:** Stelle sicher, dass die der Bridge zugewiesenen IPv6-Adresse bzw. das Prefix per SLAAC bzw. ICMPv6 in der Broadcast-Domäne announced wird. 

<details>
<summary>Tipp 1 - Challenge 2 - Spoiler Warning</summary>
<p>1. Sicherstellen, dass es eine ::/64 Maske ist. <br/>2. Die richtige Check-Box auswählen.</p>
</details>
<br/>

# Teil 3 - IPv4 Setup
Ein typisches Dual-Stack-Adressverteilungssystem kombiniert DHCPv4 und SLAAC, um sowohl IPv4- als auch IPv6-Adressen effizient bereitzustellen. Während ein Client durch SLAAC (Stateless Address Autoconfiguration) automatisch eine IPv6-Adresse erhält und über ICMPv6 das Gateway der Domäne ermitteln kann, bleibt die DNS-Server-Information unkonfiguriert. Diese Lücke wird durch die parallele Verwendung von DHCPv4 geschlossen, das neben IPv4-Adressen auch die DNS-Server-Information bereitstellt. Dieses Setup gewährleistet eine vollständige Konnektivität in Dual-Stack-Umgebungen.

**💪 Challenge 3:** Stelle auf der Bridge von Router R1 IPv4 Konnektivität sicher.

<details>
<summary>Tipp 1 - Challenge 3 - Spoiler Warning</summary>
<p>Diese Kompetenz ist Teil von NWA1.</p>
</details>
<br/>

# Teil 4 - Endgerät

**💪 Challenge 4:** Desktop01: Hat das Endgerät IPv4 und IPv6 Konnektivität ins Internet ist der Challenge erfüllt. Dies kann unter anderem mithilfe des Service https://test-ipv6.com überprüft werden (siehe Abbildung 2).

![Ubuntu IPv6 Test](media/UbuntuIPv6Test.png)
<br>*Abbildung 2: Ubuntu Desktop mit Erfolgreich durchgeführten IPv6 Test auf https://test-ipv6.com.*


# Teil 5 - Mehrere Subnetze

**💪 Challenge 5:** Üblicherweise liegen in Business-Netzwerke mehrere voneinander getrennte Netzwerke vor. Füge ein weiteres VLAN hinzu mit IPv4 und IPv6. Ziel ist es, dass `Desktop02` in einem anderem IPv4 und IPv6 Netzwerk eine Adresse hat und Zugriff aufs Internet hat. Es dürfen keine zusätzliche Verbindungen zwischen den Geräten hinzugefügt werden. 


# Teil 6 - Firewall
Eine NAT bildet auch eine Art eine "natürliche Firewall", weil die Geräte dahinter keine öffentliche IPv4 Adressen verfügen. In IPv6 wird auf eine NAT verzichtet unter anderem auch, weil die NAT eine Notlösung war, um den mangel von IPv4 entgegen zu wirken. 

In der aktuellen Konfiguration sind `Desktop01` und `Desktop02` über das Internet ohne Einschränkungen erreichbar. In einer *Zero Trust Architektur* wird die *Internet Zone* als eine *untrusted Zone* betrachtet. Dementsprechend sollten alle von dieser Zone ausgehenden Verbindungen als nicht vertrauenswürdig eingestuft und blockiert (drop) werden.

**Bevor mit der Challenge 6 begonnen wird** empfiehlt es sich `Desktop01` und `Desktop02` über dessen IPv6-Adressen vom eigenen Laptop anzupingen, um anschliessend feststellen zu können, ob die Firewall-Konfiguration erfolgreich war. 

**💪 Challenge 6:** Konfiguriere die IPv6-Firewall so, dass...
1. keine vom Router *exposed services* vom Internet erreichbar sind.
2. die Hosts "hinter dem Router" nicht erreichbar sind.
3. Der Router und alle Hosts weiterhin auf Hosts im Internet zugreifen können. 

Der Challenge 6 ist erfolgreich abgeschlossen, wenn alle Endgeräte hinter dem Router und dem Router selbst aus dem öffentlichen Internet nicht mehr erreichbar sind und auf den Desktop der IPv6-Test weiterhin einwandfrei funktioniert. 

<details>
<summary>Tipp 1 - Challenge 6 - Spoiler Warning</summary>
<p>Insgesamt werden 4 Firewall-Regeln benötigt.</p>
</details>
<br/>

<details>
<summary>Tipp 2 - Challenge 6 - Spoiler Warning</summary>
<p>Zwei der vier Firewall-Regeln sind auf dem Input-Chain, zwei auf dem Forward-Chain.</p>
</details>
<br/>

<details>
<summary>Tipp 3 - Challenge 6 - Spoiler Warning</summary>
<p>Zwei der vier Regeln sind Drop, zwei sind accept.</p>
</details>
<br/>

<details>
<summary>Tipp 4 - Challenge 6 - Spoiler Warning</summary>
<p>Der Connection-State muss bei einer Regel konfiguriert werden. Erinnere dich an die Verbindungsorientierte Firewall aus NWA1. </p>
</details>
<br/>

# Nützliche Befehle

```mikrotik
/system identity set name=
/interface bridge add name=XY
/interface bridge port add bridge= interface=
/interface eoip add name= remote-address= tunnel-id=
/interface wireguard add private-key=
/interface wireguard peers add allowed-address= endpoint-address= endpoint-port= interface= name= persistent-keepalive=25s preshared-key= public-key=
/ip address add address= interface=
/ip dhcp-client add interface=ether1
/ip dhcp-client add interface= add add-default-route=no interface=ether2 use-peer-dns=no use-peer-ntp=no
/ip dns set allow-remote-requests=
/ip firewall nat add action= chain= out-interface=
/ipv6 address add from-pool= interface=
/ipv6 dhcp-client add add-default-route= interface= pool-name= request=
/ipv6 firewall filter add action= chain= connection-state=
/ipv6 firewall filter add action= chain= connection-nat-state=!dstnat in-interface-list=WAN
```