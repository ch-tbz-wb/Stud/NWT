# Cisco Packet Tracer Labore - IPv6

# Theorie
## 📄 Text
 - Auf dem [Sharepoint NWA - CCNA](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/_read-only/Unterlagen/NWA?csf=1&web=1&e=8ZHhcx) stehen weitere Dokumente zur Verfügung:
   - `BasicIPv6-Addressing-Plan-Howto.pdf`
   - `BasicIPv6-Slides.pdf`
   - `IPv6 - Cisco Press CCNA 200-301.pdf`
   - `ipv6_reference_card_July2015.pdf`
   - `IPv6-addressing-plan-howto.pdf`
 - [MikroTik - IPv4 and IPv6 Fundamentals](https://help.mikrotik.com/docs/spaces/ROS/pages/119144661/IPv4+and+IPv6+Fundamentals)

## 🎥 Videos
 - [Youtube - CertBros - IPv6](https://www.youtube.com/watch?v=irhS0ASkvy8)

# Praxis

Unter [Sharepoint NWA - CCNA](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/wb/_read-only/Unterlagen/NWA?csf=1&web=1&e=8ZHhcx) sind folgende Labore abgelegt:

## `12.6.6 Packet Tracer - Configure IPv6 Addressing`
Einführung ins Thema IPv6: Adressierung konfigurieren und Netzwerkkonnektivität testen.
 - IPv6-Adressierung auf dem Router konfigurieren
 - IPv6-Adressierung auf Servern konfigurieren
 - IPv6-Adressierung auf Clients konfigurieren
 - Netzwerkkonnektivität testen und überprüfen

## `12.9.1 Packet Tracer - Implement a Subnetted IPv6 Addressing Scheme` 
IPv6: Subnetze planen, Adressierung konfigurieren und Konnektivität überprüfen.
 - IPv6-Subnetze und Adressierungsschema festlegen
 - IPv6-Adressierung auf Routern und PCs konfigurieren
 - IPv6-Konnektivität überprüfen


## `13.2.6 Packet Tracer - Verify IPv4 and IPv6 Addressing`
IPv4 und IPv6 Netzwerkadressierung und -diagnose: Adressierung dokumentieren, Konnektivität testen und Routen analysieren.
 - IPv4- und IPv6-Adressierungstabelle ausfüllen
 - Konnektivität mit Ping testen
 - Routen mithilfe von Traceroute analysieren

