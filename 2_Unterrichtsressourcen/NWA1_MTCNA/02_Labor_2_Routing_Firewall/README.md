# Laborübung 2: Routing & Firewall  <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Voraussetzungen](#1-voraussetzungen)
- [2. Lernziele](#2-lernziele)
- [3. Hinweise](#3-hinweise)
- [4. Vorbereitung](#4-vorbereitung)
  - [4.1. Grundkonfiguration R1](#41-grundkonfiguration-r1)
  - [4.2. Grundkonfiguration R2](#42-grundkonfiguration-r2)
  - [4.3. Grundkonfiguration R3](#43-grundkonfiguration-r3)
- [5. Static Routing](#5-static-routing)
  - [5.1. Ressourcen](#51-ressourcen)
  - [5.2. Ziele](#52-ziele)
  - [5.3. Zwingende Bedingungen](#53-zwingende-bedingungen)
  - [5.4. Hinweise](#54-hinweise)
  - [5.5. Takeaways](#55-takeaways)
    - [5.5.1. Route flags](#551-route-flags)
- [6. Connection Tracking](#6-connection-tracking)
  - [6.1. Ressourcen](#61-ressourcen)
- [7. Firewall](#7-firewall)
  - [7.1. Aufbau einer Standard-Firewall Konfiguration](#71-aufbau-einer-standard-firewall-konfiguration)
    - [7.1.1. Rahmenbedingungen](#711-rahmenbedingungen)
    - [7.1.2. Exkurs: Umsetzung von Sicherheitsmassnahmen mit der Firewall](#712-exkurs-umsetzung-von-sicherheitsmassnahmen-mit-der-firewall)
    - [7.1.3. Anti-Lockout](#713-anti-lockout)
    - [7.1.4. Interface-Lists](#714-interface-lists)
    - [7.1.5. Established, Related](#715-established-related)
    - [7.1.6. Block all from WAN](#716-block-all-from-wan)
    - [7.1.7. Drop invalid](#717-drop-invalid)
    - [7.1.8. Forward](#718-forward)
    - [7.1.9. IPv4-FastTrack](#719-ipv4-fasttrack)
    - [7.1.10. Zusammenfassung](#7110-zusammenfassung)
    - [7.1.11. Ports gezielt öffnen](#7111-ports-gezielt-öffnen)
  - [7.2. IP-Blocklisten](#72-ip-blocklisten)
    - [7.2.1. Vertiefungsfragen](#721-vertiefungsfragen)
    - [Ressourcen](#ressourcen)
- [8. NAT](#8-nat)
  - [8.1. Ressourcen](#81-ressourcen)
  - [8.2. Masquerade](#82-masquerade)
  - [8.3. Port-Forwarding](#83-port-forwarding)
  - [8.4. Port-Trigger Regeln (Mangle)](#84-port-trigger-regeln-mangle)
- [9. Autor](#9-autor)
- [10. Lizenz](#10-lizenz)

# 1. Voraussetzungen
 - Hat Laborübung 1: Mikrotik Grundlagen, Bridging, DHCP und DNS gelöst
 - Kennt die Elemente und Funktionen des IP-Protokolls (MAC- und IP-Adressen, IP-Adressklassen, private Adressen, Netzmasken, Routing, Adress Resolution Protocol (ARP)).
 - Kennt Gründe für die Aufteilung eines Netzwerks in IP-Subnetze.
 - Kennt die Algorithmen zur (binären) Berechnung von IP-Subnetzen.
 - Kennt die Unterschiede zwischen statischem und dynamischem Routing.
 - Kann statisches Routing auf Routern konfigurieren

# 2. Lernziele
 - Kann ein kleines IPv4-Subnetz planen
 - Kann IPv4-Adressen und statische Routen auf einem MikroTik-Router konfigurieren
 - Kennt alle Route Flags
 - Kennt Connection Tracking und kann dessen Funktion erklären
 - Kann alle Regeln einer Standard-Firewall Konfiguration erklären
 - Kann den Router mit einer einfachen Firewall-Konfiguraiton absichern
 - Kennt den Unterschied zwischen einer *Forward-* und einer *Input-Chain*

# 3. Hinweise
 - In dieser Laborübung gibt es nur teilweise Schritt-für-Schritt Anleitung. Es wird erwartet, dass die Teilnehmer selbstständig recherchieren. 
 - In den Aufgaben stehen jeweils Links zu weiterführenden Quellen und Hinweise zu nützlichen Befehlen. 
 - Login MikroTik: *admin* Passwort: *keines gesetzt* ==> Immer Passwort *admin* setzen!


# 4. Vorbereitung

**💪 Challenge 1:** In GNS3 ist folgendes Laborsetup aufzubauen (nachfolgende Hinweise beachten):

![Laborsetup 1](media/Laborsetup2.PNG)

Hinweise zum Laborsetup:
 - Bei `Management` handelt es sich um das `Device` bzw. Komponente `Cloud`
 - Bei `Internet` handelt es sich um das `Device` bzw. Komponente `nat`
 - Für `R1` ist die neuste Verfügbare `MikroTik 7.*` Version zu verwenden.
 - Internet `nat` muss umbedingt an Port `ether1` angeschlossen werden!
 - Auf `R1`, `R2` und `R3` muss jeweils die unten aufgeführte Grundkonfiguration vorgenommen werden. 

## 4.1. Grundkonfiguration R1
```
/ip dhcp-client add add-default-route=no interface=ether2 use-peer-dns=no
/system/identity/set name=R1
/ip firewall nat add action=masquerade chain=srcnat out-interface=ether1
/ip service set telnet disabled=yes
/ip service set ftp disabled=yes
/ip service set www disabled=yes
/certificate/add common-name=router name=router country=CH
/ip service set api disabled=yes
/ip service set api-ssl disabled=yes
```

Die nachfolgenden Befehle jeweils separat ausführen:
```
/certificate/sign router
/ip service set www-ssl certificate=router disabled=no
```

## 4.2. Grundkonfiguration R2
```
/ip dhcp-client/remove ether1
/system/identity/set name=R2
```

## 4.3. Grundkonfiguration R3
```
/ip dhcp-client/remove ether1
/system/identity/set name=R3
```

**Kontrollfrage:** Sind Sie in der Lage jede vorgenommene Konfiguration in der Grundkonfiguration zu erklären, bzw. haben Sie verstanden was die Grundkonfiguration beinhaltet?

---

# 5. Static Routing

*Statisches Routing mit MikroTik RouterOS umsetzen.*

**💪 Challenge 2:** Konfiguriere VPCS1, R1, R2 und R3, sodass die nachfolgend aufgeführten Ziele erreicht und die zwingenden Bedingungen eingehalten werden. 

## 5.1. Ressourcen
 - 🎥 [MikroTik 2 Minuten Anleitung zu *static Routing*](https://www.youtube.com/watch?v=ZpAY_6RDuRA)
 - 📄 [MikroTik Help: IP Routing](https://help.mikrotik.com/docs/display/ROS/IP+Routing)
 - [Visual Subnet Calculator](https://www.davidc.net/sites/default/subnets/subnets.html)

## 5.2. Ziele
 - `VPCS1` erhält per DHCP eine IP-Adresse, hat Zugriff aufs Internet und kann vom eigenen Laptop angepingt werden
 - Per WinBox auf dem eigenen Laptop kann auf R3 und R2 zugegriffen werden.
 - Alle Geräte sind via IPv4 vom Subnetz auf `br0` (192.168.23.0/24) erreichbar. 


## 5.3. Zwingende Bedingungen
 - Auf `R3 ether8` ist ein `/24` privates Subnetz konfiguriert
 - Auf den Links `R3 ether5 <=> R2 ether5` und  `R3 ether4 <=> R2 ether4` ist jeweils ein `/30` Subnetz konfiguriert
 - Alle Subnetze (ausser zu nat0 und br0 zugehörige Subnetze) können in einem `/22` privaten Subnetz (gem. [RFC 1597](https://www.rfc-editor.org/rfc/rfc1597)) zusammengefasst werden, sodass von eigenen Laptop nur eine statische Route konfiguriert werden muss. 
 - Keine zusätzlichen Verbindungen zwischen den Geräten angelegen

## 5.4. Hinweise
Für statisches Routing unter MikroTik werden lediglich zwei Äste in der Konfigurationshierarchie benötigt: 
 - `/ip address` : Konfigurieren von IPv4 Adressen auf interfaces
 - `/ip route` - Übersicht über alle vorhandenen Routen und Konfiguration von statischen Routen



## 5.5. Takeaways
 - Default Route nach `0.0.0.0/0`: Eine Route, wohin aller Verkehr gesendet wird, für den keine spezifischere Route definiert ist.

### 5.5.1. Route flags
![Route Flags](media/RouteFlags.PNG)
<br>*Abbildung 2: Ausschnitt aus `ip route` Fenster in WinBox. Sichtbare Spalten: Route flags und Dst.Address*

Routen in der Routing Tabelle können verschiedenen Ursprung haben. Informationen zu der Herkunft und Art der Route werden in den Route Flags angegeben:
 - `A` = active
 - `X` = disabled
 - `C` = connected
 - `D` = dynamic
 - `S` = static
 - `I` = inactive
 - `d` = (Quelle) DHCP
 - `o` = (Quelle) OSPF
 - `v` = (Quelle) VPN
 - `b` = (Quelle) BGP
 - `r` = (Quelle) RIP

![Route DHCP](media/DHCPRoute.PNG)
<br>*Abbildung 3: Aktive Default Route dynamisch durch DHCP-Client hinzugefügt.*

In der *Abbildung 3* ist eine Default Route sichtbar, die durch den DHCP-Client in die Routing Tabelle hinzugefügt wurde. Ob der DHCP-Client eine Default Route hinzufügen soll, kann über den Parameter `add-default-route` im Konfigurationsmenü `/ip dhcp-client` gesteuert werden. 

---

# 6. Connection Tracking

![Firewall Connections](media/ConnectionTracking.PNG)
<br>*Abbildung 4: Übersicht über alle aktiven Verbindungen, welche der Router zu diesem Zeitpunkt "tracked".*

Grundsätzlich ist IPv4 stateless. Das bedeutet, dass jede Datenübertragung unabhängig von vorherigen stattfindet und der Zustand einer Verbindung nicht gespeichert wird bzw. es keine Verbindung gibt. Jedes Paket wird also ohne Informationen über den Zustand anderer Pakete behandelt. 

Das stellt uns vor eine Herausforderung, wenn wir Technologien wie [NAT (Network Address Translation)](https://de.wikipedia.org/wiki/Netzwerkadress%C3%BCbersetzung) einsetzen möchten. Dafür müssen wir die Verbindungen auf dem Router nachverfolgen (tracking). Jeder Router der NAT unterstützt muss entsprechend *Connection Tracking* unterstützen. Dabei ist zu beachten, dass *Connection Tracking* nicht in jedem Fall gewünscht ist, denn Connection Tracking beansprucht Systemressourcen. Bei High-Performance Anwendungen, wie bei einem Core-Router in einem Rechenzentrum, empfiehlt es sich auf *Connection Tracking* und damit verbundenen Technologien zu verzichten. 

Weiter wird *Connection Tracking* benötigt, wenn eine Firewall abhängig von der Verbindungsaufbaurichtung Filterentscheidungen treffen muss. Dazu muss der Router nicht nur die Header des IP Protokolls analysieren, sondern auch die Header der darüberliegenden Transportprotokolle wie TCP, UDP und ICMP. Eine Liste der Funktionen, die auf *Connection Tracking* angewiesen sind, kann [hier](https://help.mikrotik.com/docs/display/ROS/Connection%20tracking#Connectiontracking-Featuresaffectedbyconnectiontracking) gefunden werden. 

![Firewall Connection Tracking Configuration](media/ConnectionTrackingConfig.PNG)
<br>*Abbildung 5: Connection Tracking konfiguration*

Standardmässig ist Connection Tracking auf `auto`. Gemäss Handbuch: `This means that connection tracing is disabled until at least one firewall rule is added.`

## 6.1. Ressourcen
 - [MikroTik Help Connection Tracking](https://help.mikrotik.com/docs/display/ROS/Connection+tracking)


---

# 7. Firewall

Eine Firewall ist ein Netzwerksicherheitsfunktion, welche Datenverkehr zwischen Netzwerkwerken filtert. Sie arbeitet nach Regeln, die sequenziell analysiert werden, bis eine erste Übereinstimmung gefunden wird. In RouterOS werden die Firewall-Regeln in den Bereichen Filter und NAT verwaltet. Die Firewall-Filterung beinhaltet drei Chains: 'input' für eingehende Daten zum Router, 'output' für ausgehende Daten vom Router und 'forward' für Daten, die den Router durchlaufen. Jede Regel ist mit einer Aktion verknüpft, die festlegt, was bei Übereinstimmung mit einem Paket passieren soll: 'accept' für die Annahme, 'drop' für stilles Verwerfen oder 'reject' für die Zurückweisung mit einer ICMP-Ablehnungsnachricht sowie 'jump/return' für den Sprung zu oder die Rückkehr von einer benutzerdefinierten Kette. 

![Firewall Chains](media/MikroTikChains.png)
<br>*Abbildung 6: Übersicht über alle Chains innerhalb von RouterOS. Quelle: [MikroTik Help Filter](https://help.mikrotik.com/docs/display/ROS/Filter)*

In erster Linie ist der Unterschied zwischen der *Input-* und *Forward-Chain* zu verstehen:
 - *Input-Chain*: Betrifft alle Pakete/Verbindungen welche auf Services vom Router selbst zugreifen (z.B. Wireguard, Web-Interface via HTTPS, DNS-Server, usw. )
 - *Forward-Chain:* Betrifft alle Pakete/Verbindungen welche der Router "weiter-routet" bzw. weiterleitet. 

Es gibt zwei grundsätzliche Möglichkeiten eine IP-Firewall-Filter zu konfigurieren: 
 - Erlaube spezifischen Traffic und "droppe" den Rest
 - "Droppe" ungewünschten Traffic und erlaube den Rest

## 7.1. Aufbau einer Standard-Firewall Konfiguration

Nachfolgend bauen wir eine Standard-Firewall Konfiguration auf. Dabei wird jede vorgenommene Konfiguration erklärt. 

Die Konfiguration nehmen wir auf dem Router R1 vor. 

### 7.1.1. Rahmenbedingungen
 - Management und Internet sind WAN: Nicht vertrauenswürdig
 - Das Netzwerk interne /22 gilt als LAN: vertrauenswürdig. 

### 7.1.2. Exkurs: Umsetzung von Sicherheitsmassnahmen mit der Firewall
`Zero Trust Security`: Während früher Ressourcen Geräten im LAN ohne weitere Authentifizierung zur Verfügung gestellt wurden, gilt heute Zero Trust Security. Das bedeutet: Misstrauen gegenüber allen: In einem Zero Trust-Netzwerk wird grundsätzlich niemandem oder nichts vertraut. Alle Benutzer, Geräte und Anwendungen werden als potenzielle Sicherheitsrisiken betrachtet. (vgl. [Wikipedia Zero Trust Security](https://de.wikipedia.org/wiki/Zero_Trust_Security)). Eine Netzwerkarchitektur sollte heutzutage immer nach den Zero Trust Security Grundprinzipien aufgebaut werden. Bei der Firewall Konfiguration bedeutet das: Nur das Zulassen, was wirklich benötigt wird. Teilweise kann dieser Grundsatz jedoch auch dazu führen, dass Netzwerkadministratoren jeglichen Datenverkehr Port-basiert (z.B. ins Internet) blockieren. Dies bringt nur eine "Schein"-Sicherheit und die für den Nutzer resultierenden Unannehmlichkeiten stehen in keinem Verhältnis. 
Eine günstige Art bekannte Gefahren zu blockieren, ist es IP-Adressen von einer IP-Blockliste wie [FireHOL](https://iplists.firehol.org/) zu blockieren oder einen DNS-Filter (PiHole) einzusetzen. Das schützt nicht vor gezielten Hackerangriffen und Überwachungen durch Staaten, verringert jedoch das Risiko durch versehentliche Zugriffe oder unautorisierte Zugriffe von automatischen Bots. 

### 7.1.3. Anti-Lockout

Beim Konfigurieren von Netzwerkgeräten ist es schnell passiert: Falsche Konfiguration vorgenommen und - Zack! - Zugriff auf die Konfigurationsoberfläche verloren. 

Um das zu verhindern, empfiehlt es sich als erstes eine *Anti-Lockout* Regel anzulegen. Diese wird auf dem *Input-Chain* angelegt, das der Zugriff auf Services des Routers erlaubt werden soll. 

```
/ip firewall filter add action=accept chain=input comment=Anti-Lockout src-address=192.168.23.0/24
```

![Anti-Lockout](media/antilockoutfirewall.PNG)
<br>*Abbildung 7: CLI /ip firewall filter print: Anti-Lockout Regel ist Regel Nr. 1*

In einer Firewall werden die Regel der Reihe nach abgearbeitet. Das bedeutet, dass die Reihenfolge der Regel relevant ist! Die Anti-Lockout-Regel ist an erster Stelle ("0"). Wird mit `add` eine weitere Regel hinzugefügt, so wird diese jeweils am Schluss angefügt. Mit dem Parameter `place-before` kann dies gesteuert werden: `place-before=0` hat zum Beispiel zur Folge, dass die Regel als oberste Regel eingefügt wird. 

### 7.1.4. Interface-Lists

Damit Firewall-Regeln, welche als Kriterien *Interfaces* haben, besser organisiert werden können, empfiehlt es sich diese *Interface-Lists* anzulegen. 

In unserem Beispiel sind `ether1` und `ether2` WAN. Mit 
```
/interface/list/add name=WAN
```
wird eine neue Liste angelegt und mit 
```
/interface/list/member/add interface=ether1 list=WAN
/interface/list/member/add interface=ether2 list=WAN
```
die beiden "physischen"-Schnittstellen `ether1` und `ether2` dieser neuen Liste zugeordnet. 

![Interface-List](media/interfacelist.PNG)
<br>*Abbildung 8: Winbox mit Interface-List-Member Verwaltung*

**Challenge 3:** Erstelle eine Liste LAN und füge alle Schnittstellen hinzu, welche erstens aktiv sind und zweitens zum LAN gehören. 

### 7.1.5. Established, Related
![Interface-List](media/interfacelist.PNG)
<br>*Abbildung 9: Ohne Connection-Tracking und Connection-State ist es nicht möglich Antworten als solche zu identifizieren*

Alle Pakete, die vom WAN kommen, sollen blockiert werden, aber die *Replies* von *Request*, die (in unserem Fall) von einem Gerät im LAN initiiert wurde, soll die Firewall durchlassen. Ausschlaggebend dabei ist die Verbindungsrichtung: Der Router verfolgt dank *Connection Tracking* alle Verbindungen und weiss, wer die Verbindung initial gestartet hat und welche Pakete zu welchen Verbindungen gehören. 

Damit die Firewall diese *Reply-Packages* auch durchlässt, wird eine entsprechende Firewall Regel benötigt:
```
/ip firewall filter add action=accept chain=input comment="input: allow established, related" connection-state=established,related
```
Wenn Packet einer Verbindung zugeordnet werden kann, welche sich im Zustand `established` oder `related` ist, dann lasse das Paket zu. 

### 7.1.6. Block all from WAN

Alle Pakete, die von Seiten WAN initiiert werden, wollen wir 
blockieren:

```
/ip firewall filter add action=drop chain=input comment="input: default drop all from WAN" in-interface-list=WAN
```

![Firewall Input-Chain](media/firewallinputonly.PNG)
<br>*Abbildung 10: Winbox: Firewall mit drei Regeln auf der Input-Chain.*

Da die Regel für `established, related` vor der `default drop` Regel kommt, werden nur Pakete blockiert, die nicht bestehender Verbindungen zugeordnet werden können. 

### 7.1.7. Drop invalid

Alle Pakete, die einer Verbindung zugeordnet werden, welche sich in einem ungütligen Zustand befindet, sollen nicht zugelassen werden. 

```
/ip firewall filter add action=drop chain=input comment="input: drop invalid" connection-state=invalid
```

Diese Regel muss vor die Regel `"input: allow established, related"`. In Winbox und in der WebGui kann die Regel mit der Maus einfach an die entsprechende Position gezogen werden. 

![Firewall Input-Chain with Invalid Rule](media/firewallinputdropinvalid.PNG)
<br>*Abbildung 11: Winbox: Firewall mit vier Regeln auf der Input-Chain.*

### 7.1.8. Forward

**💪 Challenge 4:** Für die Forward-Chain (also alle Pakete die der Router weiterleitet) müssen dieselben Firewall Regeln (wie in der Input-Chain) angelegt werden. Eine Anti-Lockout Regel ist nicht notwendig. Lege analog Input-Chain die drei Firewall-Regeln an. 

Ob die Firewallkonfiguration funktioniert, kann wie folgt getestet werden:
 - `VPCS1` kann nicht mehr vom eigenen Laptop angepingt werden. 
 - `VPCS1` kann weiterhin ins Internet pingen
 - Der Zugriff auf R2 und R3 per Winbox ist nicht mehr möglich.

### 7.1.9. IPv4-FastTrack

Die Verarbeitung jedes einzelnen Paketes in der Firewall ist ressourcenintensiv. Das `IPv4 FastTrack`-Feature erlaubt es Verbindungen für FastTrack zu markieren, was die CPU des Routers entlastet (Mehr dazu auf [Manual:IP/FastTrack](https://wiki.mikrotik.com/wiki/Manual:IP/Fasttrack)).

![Ethernet test results for CCR2004-1G-12S+2XS](media/fastpathccr2004.PNG)
<br>*Abbildung 12a: [Ethernet test results for CCR2004-1G-12S+2XS](https://mikrotik.com/product/ccr2004_1g_12s_2xs#fndtn-testresults)*

Für fast alle Geräte stellt MikroTik Benchmarking-Werte zur Verfügung. In der Abbildung 12a sind sind *Ethernet test results* für den *CCR2004-1G-12S+2XS* dargestellt. Der Unterschied ohne und mit FastTrack (fast path) (Werte gelb markiert) ist dabei signifikant. 

Damit unsere Konfiguration FastTrack verwenden kann muss eine weitere Regel eingefügt werden:

```
/ip firewall filter add action=fasttrack-connection chain=forward comment="forward: fasttrack established, related" connection-state=\
    established,related hw-offload=yes
```

![FastTrack Connection Rule](media/forwardfasttrack.PNG)
<br>*Abbildung 12b: Winbox firewall filter configuation with the fasttrack connection rule highlighted* 

Die `forward: fasttrack established, related`-Regel muss zwigend vor die `forward: allowed established, related` gezogen werden, sonst werden die Verbindungen nie für FastTrack markiert, weil die andere Regel zuerst greift. 

### 7.1.10. Zusammenfassung
Die Firewallkonfiguration umfasst nun insgesamt 8 Filter-Regeln, jeweils 4 für den Input-Chain und 4 für den Forward-Chain. Diese Standardkonfiguration ist in ähnlicher Form standardmässig auf jedem MikroTik Router (Hardware) installiert.

### 7.1.11. Ports gezielt öffnen

**💪 Challenge 5:** Alle Pakete in Verbindungsrichtung (WAN => LAN) sind aktuell blockiert. Erstelle eine Firewall-Regeln, welche *Address-lists* verwendet und den Zugriff via SSH auf R2 und R3 vom Subnetz 192.168.23.0/24 erlauben. 

[MikroTik Help Address-lists](https://help.mikrotik.com/docs/display/ROS/Address-lists)

**Kontrollfrage:** Wurde die Firewall nach dem Prinzip "default drop" oder "default accept" konfiguriert?

## 7.2. IP-Blocklisten

Der Einsatz von IP-Blocklisten wie [FireHOL](http://iplists.firehol.org/) und [Geoblocking](https://de.wikipedia.org/wiki/Geoblocking) ist eine effektive Methode, um unerwünschte Zugriffe durch automatisierte Bots zu minimieren. Diese Tools ermöglichen es Administratoren, IP-Adressen oder ganze Regionen, die als Ursprung von Cyberangriffen bekannt sind, proaktiv zu blockieren. Durch das Blockieren dieser Adressen wird das Volumen des bösartigen und unnötigen Verkehrs erheblich reduziert, wodurch die Belastung der Netzwerk- und Rechenressourcen verringert wird. Dies führt zu einer effizienteren Nutzung der Systemkapazitäten, da zum beispielsweise Server hinter einer Firewall weniger damit beschäftigt sind sinnlose Anfragen von irgendwelchen Bots zu beantworten. Es ist jedoch wichtig zu beachten, dass solche Massnahmen das Risiko von gezielten Angriffen (siehe [National Cyber Security Center - How cyber attack work](https://www.ncsc.gov.uk/information/how-cyber-attacks-work)) **nicht** verringern. Angreifer, die spezifische Ziele verfolgen, können ohne weiteres solche Blockierungen umgehen. Daher sollten IP-Blocklisten und Geoblocking als Teil einer umfassenderen Sicherheitsstrategie angesehen werden, die verschiedene Abwehrmechanismen umfasst, um eine robuste Verteidigung zu gewährleisten.

Im Netz sind verschiedene Anbieter solcher Blocklisten zu finden. 

Eine nützliche Quelle ist die [IP-Blockliste von FireHOL](http://iplists.firehol.org/). FireHOL setzt ihre Blockliste aus verschiedenen Quellen automatisch zusammen und aggregiert die IP-Ranges, um die Länge der Blockliste so kurz wie möglich zu halten. 

Diese IP-Blocklisten können automatisch per Skript in RouterOS hinterlegt werden. 

**💪 Challenge 7a:** In Repo [mikrotikgeoip](https://gitlab.com/muqiuq/mikrotikgeoip) werden wöchentlich aktualisierte GeoIP und Blocklisten zur Verfügung gestellt. Verwende die *Abbildung 12c* und *Abbildung 12d*, sowie die Hinweise im Repo, um die FireHol Blockliste im Router `R1` zu hinterlegen. 

![Schedule](media/schedulefirehol.png)
<br>*Abbildung 12c: New schedule window in winbox* 

![Schedule](media/blocklistaddresslist.png)
<br>*Abbildung 12d: addresslist containing the entries from the FireHol Level 1 Blocklist* 

**💪 Challenge 7b:** Damit die Blockliste funktioniert, müssen noch entsprechende Einträge in den *Filter Rules* hinterlegt werden. Einmal auf dem Input-Chain und einmal auf der Forwarding-Chain. Wichtig dabei zu beachten ist, dass diese Regeln **nach** der Anti-Lockout Regeln hinzugefügt werden. Hinterlege die beiden *Drop Rules* in den *Filter Rules* in den entsprechenden Chains. 

### 7.2.1. Vertiefungsfragen
 - Weshalb sollten die Blocklisten Drop Regeln immer nach den Anti-Lockout Regeln eingefügt werden? 
 - Welche Risiken gehen durch die Verwendung des Repositories [mikrotikgeoip](https://gitlab.com/muqiuq/mikrotikgeoip) als Quelle für die IP-Blocklisten aus?
 - Ist die Aktualisierung der Blocklisten einmal wöchentlich häufig genug? 
 - Weshalb bringt es nichts die *Schedule* in RouterOS häufiger auszuführen, um die Blocklisten aktueller zu behalten (z.B. jede Stunde)?

### Ressourcen
 - 📕 [MikroTik Help - Scheduler](https://help.mikrotik.com/docs/display/ROS/Scheduler)
 - 📕 [MikroTik Help - Scripting](https://help.mikrotik.com/docs/display/ROS/Scripting)

---

# 8. NAT
Unter `/ip firewall nat` können NAT Regeln angelegt werden. Zwei typische NAT-Operationen die häufig verwenden werden, werden in diesem Abschnitt behandelt. Dazu gehören: 
 - Masquerade: Source-IP durch ausgehende IP des Routers ersetzen und Source-Port auf freien Port mappen (Siehe [Wikipedia Port Address Translation](https://de.wikipedia.org/wiki/Port_Address_Translation))
 - Port-Forwarding: Eigehende Pakete auf eine andere IP-Adresse und einen anderen Port weiterleiten (Siehe: [Wikipedia Portweiterleitung](https://de.wikipedia.org/wiki/Portweiterleitung))

Sowohl bei Firewall-Filter-Regeln als auch bei NAT-Regeln gibt es zwei Elemente: Kriterien, die von den Paketen erfüllt werden müssen, damit die Regel angewendet wird, und die Aktion, die daraufhin durchgeführt wird. Sofern in der Regel nicht `passthrough=yes` angewählt ist, werden das Paket nicht mit weiteren Regeln abgeglichen. Bei den Firewall-Filter-Regeln ist die typischen Aktionen `accept` und `drop`. Die NAT geht dabei ein wenig weiter: Pakete werden nicht nur weitergeleitet oder verworfen, sondern manipuliert. Wie die Pakete manipuliert werden, muss ebenfalls festgelegt werden. 

## 8.1. Ressourcen
 - [MikroTik Help: NAT](https://help.mikrotik.com/docs/display/ROS/NAT)
 - [Video:  How to port forward on MikroTik ](https://www.youtube.com/watch?v=a_8AV6vIDYQ)

## 8.2. Masquerade

In der zu Beginn des Labors vorgenommenen Grundkonfiguration wurde bereits eine NAT-Regel eingefügt:

```
/ip firewall nat add action=masquerade chain=srcnat out-interface=ether1
```
Erklärung: Alle Pakete, welche den Router über das Interface `ether1` verlassen, führe die Aktion `masquerade` bzw. [Wikipedia Port Address Translation](https://de.wikipedia.org/wiki/Port_Address_Translation) durch.

![Connection Tracking NAT example](media/connectiontrackingpingnat.PNG)
<br>*Abbildung 13: Detailansicht in Connecting Tracking: ICMP Ping bei dem die Source-Addresse durch die IPv4-Adresse des Routers ersetzt wurde.* 

![Connection Tracking TCP example](media/connectiontrackingtcpnat.PNG)
<br>*Abbildung 14: Detailansicht einer TCP-Verbindung bei der eine Masquerade NAT-Regel angewendet wurde.* 

Je nach Transportprotokoll müssen weitere Werte manipuliert und anschliessend verfolgt werden. In der *Abbildung 14* sind die Verbindungsdetails eine TCP-Verbindung aufgelistet. Je nach Router-Software ist die Portvergabe anderst implementiert. In diesem Beispiel konnte der Router denselben Port nehmen wie bereits der Client verwendet hat. Das muss nicht zwigend so sein. Denn wenn der Router den Port bereits für eine andere Verbindung verwendet, muss er einen anderen freien Port nehmen. Jede IP-Addresse + Port Kombination ist einzigartig. 

## 8.3. Port-Forwarding

**💪 Challenge 7:** Konfiguriere Port-Forwarding, sodass die der Port `2222` auf `R1` den Port `22` von `R3` weitergeleitet wird. Der Challenge ist abgeschlossen, wenn erfolgreiche eine SSH Verbindung über diese Portweiterleitung auf `R3` aufgebaut werden kann. 

**Hinweis**: Damit die durch die NAT weitergeleiteten Pakete nicht in der Forward-Chain blockiert werden, müssen diese in der `forward: default drop WAN` ausgeschlossen werden. Dafür müssen muss eine zusätzliche Bedingung in diese Regel eingefügt werden: Paket darf nicht den NAT-Status `dstnat` haben. Siehe *Abbildung 15*. 

![Connection NAT State Konfiguration](media/connectionnatstate.PNG)
<br>*Abbildung 15: Winbox Connection NAT State Konfiguration einer Filter-Firewall-Regel* 

## 8.4. Port-Trigger Regeln (Mangle)

Eine einfache Art Ressourcen auf dem Router zu sparen, ist es unerwünschte Verbindungsanfragen so früh wie möglich zu blockieren. Bots im Internet klappern systematisch IP-Adressen ab, um unter anderem Informationen zu Services zu sammeln oder nach Sicherheitslücken zu suchen. Bekannt sind Suchmaschinen wie [Shodan.io](https://www.shodan.io/) auf denen nach Services oder IP-Adresen gesucht werden kann (Siehe [Github - Awesome Shodan Search Queries](https://github.com/jakejarvis/awesome-shodan-queries)). 

Um mit den "dümmeren" Bots keine Ressourcen zu verschwenden, können IP-Adressen, die sich versuchen auf gewissen Ports zu verbinden automatisch blockiert werden. Ein Port-Scan klappert typischerweise alle Standard-Ports ab. Dazu gehört unter anderem FTP (21/TCP), SSH (22/TCP), PPTP (1723/TCP), POP3 (110/POP3). Wenn die Ports nicht bereits für Services verwendet werden, können verwendet werden um Port-Scans zu detektieren und zu blockieren. 

Dafür legen wir eine neue Firewall-Regel bzw. Mangle-Regel an:

```
/ip firewall mangle add action=add-src-to-address-list address-list=portscanblock address-list-timeout=1d chain=prerouting comment="port scan detection rule" dst-port=21,1723,110 in-interface-list=WAN protocol=tcp
```
Erklärung: Pakete die auf einem WAN-Interface ankommen, das Protokoll TCP haben und als Destination-Port 21, 1723 oder 110 haben, sollen der Address-Liste `portscanblock` für einen Tag (=1d) hinzugefügt werden.

![Port-Scan](media/portscan.PNG)
<br>*Abbildung 16: Im Hintergrund Winbox Fenster auf der die Adress-Liste portscanblock mit einem Eintrag eingeblendet ist. Im Vordergrund nmap-Scan-Ausgabe.* 

Getestet werden kann die Regel am einfachsten mit einem Port-Scan mit [nmap](https://nmap.org/). 

Beispiel:
```bash
nmap -v -Pn 192.168.23.148
```

**💪 Challenge 8:** Lege auf der *Input-* und *Forward-Chain* eine Firewall-Regel an, die alle Pakete blockiert, dessen Source-IP sich in der *Address-list* `portscanblock` befindet. 

---

# 9. Autor

Philipp Albrecht <philipp.albrecht@tbz.ch> - https://github.com/alptbz


---

# 10. Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a> and [GNU GENERAL PUBLIC LICENSE version 3](https://www.gnu.org/licenses/gpl-3.0.en.html). If there are any contradictions between the two licenses, the Attribution-NonCommercial-ShareAlike 4.0 International license governs. 