# Laborübung 3: Wireless & QoS  <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Struktur](#1-struktur)
- [2. Vorkenntnisse](#2-vorkenntnisse)
- [3. Voraussetzungen](#3-voraussetzungen)
- [4. Lernziele](#4-lernziele)
- [5. Übersicht](#5-übersicht)
- [6. Router Vorbereitungen](#6-router-vorbereitungen)
  - [6.1. Hardware-Reset](#61-hardware-reset)
  - [6.2. Zugriff mit Winbox](#62-zugriff-mit-winbox)
  - [6.3. Standard-Konfiguration entfernen](#63-standard-konfiguration-entfernen)
- [7. Wireless Interfaces](#7-wireless-interfaces)
- [8. Internet-Uplink via Wireless](#8-internet-uplink-via-wireless)
  - [8.1. Sicherheitsprofil anlegen](#81-sicherheitsprofil-anlegen)
  - [8.2. Repetition WLAN-Sicherheitsstandards](#82-repetition-wlan-sicherheitsstandards)
  - [8.3. WiFi Scanner](#83-wifi-scanner)
  - [8.4. Wireless Interface (2.4 GHz) Konfiguration](#84-wireless-interface-24-ghz-konfiguration)
  - [8.5. Layer 3 Connectivity](#85-layer-3-connectivity)
  - [8.6. Zusammenfassung](#86-zusammenfassung)
- [9. Input - WLAN Kanäle](#9-input---wlan-kanäle)
- [10. Input - DFS](#10-input---dfs)
- [11. Router-Router WLAN Link (WLAN A)](#11-router-router-wlan-link-wlan-a)
  - [11.1. Vorbereitungen und Hinweise](#111-vorbereitungen-und-hinweise)
  - [11.2. Challenge 5 - OSI-Layer 1 and 2](#112-challenge-5---osi-layer-1-and-2)
  - [11.3. Challenge 6 - OSI-Layer 3](#113-challenge-6---osi-layer-3)
  - [11.4. Zusammenfassung - WLAN A](#114-zusammenfassung---wlan-a)
- [12. Registration](#12-registration)
- [13. Access List](#13-access-list)
- [14. 2.4 GHz Access Point - WLAN B](#14-24-ghz-access-point---wlan-b)
- [15. Default Forward](#15-default-forward)
- [16. Simple \& Advanced Mode](#16-simple--advanced-mode)
- [17. Radio Name](#17-radio-name)
- [18. Wireless Snooper](#18-wireless-snooper)
- [19. Connect List](#19-connect-list)
- [20. Quality of Service](#20-quality-of-service)
  - [20.1. Simple Queue](#201-simple-queue)
    - [20.1.1. Limit](#2011-limit)
      - [20.1.1.1. Aktuelle maximale Übertragungsrate messen](#20111-aktuelle-maximale-übertragungsrate-messen)
      - [20.1.1.2. Simple Queue Regel anlegen](#20112-simple-queue-regel-anlegen)
      - [20.1.1.3. Maximale Übertragungsrate messen mit aktiver Simple Queue](#20113-maximale-übertragungsrate-messen-mit-aktiver-simple-queue)
    - [20.1.2. Garantie](#2012-garantie)
      - [20.1.2.1. Exkurs: IP-Adressen ermitteln und statische Zuweisungen im DHCP-Server hinterlegen](#20121-exkurs-ip-adressen-ermitteln-und-statische-zuweisungen-im-dhcp-server-hinterlegen)
      - [20.1.2.2. Garantie pro IP-Adresse](#20122-garantie-pro-ip-adresse)
    - [20.1.3. Burst](#2013-burst)
  - [20.2. Per Connection Qeueing](#202-per-connection-qeueing)
- [21. Repetition Routing](#21-repetition-routing)

# 1. Struktur
 - Wireless Client Konfiguration, Verbinden mit LERNKUBE
 - 2.4 GHz Input, Connect List Input, Security Input
 - Country Regulation
 - Wireless Client Konfiguration zu zweit, einer macht einen AP in 5 GHz und einer macht einen Client in 5 GHz
 - 5 GHz Input, DFS Input, Registration Table Input, Access List Input
 - Input zu *Default Authenticate*, *Default Forward*
 - *Quality of Service*: *Simple Queues* und *Per Connection Queue*

# 2. Vorkenntnisse
 - Kennt die gängigen WLAN-Standards.
 - Kennt die erforderlichen Arbeitsschritte und Komponenten, um eine einfache WLAN-Vernetzung einzurichten.
 - Alle Kenntnisse aus Labor 1 und 2

# 3. Voraussetzungen
 - Pro Person 1 x [hAP ac2](https://mikrotik.com/product/hap_ac2) oder ähnlich
 - Mindestens 2 Personen pro Labor

# 4. Lernziele
 - Versteht die Überlappung im 2.4 GHz Band und kann entsprechend die richtigen Kanäle auswählen
 - Kann auf einem MikroTik Router ein *Wireless Interface* als *Access Point* oder *Station* konfigurieren
 - Kann ein 802.11 *Wireless Network* mit zeitgemässen Sicherheitsmassnahmen absichern
 - Weiss wo *Country Regulations* hinsichtlich WLAN Frequenzbänder nachgeschaut werden können und kann die *Country Regulations* in einem *Wireless Interface* konfigurieren
 - Kann DFS erklären und kann es bei der Planung berücksichtigen

# 5. Übersicht

**Wichtig: Das nachfolgende Laborsetup wird in der Laborübung Schritt-für-Schritt aufgebaut. Bauen Sie zu diesem Zeitpunkt noch NICHTS auf.**

![Laborsetup](media/LabSetup.PNG)
<br>*Abbildung 1: Übersicht Laborsetup im finalen Zustand.*

**Wichtig:** Die nachfolgenden Schritte sind auf dem eigenen Router durchzuführen. Die Gruppenarbeit beginnt erst zu einem späteren Zeitpunkt. 

# 6. Router Vorbereitungen

ür die Durchführung der Laborübungen 1 und 2 wurde GNS 3 genutzt, um die Übungen in einer virtuellen Umgebung zu aufzubauen. In der Laborübung 3 wird das erste Mal in diesem Kurs mit MikroTik Hardware gearbeitet. Um diese Übung durchführen zu können, muss der Router zuerst vorbereitet werden.

**Hinweis:** Nachfolgend wird der Begriff "Router" für den *hAP ac2* verwendet. Jedoch handelt es sich hierbei nicht um einen reinen Layer 3 IP-Router, sondern um ein Universalgerät, dass je nach Konfiguration ein oder mehrere Funktionen übernehmen kann. Umgangssprachlich hat sich der Begriff "Router" für solche Universalgeräte durchgesetzt.


## 6.1. Hardware-Reset

Die nachfolgenden Laborübungen bauen auf einer leeren Router-Konfiguration auf. Da der Router eventuell vorher von anderen Studierenden verwendet wurde, empfiehlt es sich den Router zuerst auf Werkeinstellungen zurückzusetzen. 

**💪 Challenge 1**: Setze den Router mithilfe der Hardwaretaste zurück (Resetting the RouterOS configuration). Anleitung. [MikroTik Help - Reset Button](https://help.mikrotik.com/docs/display/ROS/Reset+Button)


## 6.2. Zugriff mit Winbox

Verbinde deinen eigenen Laptop / PC mit dem Router und greife mithilfe von [Winbox](https://mikrotik.com/download) auf den Router zu. 

![Winbox Neighbors](media/WinboxNeighbors.PNG)
<br>*Abbildung 2: Unter Neighbors werden alle im Netzwerk gefundenen MikroTik Geräte angezeigt, sofern im Router [Discovery Protokolle](https://help.mikrotik.com/docs/display/ROS/Neighbor+discovery) aktiviert sind.*

Sofern keine Firewall auf dem eigenen Laptop Winbox blockiert, wird das eigene Gerät unter *Neighbors* (siehe Abbildung 2) angezeigt. Mit einem Klick auf die Mac-Adresse des Eintrages, wird die Mac-Adresse in die Verbindungsparameter übernommen. 

## 6.3. Standard-Konfiguration entfernen

Die Standard-Konfiguration auf dem **hAP ac2** eignet sich hervorragend als Grundlage für ein kleines Firmennetzwerk oder ein Heimnetzwerk. Nebst den Standard IP-Konfigurationen (DHCP-Client auf WAN und DHCP-Server auf LAN), ist auch die Firewall vorkonfiguriert. Für das Labornetzwerk ist diese Standard-Konfiguration **unpassend** und soll deshalb entfernt werden. Nach dem ersten Login erscheint das in *Abbildung 3* dargestellt Fenster. Da die Standard-Konfiguration in unserem Fall unerwünscht ist, wird diese mit einem Klick auf *Remove Configuration* gelöscht. Anschliessend empfiehlt es sich die Verbindung neu aufzubauen (Oben Links: *Session* -> *Disconnect*, danach neu verbinden). 

![Winbox Remove Konfiguration](media/RemoveConfiguration.PNG)
<br>*Abbildung 3: Beim ersten Einloggen in den Router via Winbox erscheint das dargestellt *Default Configuration* Fenster.*

**Tipp:** Falls im Fenster aus *Abbildung 3* aus Versehen auf *OK* gedrückt wurde, kann die Konfiguration unter *System* -> *Reset Configuration* zurückgesetzt werden. **Wichtig:** Unbedingt die Checkboxen *No Default Configuration* and *Do Not Backup* markieren!

# 7. Wireless Interfaces

Sofern keine Konfiguration auf dem Wireless-Router vorliegt, sind die *Wireless Interfaces* normalerweise deaktiviert. Der **hAP ac2** hat zwei *Wireless Interfaces*: Ein Interface auf dem 2.4 GHz-Band und ein Interface auf dem 5 GHz-Band. Jedes dieser Interfaces kann unabhängig voneinander konfiguriert und aktiviert werden. (der *hAP ax lite* hat nur ein 2.4 GHz Interface. Die Challenges in diesem Labor bei der Verwendung dieses Netzwerkgerätes angepasst gelöst werden. Informieren Sie sich bei Unsicherheiten bei der Lehrperson.)

![Winbox Wireless Interfaces](media/WirelessInterfacesDeactivated.PNG)
<br>*Abbildung 4: Wireless Interfaces in einem hAP ac2*

# 8. Internet-Uplink via Wireless

Im ersten Schritt soll ein Uplink ins Internet über das Wireless-Interface konfiguriert werden. Das zu verwendete Netz *LERNKUBE* befindet sich im 2.4 GHz Band. Damit die Verbindung zu diesen WLAN klappt, muss zuerst die Passphrase in einem Sicherheitsprofil hinterlegt werden. 

## 8.1. Sicherheitsprofil anlegen

![Winbox Security Profile](media/SecurityProfile.PNG)
<br>*Abbildung 5: Security Profile für LERNKUBE*

**💪 Challenge 2:** Erstelle ein neues *Security Profile* für die SSID LERNKUBE (Siehe Abbildung 5). Es empfiehlt sich dabei als Name des *Security Profile* die SSID zu setzen. Jedoch muss dieser Name *nicht* zwangsläufig mit der SSID übereinstimmen.

## 8.2. Repetition WLAN-Sicherheitsstandards

1. Wie werden die nachfolgenden Sicherheitsstandards und Authentifizierungsmethoden in einem Security Profile hinterlegt?
2. Welche der Sicherheitsstandards sollten nicht mehr verwendet werden?
3. Welche Authentifizierungsmethode eignet sich am besten, wenn sich die User jeweils mit Benutzername und Passwort authentifizieren sollen?
4. Gibt es neuere (bessere) Sicherheitsstandards? Wenn ja, welche?

**Sicherheitsstandards und Authentifizierungsmethoden:**
 - WPA2-PSK
 - WPA-PSK
 - WPA2-EAP
 - WPA-EAP
 - WEP
 - EAP

## 8.3. WiFi Scanner

MikroTik bietet einige Tools an, die für eine professionelle Wireless Konfiguration von Wichtigkeit sind. Ein Tool ist dabei der *Scanner*. Mithilfe des Scanners können unter anderem die Frequenzen und Signalstärken der WLANs identifiziert werden, welche im Empfangsbereich des Routers liegen. 

![Winbox Scanner](media/Scanner.PNG)
<br>*Abbildung 6: Scanner aktiv im 2.4 GHz Band*

**💪 Challenge 3:** Nutzer den *Scanner* um das *LERNKUBE* WLAN zu finden und klicke anschliessend auf *Connect*, um die Verbindungsdaten zu übernehmen. 

## 8.4. Wireless Interface (2.4 GHz) Konfiguration

![Winbox Wireless Interface Konfiguration unvollständig](media/WirelessInterfaceConfigurationIncomplete.PNG)
<br>*Abbildung 7: Unvollständige Wireless Interface Konfiguration*

Nachdem die Aufgabe aus *Challenge 3* ausgeführt wurde, schauen wir uns die Interface Konfiguration von wlan 1 genauer an (Quelle: [MikroTik Help - Wireless](https://help.mikrotik.com/docs/display/ROS/Wireless)):

 - **Mode:** Ein WLAN-Client wird als *Station* bezeichnet. Wenn sich MikroTik Router untereinander via WLAN Verbinden, kann der Modus *station bridge* verwendet werden. Dieser erlaubt es, das WLAN Interface anschliessend mit einer virtuelle Bridge zu verbinden. Bei einer normalen *Station* ist das üblicherweise nicht möglich. Im Modus *Station* muss die IP-Adresse direkt auf dem Interface konfiguriert werden. In diesem Übungsteil konfigurieren wir den *Mode* auf *Station*. 
 - **Band:** Die WLAN Normen 802.11b (1999) und 802.11g (2003) sind veraltet. Leider sind immer noch Access Points in Betrieb, welche diese WLAN Normen verwenden. Das beeinträchtigt die Performance aller WLANs in Reichweite von solchen Access Points, weil alle WLAN Geräte mit performancemindernden Algorithmen auf diese alten WLAN-Standards Rücksicht nehmen müssen. In diesem Übungsteil konfigurieren wir das *Band* auf die neuste verfügbare Norm *2GHz-only-N*.
 - **Channel Width**: Im 2 GHz-Band können zwei Kanäle parallel verwendet werden, um die mögliche Übertragungsrate zu verdoppelt. Sofern der Access Point dies zur Verfügung stellt und es die Signalstärke erlaubt, wird automatisch ein zweiter Kanal aktiviert, wenn in *Channel Width* *20/40MHz* gewählt wird. *Ce* bedeutet, dass der zusätzliche Kanal oberhalb des *Haupt-Kanals* liegt und *eC* bedeutet, dass der zusätzliche Kanal unterhalb der *Haupt-Kanals* liegt. *XX* bedeutet, dass die *Station* die zusätzlichen Kanäle automatisch konfigurieren soll. In unserem Beispiel ist *20/40 MHz Ce* auszuwählen.
 - **Frequency**: Damit eine *Station* von Access Point zu Access Point "roamen" (siehe [IEEE 802.11r-2008](https://en.wikipedia.org/wiki/IEEE_802.11r-2008), eine Erweiterung die schnelleres Roaming erlaubt) kann, muss das WLAN-Interface die Frequenz automatisch wählen können. Durch die Verwendung der Funktion *Connect* im *Scanner* wurde die aktuelle Frequenz automatisch übernommen. Bewegt sich die WLAN Station zu einem anderen Access Point, der dasselbe Netzwerk (gleiche SSID) zur Verfügung stellt oder ändert der Access Point die Frequenz, kann sich die *Station* nicht mehr verbinden. Im Normalfall empfiehlt es sich deshalb bei *Frequency* (im *Station*-Modus) *auto* auszuwählen.
 - **SSID**: "SSID (service set identifier) is a name that identifies wireless network." Mit diesem Netzwerk soll sich die Station verbinden (Siehe [Wikipedia SSID](https://en.wikipedia.org/wiki/Service_set_(802.11_network)#SSID)).
 - **Security Profile**: Hier kann hinterlegt werden, welches Sicherheitsprofil zu verwenden ist. Die dort hinterlegte Konfiguration wird zur Authentifizierung beim Access Point bzw. in WLAN verwendet. Als *Security Profile* ist in diesem Übungsteil das neu erstelle *Security Profile* "LERNKUBE" zu verwenden. 
 - **Frequency-Mode:** Aus dem [MikroTik-Wiki](https://wiki.mikrotik.com/wiki/Manual:Interface/Wireless): <br>"Three frequency modes are available:<br> - **regulatory-domain**: Limit available channels and maximum transmit power for each channel according to the value of country<br> - **manual-txpower** - Same as above, but do not limit maximum transmit power.<br> - **superchannel** - Conformance Testing Mode. Allow all channels supported by the card.<br>List of available channels for each band can be seen in /interface wireless info allowed-channels. This mode allows you to test wireless channels outside the default scan-list and/or regulatory domain. This mode should only be used in controlled environments, or if you have special permission to use it in your region."<br> In Frequency-Mode sollte **immer** *regulatory-domain* gewählt werden. 
 - **Country:** Unter Land ist immer das Land zu wählen in dem sich der Access Point bzw. die Station befindet. 
 - **Installation:** Bei einer *Station*, die sich bewegen kann, kann bei *Installation*: *any* gewählt werden. 

**Achtung:** Das [BAKOM](https://www.bakom.admin.ch/bakom/de/home/telekommunikation/technologie/wlan.html) regeln in der Schweiz die Verwendung der Frequenzen und viele weitere Aspekte der drahtlosen Kommunikation. Der Betrieb einer Sendeanlage, welche gegen Vorschriften verstösst, kann zu sehr hohen Bussen führen (Siehe [Artikel 52 im Fernmeldegesetz](https://www.fedlex.admin.ch/eli/cc/1997/2187_2187_2187/de#art_52)). Es empfiehlt sich sehr, das ["Faktenblatt WLAN" vom BAKOM](https://www.bakom.admin.ch/dam/bakom/de/dokumente/bakom/telekommunikation/Technologie/WLAN/factsheet_wlan.pdf.download.pdf/factsheet_wlan.pdf) zu lesen und sich mit den Schweizerischen Regeln zum Betrieb eines WLANs vertraut zu machen. 

![Connected To Ess](media/ConnectedToEss.PNG)
<br>*Abbildung 8: Verbindungsstatus in der Wireless Interface Konfiguration*

Sobald alle Einstellungen wie beschrieben vorgenommen wurden, kann mit einem klick auf *Apply* die Konfiguration übernommen werden. Ob sich die *Station* erfolgreich mit dem *Access Point* verbunden hat, ist in der Statusleiste zu erkennen. *connected to ess* bedeutet, dass sich die *Station* erfolgreich verbunden hat. 

## 8.5. Layer 3 Connectivity

Die Konfiguration von Layer 1 und Layer 2 des OSI-Modells ist nun abgeschlossen. Einzig die IP-Adresse und die NAT Konfiguration wird noch benötigt, dass der *hAP ac2* per IPv4 mit anderen Geräten kommunizieren kann, respektive sich ins Internet verbinden kann. 

**💪 Challenge 4:** Konfiguriere ein DHCP-Client auf dem *wlan1 interface* (bzw. auf dem Interface, welches im vorherigen Abschnitt mit *LERNKUBE* verbunden wurde) und Konfiguriere eine NAT Regel *MASQUERADE* für alle Verbindungen die auf dem *wlan1 interface* abgehen. Teste anschliessend mit *ping* ob Konnektivität mit Hosts im Internet möglich ist. 


## 8.6. Zusammenfassung

Nach diesem Abschnitt hat der Router bzw. hAP ac2 über WLAN eine Verbindung ins Internet. Die Grundlagen zum Modus Station, zu den Sicherheits-Normen und Verordnungen wurden vermittelt. Im ersten Schritt wurde ein Uplink über das 2.4 GHz Wireless-Interface konfiguriert, wobei das Netz LERNKUBE im 2.4 GHz Band genutzt. Ein neues Security Profile für die SSID LERNKUBE wird erstellt und die Sicherheitsnormen repetiert. In der Wireless Interface Konfiguration wurden wichtige Einstellungen wie Mode, Band, Channel Width, und Security Profile erklärt und anschliessend angepasst. Wichtige regulatorische Vorgaben wurden vermittelt. Abschließend wird die Layer 3 Konnektivität durch die Konfiguration eines DHCP-Clients und einer NAT-Regel MASQUERADE auf dem wlan1 interface hergestellt, um die Internetverbindung zu ermöglichen.

Blicken wir auf die Übersichtsgrafik (*Abbildung 1*): Wir haben haben nun den Teil "WLAN UPLINK" erledigt. 


# 9. Input - WLAN Kanäle

Im vorherigen Abschnitt wurden bereits die regulatorischen Vorschriften der Schweiz erwähnt. Eine Möglichkeit sich einen Überblick über die verfügbaren bzw. nutzbaren Kanäle zu verschaffen ist der Wikipedia Artikel [List Of WLAN channels](https://en.wikipedia.org/wiki/List_of_WLAN_channels). 

Dieser führt alle WLAN Frequenzen bzw. Kanäle, zeigt wie diese gebündelt werden können (z.B. bei 5 GHz 20, 40, 80, 160 MHz) und in welchen Ländern welche Kanäle verwendet werden dürfen und welche weiteren Regulatorischen Vorschriften beachtet werden müssen. 

*Wissensfragen:* 
  1. Welche Kanäle sollen im 2.4 GHz Band verwendet werden und welche nicht? 
  2. Weshalb sollten nur diese Kanäle verwendet werden?
  3. Was sind die Folgen, wenn sich jemand nicht daran hält und einen "falschen" Kanal wählt?

# 10. Input - DFS

*Dynamic Frequency Selection* ist ein Mechanismus der im 5 GHz Frequenzbereich eingeführt wurde, um zu verhindern, dass WLAN-Geräte in diesem Frequenzbereich Wetterradarsysteme nicht stören. Der Einsatz von DFS hat weitreichende Konsequenzen und dessen Geschichte ist ein interessantes Thema zu dem es unterhaltsame Präsentationen und Talks gibt: 

 - [Radar Detect and DFS on MikroTik](https://www.youtube.com/watch?v=ZbrbRUAfpac)
 - [DFS - The Untold Story | David Coleman | WLPC Phoenix 2020](https://www.youtube.com/watch?v=qCzknrd_F8o)
 - [The 5GHz “Problem” For Wi-Fi Networks: DFS](http://wifinigel.blogspot.com/2018/05/the-5ghz-problem-for-wi-fi-networks-dfs.html)

**Zu merken:** Für alle 5 GHz-Kanäle für die DFS vorgeschrieben ist, aktiviert MikroTik DFS automatisch, wenn das korrekte Land ausgewählt ist und in *Frequency-Mode* *regulatory-domain* ausgewählt ist. DFS hat zur Folge, dass der Access Point beim starten zuerst aktive Wetterradare detektieren muss und später im Betrieb den Kanal wechseln muss, wenn ein Wetterradar detektiert wird. Es ist deshalb von Vorteil dem Access Point im 5 GHz Bereich immer mehere Kanäle zur Verfügung zustellen, die er Verwenden kann, wenn Kanäle aus den *U-NII-2A* und *U-NII-2C* Bändern verwendet werden. 

# 11. Router-Router WLAN Link (WLAN A)

In diesem Abschnitt soll das "WLAN A" (*Siehe Abbildung 1*) erstellt konfiguriert werden. Dafür wird das noch freie 5 GHz *Wireless Interface* (wlan2) verwendet. 

## 11.1. Vorbereitungen und Hinweise
 - Lese zuerst alle Vorbereitungen, Hinweise und den Challenge 5 durch bevor du mit dem Konfigurieren beginnst. 
 - Damit sich die Kursteilnehmer nicht gegenseitig stören und muss eine einzigartige *SSID* für das "WLAN A" ausgewählt werden. 
 - Als Passphrase soll "securemikrotik" verwendet werden. 
 - Authentifzierung: WPA2-PSK (Zusätzliches *Security Profile* anlegen*)
 - Es sollen auschliesslich Kanäle verwendet werden, die für *Indoor use* zugelassen sind. 
 - Gruppenbildung: Ein Gruppenmitglied konfiguriert sein Router als *Access Point* und alle anderen konfigurieren ihre Router als *Station*. Vor Übungsbeginn sind diese Rollen zuzuteilen. 

## 11.2. Challenge 5 - OSI-Layer 1 and 2

Wähle den nachfolgenden Challenge abhängig von der Rolle in der Gruppe: 

**💪 Challenge 5 (Access Point):** Plane gemeinsam mit deiner Gruppe das "WLAN A" (siehe Vorbereitungen). Konfiguriere anschliessend das 5 GHz *Wireless Interface* (wlan2) als *Access Point* (*ap bridge*). Der Challenge gilt als abgeschlossen, wenn sich alle *hAP ac2* der anderen Gruppenmitglieder erfolgreich verbunden haben. 

**💪 Challenge 5 (Station):** Plane gemeinsam mit deiner Gruppe das "WLAN A" (siehe Vorbereitungen). Konfiguriere anschliessend das 5 GHz *Wireless Interface* (wlan2) als *Station* (*ap bridge*). Der Challenge gilt als abgeschlossen, wenn du dich erfolgreich mit dem WLAN Netzwerk verbunden hast (*connected to ess*).

## 11.3. Challenge 6 - OSI-Layer 3

Wähle den nachfolgenden Challenge abhängig von der Rolle in der Gruppe: 

**💪 Challenge 6 (Access Point):** Lege eine Bridge an, konfiguriere ein /24 Subnetz auf der Bridge (nicht 192.168.0.0/24, nicht 192.168.1.0/24, nichts aus 172.16.0.0/12). Füge das in das *Challenge 5* konfigurierte *WLAN Interface* der Bridge hinzu. Konfiguriere einen DHCP-Server auf der angelegten Bridge. 

**💪 Challenge 6 (Station):** Deaktiviere das im vorherigen Abschnitt konfigurierte *wlan1* (WLAN UPLINK) *interface*. Konfiguriere einen DHCP-Client auf dem neu Konfiguriereten *wlan2* (WLAN A, *Abbildung 1*). Passe die Masquerade-Regel an, sodass neu *wlan* für *Outgoing Interface* verwendet wird. Der Challenge gilt als Abgeschlossen, wenn Hosts im Internet vom Router *Student 2* aus erfolgreich angepingt werden können. 

## 11.4. Zusammenfassung - WLAN A

Der Router von *Student 2* hat nun einen Internet-Uplink über zwei WLAN Netzwerke in unterschiedlichen Frequenzbänder hinweg aufgebaut. Zwischen "WLAN A" und "WLAN UPLINK" ist der Router von *Student 1*, der einerseits als *Access Point* für "WLAN A" fungiert und andererseits als *Station* mit dem WLAN "LERNKUBE" verbunden ist. Das Routing wird mithilfe von automatisch angelegten Default-Routen (durch den DHCP-Client) und NAT-Regeln sichergestellt. 

# 12. Registration

![Registration Table](media/RegistrationTable.PNG)
<br>*Abbildung 9: Registration zeigt alle Verbunden Stations wie auch die eigene Registration bei einem Access Point an.*

Im Tab *Registration* können alle erfolgreich aufgebauten WLAN Verbindungen überwacht werden. Nützliche Informationen wie Signalstärke und Tx- und Rx-Rate werden für jede Verbundene Station bzw. für jede Verbindung zu einem Access Point angezeigt. 

# 13. Access List

Standardmässig können sich alle *Stations* mit einen *Access Point* verbinden, solange die richtigen Authentifzierungsinformationen bekannt sind. Mithilfe von Access Lists kann die Authentifzierung zusätzlich mithilfe der MAC-Adresse eingeschränkt werden. Dabei gilt zu beachten, dass dies die Sicherheit nicht wirklich erhöht. 

![Signal Information](media/RegistrationTable.PNG)
<br>*Abbildung 10: Signal-Informationen zu einem Eintrag in der Registration Table*

**💪 Challenge 7 (Student 1, Access Point):** Füge mithilfe der *Registration*-Tabelle den Routers von *Student 2* in die Access Liste ein (*Copy to Access List*). Deaktiviere anschliessend in der Konfiguration des *wlan2* Interfaces *Default Authenticate*. In der *Access List* kann anschliessend der angelegte Eintrag deaktiviert werden, was zur Folge hat, dass sich Router von *Student 2* trennt bzw. nicht mehr mit dem WLAN verbinden kann. War dies erfolgreich, kann der *Access List*-Eintrag anschliessend wieder aktiviert werden, sodass sich der Router von *Student 2* wieder verbinden kann. 

# 14. 2.4 GHz Access Point - WLAN B

Im letzten Schritt soll auf dem Router von *Student 2* ein WLAN Netzwerk im 2.4 GHz Band konfiguriert werden. In diesem sollen sich Usergeräte mit dem WLAN verbinden können.

**💪 Challenge 8 (Student 2):** Konfiguriere das 2.4 GHz *wlan1* Interface zu einem Access Point um. Dafür muss eine neue eindeutige SSID gewählt werden. Als Passphrase soll diesselbe wie in "WLAN A" verwendet werden. Weiter soll gleich im Layer 3 die IP-Konfiguration vorgenommen werden. D.h. IPv4-Subnetz definieren, Bridge anlegen, IPv4-Adresse auf Bridge konfigurieren, DHCP-Server konfigurieren auf Bridge. Der Challenge ist abgeschlossen, wenn ein mit dem neu erstellten WLAN verbundenes Gerät (z.B. eigenen Smartphone) Zugriff ins Internet hat

# 15. Default Forward
Ist *Default Forward* in der *Wireless Interface* Konfiguration gesetzt (Standardmässig aktiv), können *Stations* innerhalb des WLANs untereinander kommunizieren. Ist diese Option deaktiviert, können die Clients untereinander nicht kommunizieren. Performancetechnisch ist es hinsichtlich der vielen Discovery-Protokolle, die im Netzwerk aktiv sind, von Vorteil insbesondere in grossen Firmennetzwerken Client-To-Client Kommunikation zu verbieten: Jedes Consumer-Gerät, insbesondere Windows- und Mac-Computer, ist sehr mitteilungsbedürftig: Die Geräte broadcasten ständig die Services, welche sie anbieten. Diese Funktion ist z.B. für [AirPrint](https://de.wikipedia.org/wiki/AirPrint) relevant, damit das Tablet z.B. den Drucker im Netzwerk finden kann. Dasselbe gilt für Geräte wie Chromecast, die nur über Discovery Protokolel verbunden werden können. In grösseren Netzwerken kann der durch Discovery-Protokolle verursachte *Traffic* die Network-Performance wesentlich verschlechtern. (Siehe [Packet Pushers: How mDNS Can Kill Wi-Fi Performance And What To Do About It](https://packetpushers.net/podcast/hw016-how-mdns-can-kill-wi-fi-performance-and-what-to-do-about-it/)). 

In kleinen Firmennetzwerken mit wenigen Geräten und in Heimnetzwerken ist *Default Forward* jedoch meistens notwendig, da (leider) sehr viele Geräte nur über solche Discovery-Protokolle verwendet werden können.

![Default Forwarding](media/DefaultForwarding.PNG)
<br>*Abbildung 11: Winbox: Default Forwarding Checkbox in Wireless Interface => Wireless*

# 16. Simple & Advanced Mode
Das Wireless Interface kann in zwei Modi konfiguriert werden:
 - Simple Mode: Nur die wesentlichen Einstellungen werden angezeigt
 - Advanced Mode: Alle Einstellungen werden angezeigt

# 17. Radio Name

Im *Advanced Mode* kann zusätzlich der *Radio Name* gesetzt werden. Dieser ist nur für andere MikroTik Geräte sichtbar. In der Abbildung 6 können mit dieser Information MikroTik Geräte erkannt werden: Nur bei einem Teil der Geräte wird der *Radio Name* angezeigt. Das Setzen des *Radio Name*s bringt vor allem dem Netzwerk Administrator Vorteile: Er kann *Stations* und *Access Points* einfacher identifizieren, indem zum Beispiel eine aussagekräftige ID im *Radio Name* hinterlegt wird. 

Siehe [Help Mikrotik - Registration Table](https://help.mikrotik.com/docs/display/ROS/Wireless+Interface#WirelessInterface-RegistrationTable)

# 18. Wireless Snooper
Von [Help Mikrotik - Wireless Snooper](https://help.mikrotik.com/docs/display/ROS/Wireless+Interface#WirelessInterface-Snooper) "This tool monitors surrounding frequency usage, and displays which devices occupy each frequency. It's available both in console, and also in Winbox. Snooper will use frequencies from scan-list."

Ein effektives Werkzeug zur Analyse der Auslastung von drahtlosen Netzwerken nach Frequenz, ideal zur Auswahl der optimalen Frequenz für die eigene WLAN-Infrastruktur.

# 19. Connect List
Eine *Wireless Station* verbindet sich mit jedem *Access Point* bei der SSID mit der einprogrammierten übereinstimmt und die Authentifizierung erfolgreich ist. In den nachfolgenden Fällen, ist es von Vorteil, wenn dieses Verhalten beeinflusst werden kann:
 - Die *Station* ist Teil einer beweglichen Einheit (z.B. Stapler in einem Lager). Es kann zum Beispiel vorkommen, dass an einer Stelle im Lager sich die *Station* immer mit einem *Access Point* verbindet, der Übertragungstechnisch supoptimal ist. Mit den entsprechenden *Connect List* Einträgen kann dieses Verhalten beeinflusst werden. 
 - *Point-zu-Point* Links: WLAN Links mit Richtantennen: Die *Station* soll sich nur mit der Antenne auf der gegenüberliegenden Seite verbinden und mit keiner anderen. 

![Connect List](media/ConnectList.PNG)
<br>*Abbildung 12: Winbox: Connect List Eintrag*

# 20. Quality of Service
Im Netzwerk gibt es keine Übertragungsgarantien. Pakete können auf dem Weg verloren gehen. Sei es, weil sie aus elektronischen Grüden auf einem Kupfer-Netzwerk-Patchkabel kaputt gegangen sind, oder weil ein Switch an seine Kapazitätsgrenze gekommen ist. Unter den Begriff *Quality of Service* fallen alle Technologien, die Einfluss auf die Entscheidungen der Prioriesierung der Datenpakete nehmen. Während das defekte Patchkabel im einfachsten Fall einfach ausgetauscht werden kann, können insbesondere bei begrenzten Ressourcen durch Algorithmen und Regeln auf Routern und Switches Einfluss genommen werden. Zum Beispiel wenn es darum geht, die Audiostreaming Qualität der Sprachanrufe zu gewährleisten. RouterOS hat verschiedene QoS Methoden integriert, darunter gehören [*Traffic Shaping*](https://en.wikipedia.org/wiki/Traffic_shaping) oder *Traffic Prioritisation* Technologien. Allgemein Fallen diese Mechanismen unter [Bandwith Management](https://en.wikipedia.org/wiki/Bandwidth_management).

Die nachfolgend erklärten Funktionen können in den folgenden Use Cases vom Nutzen sein:
 - Die Übertragungsrate für einen Server soll garantiert werden, umabhängig vom restlichen Datenverkehr.
 - Die Bandbreite für das Gastnetzwerk soll limitiert werden. 
 - Die Übertragunsrate pro Verbindung (*per connection*) limitieren

## 20.1. Simple Queue

### 20.1.1. Limit
Im aktuelle Laborsetup stellt der Router *Student 2* ein WLAN im 2.4 GHz Band zur Verfügung. Für gewisse Geräte soll die Bandbreite limitiert werden. 

#### 20.1.1.1. Aktuelle maximale Übertragungsrate messen
Verbinde ein beliebiges Gerät (z.B. einer der Notebooks oder ein Smartphone) mit dem WLAN von Router *Student 2*. Führe einen Speedtest durch (z.b. mit [speedtest.net](https://www.speedtest.net/)). Merke dir den ungefähren Wert. 

#### 20.1.1.2. Simple Queue Regel anlegen
![Simple Queue](media/SimpleQueue.PNG)
<br>*Abbildung 13: Simple Queue regel mit einer Limite von 1 Mbit/s*

Unter dem Menü *Queues* kann unter dem Tab *Simple Queues* eine neue Regel wie in *Abbildung 13* dargestellt anzulegen. *Target* ist dabei auf das eigenen Subnetz zu setzen. 

#### 20.1.1.3. Maximale Übertragungsrate messen mit aktiver Simple Queue
Wiederhole nun den Speedtest. Die gemessene Übertragungsrate sollte nun auf 1Mbit/s beschränkt sein. 

### 20.1.2. Garantie
Nun soll die Bandbreite nicht nur begerenzt werden, sondern einzelnen Teilnehmern soll eine gewisse Bandbreite zugesichert werden. Um diesen Test durchzuführen, müssen mindestens zwei Geräte mit dem WLAN vom Router *Student 2* verbunden werden. 

#### 20.1.2.1. Exkurs: IP-Adressen ermitteln und statische Zuweisungen im DHCP-Server hinterlegen
Damit sich die IP-Adresse der Clients über die weitere Laborübung nicht verändert, kann im DHCP-Server eine statische Zuweisung hinterlegt werden, sodass dieser immer dieselbe IP-Adresse einem bestimmten Gerät zuteilt. Damit diese Zuweisung einfach erstellt werden kann, ist es von Vorteil, wenn die Geräte bereits mit dem Netzwerk verbunden ist und eine IP-Adresse dynamisch vergeben wurde. 

Unter *IP* => *DHCP Server* => *Leases* werden alle statischen und dynamischen Leases aufgeführt. 

Mit einem Klick auf einen Eintrag, kann dieser geöffnet werden. Mit dem Button *Make Static* kann dieser Eintrag in einen statischen Eintrag umgewandelt werden. *Merke:* Der Eintrag kann nicht sofort bearbeitet werden, sondern das Fenster muss zuerst geschlossen und anschliessend neu geöffnet werden. 

![DHCP Server Leases](media/DHCPServerDHCPLease.PNG)
<br>*Abbildung 14: DHCP-Server leases mit einem dynamischen Lease geöffnet im Vordergrund*

Es empfiehlt sich bei statischen *DHCP Leases* jeweils die *Client ID* nicht als Kriterium zu setzen, da diese sich je nach implementation des DHCP-Clients ändern kann. Das hat dann zufolge, dass der DHCP-Server eine neue IP-Adresse zuteilt. 
Deaktiviere die *Client ID* aus dem gerade erstellen statischem Lease (Siehe *Abbildung 15*).

![DHCP Server Leases](media/DHCPLease.PNG)
<br>*Abbildung 15: Statischer DHCP-Lease. Gelb markiert ist der Button, welcher gerade gedrückt wurde, um die Client ID als Paramter zu deaktivieren.*

**💪 Challenge 9 (Student 1):** Richte auf deinem Router einen statische DHCP-Lease für den Router von *Student 1* ein. 

**💪 Challenge 9 (Student 2):** Richte auf deinem Router mindestens zwei statische DHCP-Leases für die im Beginn des Abschnittes *Garantie*  verbundenen Geräte ein. 

#### 20.1.2.2. Garantie pro IP-Adresse
Mit einer simple Queue kann nicht nur die Bandbreite begrenzt werden, sondern auch eine minimale Bandbreite garantiert werden. In diesem Beispiel wird die Bandbreite für alle Geräte auf 10 MBit/s beschränkt. Ein Gerät soll eine Garantie von 8 MBit/s erhalten und das andere Gerät eine Garantie von 1 Mbit/s. Bei einem gleichzeitigen Speedtest, müssten anschliessend genau diese beiden Werte erreicht werden (sofern die maxial mögliche Übertragungsrate 10 Mbit/s erreicht oder besser übersteigt).

![LimitAt](media/LimitAt.PNG)
<br>*Abbildung 15: Drei konfigurierte Simple Queues, wobei zwei der Simple Queues der queue1 untergeordnet sind.*

**💪 Challenge 10 (Student 2):** Richte die Simple Queues wie in *Abbildung 15* dargestellt ein. Einem Gerät werden massiv höhere Bandbreiten zugesichert, als dem anderen Gerät. Führe anschliessend auf beiden Testgeräten (*Stations*) Speedtests durch. 

1. Kannst du feststellen, dass einem Gerät eine höhere Übertragungsrate zugesichert wird, als dem anderen? 
2. Wenn nur eines der beiden Geräte die Bandbreite ausnutzt, müsste dieses jeweils die gesamte Bandbreite von 10M ausnutzen können. Ist das der Fall?

### 20.1.3. Burst
Ein weiterer Use Case ist, dass die Bandbreite grundsätzlich den Clients zur Verfügung stehen soll (sodass z.B. eine Webseite schnell geladen wird), jedoch verhindert werden soll, dass einzelne Clients die Bandbreite über eine längere Zeit auslasten. 

Dafür ist der Mechanismus Burst äusserst nützlich. Dieser erlaubt es, dass die Bandbreitenbegrenzung (*Max Limit*) für eine gewisse Dauer (*Burst Time*) bis zu einer bestimmten Grenzen (*Burst Limit*) überschritten werden darf. Der Countdown für diese Dauer startet, wenn ein bestimmter Schwellenwert (*Burst Threshold*) überschritten wird. 

## 20.2. Per Connection Qeueing
Mit vorherigen Abschnitt wurde mit Simple Queues für ein ganzes Subnetz und für einzelne Hosts die Bandbreite eingeschränkt bzw. garantiert. Der *pcq* (*Per Connection Queuing*)-Mechanismus erlaubt es pro Verbindung eine Limite zu setzen. 

In diesem Beispiel soll auf auf dem Bridge Interface der Standard Queue-Type durch einen *custom queue type* ersetzt werden, welcher jede Verbindung auf 1 Mbit/s begrenzt. 

In der *Abbildung 16* ist dargestellt, wie eine neuer Queue Type vom Tip *pcq* angelegt wird, der jede Verbindung aufgrund der Src. Adresse auf 1 Mbit/s begrenzt. Soll einem einzelnen Host mehere Verbindungen à 1 Mbit/s erlaubt werden, muss beim *Classifier* zusätzlich der *Src. Port* als Kriterium miteinbezogen werden. 

![PCQ](media/PCQ.PNG)
<br>*Abbildung 16: Anlegen einer enuen Queue-Type*


 # 21. Repetition Routing

**💪 Challenge 10:** Aktuell ist auf Router von *Student 1* und *Student 2* *Masquerade* aktiviert. Die Verwendung von *Masquerade* benötigt CPU-Ressourcen. In diesem Laborsetup kann eine dieser Masquerade-Regeln weggelassen werden. Dafür müssen zusätzliche Routen angelegt werden. Ziel: Richte den Router von *Student 2* so ein, dass keine Masquerade-Regeln mehr konfiguriert sind. 