# MikroTik - MTNCA

Dieser Kursteil umfasst die Grundlagen, welche für die MikroTik MTCNA Zertifizierung benötigt werden. 

Weitere Informationen zum Inhalt: [Certified Network Associate (MTCNA) - Training outline](https://i.mt.lv/cdn/training_pdf/mtcna_outline_2019181161836.pdf)

Die einzelne Labore wurden durch zusätzliche Kursinhalte (über die MTCNA Zertifizierung hinweg) erweitert. 


## Labore

 - [Laborübung 1: Mikrotik Grundlagen, Bridging, DHCP und DNS](01_Labor_1_DHCP_DNS/README.md)
 - [Laborübung 2: Static Routing & Firewall](02_Labor_2_Routing_Firewall/README.md)
 - [Laborübung 3: Wireless & QoS](03_Labor_3_Wireless_QoS/README.md)
 - [Laborübung 4: PPP VPN Tools](04_Labor_4_PPP_VPN/README.md)


