# WireGuard Konfiguration mit Python

Anstatt mit [wgcfghelp](https://github.com/muqiuq/wgcfghelp) kann die Übung auch mit einem Python Script gelöst werden.

Im Repository [alptbz/mikrotikwireguardrestexample](https://github.com/alptbz/mikrotikwireguardrestexample) ist ein vereinfachtes Skript abgelegt, welches in der Lage ist neue Peers in RouterOS anzulegen und Client-Konfigurationsdateien zu generieren. 

Der Zugriff auf die REST-API von MikroTik kann nur über *https* erfolgen. Deshalb muss auf Router *R1* zuerst HTTPs aktiviert werden. 

Die nachfolgenden Befehle jeweils separat ausführen:
```
/certificate/add common-name=router name=router country=CH
/certificate/sign router
/ip service set www-ssl certificate=router disabled=no
```

**Challenge 6:** Verwende das Python-Skript um fünf Wireguard Peers auf Router R1 zu erstellen. 