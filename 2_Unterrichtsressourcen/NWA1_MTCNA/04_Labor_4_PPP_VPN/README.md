# Laborübung 4: Tunnelprotokolle und Tools <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Vorkenntnisse](#1-vorkenntnisse)
- [2. Voraussetzungen](#2-voraussetzungen)
- [3. Lernziele](#3-lernziele)
- [4. Vorbereitung](#4-vorbereitung)
  - [4.1. Grundkonfiguration R1](#41-grundkonfiguration-r1)
  - [4.2. Grundkonfiguration R2](#42-grundkonfiguration-r2)
  - [4.3. Grundkonfiguration VPC1](#43-grundkonfiguration-vpc1)
  - [4.4. Grundkonfiguration VPC2](#44-grundkonfiguration-vpc2)
- [5. Netwatch](#5-netwatch)
- [6. PPPoE](#6-pppoe)
  - [6.1. Ressourcen](#61-ressourcen)
  - [6.2. PPPoE-Server](#62-pppoe-server)
- [7. PPPoE-Client](#7-pppoe-client)
- [8. VPN](#8-vpn)
  - [8.1. WireGuard](#81-wireguard)
    - [8.1.1. WireGuard Konfiguration Verwalten mit CLI Tool](#811-wireguard-konfiguration-verwalten-mit-cli-tool)
    - [8.1.2. WireGuard Client verbinden](#812-wireguard-client-verbinden)
  - [8.2. SSTP](#82-sstp)
    - [8.2.1. Key Points zu SSTP](#821-key-points-zu-sstp)
- [9. The Dude](#9-the-dude)
- [10. Torch](#10-torch)
- [11. Traceroute](#11-traceroute)
- [12. SNMP](#12-snmp)
- [13. Graphing](#13-graphing)

# 1. Vorkenntnisse
 - Alle in Labor 1, 2 und 3 vermittelten Kenntnisse

# 2. Voraussetzungen
 - GNS3 Labor (TBZ Cloud GNS3 Instanz)
 - WinBox um auf RouterOS zuzugreifen

# 3. Lernziele
 - Kennt IP Pools und kann IP-Pools konfigurieren
 - Kennt PPPoE Tunnels und kann einen solchen zwischen zwei MikroTik Routern konfigurieren
 - Kennt die wichtigsten Eigenschaften der VPN Protokolle PPTP, SSTP und WireGuard
 - Kann mit einer Skriptsprache via REST API Änderungen auf RouterOS vornehmen
 - Kennt Netwatch und kann es verwenden um Hosts zu überwachen

# 4. Vorbereitung

**Challenge 1:** In GNS3 ist folgendes Laborsetup aufzubauen (nachfolgende Hinweise beachten):

![Laborsetup](media/Laborsetup.PNG)
<br>*Abbildung 1: Übersicht Laborsetup im finalen Zustand.*


Hinweise zum Laborsetup:
 - Bei `Management` handelt es sich um das `Device` bzw. Komponente `Cloud`
 - Bei `NAT1` handelt es sich um das `Device` bzw. Komponente `nat`
 - Für `R1` ist die neuste Verfügbare `MikroTik 7.*` Version zu verwenden.
 - Internet `nat` muss umbedingt an Port `ether1` angeschlossen werden!
 - Auf `R1`, `R2`, `VPC1`, `VPC2` muss jeweils die unten aufgeführte Grundkonfiguration vorgenommen werden. 

## 4.1. Grundkonfiguration R1
```
/ip dhcp-client add add-default-route=yes interface=ether2 use-peer-dns=yes
/ip dhcp-client/set ether1 add-default-route=no use-peer-dns=no
/system/identity/set name=R1
/ip firewall nat add action=masquerade chain=srcnat out-interface=ether2
/ip service set telnet disabled=yes
/ip service set ftp disabled=yes
/ip service set www disabled=no
/ip service set api disabled=yes
/ip service set api-ssl disabled=yes
/ip address/add interface=ether8 address=192.168.10.1/24
```

## 4.2. Grundkonfiguration R2
```
/ip dhcp-client/set ether1 add-default-route=no use-peer-dns=no
/system/identity/set name=R2
/ip address/add interface=ether8 address=192.168.20.1/24
```

## 4.3. Grundkonfiguration VPC1
```
ip 192.168.10.10/24 192.168.10.1
```

## 4.4. Grundkonfiguration VPC2
```
ip 192.168.20.10/24 192.168.20.1
```

# 5. Netwatch
[Netwatch](https://help.mikrotik.com/docs/display/ROS/Netwatch) ist ein Feature von Mikrotik, das den Zustand von Hosts im Netzwerk überwacht. Diese Überwachung kann durch Requests über verschiedene Protokolle erfolgen, darunter ICMP, TCP und HTTP. Für jeden Eintrag in der Netwatch-Tabelle kann eine IP-Adresse, ein Ping-Intervall und Skripte festgelegt werden. Ein Hauptvorteil von Netwatch ist seine Fähigkeit, beliebige Konsolenbefehle bei Zustandsänderungen des Hosts auszuführen. Hosts können kontinuierlich überwacht werden, wobei angezeigt wird, wann die Überwachung zuletzt erfolgreich war. Skripte können automatisch ausgeführt werden, wenn sich der Zustand eines Hosts ändert, was eine flexible Reaktion auf Netzwerkereignisse ermöglicht.

![Winbox Netwatch](media/Netwatch.PNG)
<br>*Abbildung 2: WinBox Netwatch mit drei Einträgen*

Bei manuellen Netzwerkkonfigurationen ist es äusserst hilfreich, wenn mit einem Blick überprüft werden kann, ob alle Hosts erreichbar sind. Deshalb ist es sehr empfehlenswert Netwatch als erstes zu Konfigurieren. Damit erspart man sich das wiederholte manuelle Pingen. Im Laborsetup empfiehlt es sich ein kurzes Intervall (10 Sekunden) festzulegen, damit nicht allzu lange gewartet werden muss. In produktiven Umgebungen sind längere Intervalle zu bevorzugen, da die regelmässigen Checks die CPU des Routers und das Netzwerk - wenn auch wenig - belasten. 

**Challenge 1:** Konfiguriere in Router R1 und R2 Netwatch Einträge, welche bei der nachfolgenden Konfiguration des Labors helfen. Folgende Fragestellung unterstützt bei der Suche: Welche Hosts müssen bei Abschluss des Labors alle erreichbar sein?

**Überlegung:** Wird ein Netzwerk manuell Konfiguriert (bzw. wenn kein automatisches Testing vorhanden ist) wird der Netzwerk Administrator bzw. die Person, welche das Netzwerk konfiguriert immer wieder verschiedene Hosts anpingen, um zu testen, ob Hosts erreichbar sind. Anstatt diese Pings manuell in einzelnen Konsolenfenstern durchzuführen, kann Netwatch zur Unterstützung verwendet werden. Nachdem die IP-Planung abgeschlossen wurde, sind alle IP-Adressen bekannt, die anschliessend im Netzwerk konfiguriert werden müssen. Alle diese Hosts können in Netwatch hinterlegt werden. Egal ob das Netzwerk nun automatisch Ansible konfiguriert wird oder manuell: In Netwatch kann sich er Netzwerkadministrator nun schnell einen Überblich verschaffen, welche Hosts erreichbar sind. Ein weiterer Use Case sind kleine Netzwerke ohne aktive Monitoring-Software: Mit Netwatch kann zumindest überprüft werden, ob die Hosts erreichbar sind und sogar einfache HTTP basierte Services können automatisch überwacht werden. 

# 6. PPPoE
[Point to Point over Ethernet (PPPoE)](https://help.mikrotik.com/docs/display/ROS/PPPoE) ist einfach eine Methode, PPP-Pakete in Ethernet-Frames einzukapseln. PPPoE ist eine Erweiterung des standardmäßigen Point to Point Protocol (PPP) und der Nachfolger von PPPoA. Der PPPoE-Standard ist in [RFC 2516](https://www.rfc-editor.org/rfc/rfc2516) definiert. Der PPPoE-Client und -Server funktionieren über jede Layer2-Ethernet-Level-Schnittstelle auf dem Router, zum Beispiel Wireless, Ethernet, EoIP usw. Allgemein gesprochen, wird PPPoE verwendet, um IP-Adressen an Clients basierend auf Authentifizierung durch Benutzernamen (und falls erforderlich, durch Arbeitsstation) zu vergeben, im Gegensatz zur Authentifizierung nur durch Arbeitsstation, wo statische IP-Adressen oder DHCP verwendet werden. Es wird empfohlen, keine statischen IP-Adressen oder DHCP auf den gleichen Schnittstellen wie PPPoE zu verwenden.

RouterOS unterstützt die Funktionen *PPPoE Client* und *PPPoE Server*.

Häufig wird PPPoE von [ISP](https://en.wikipedia.org/wiki/Internet_service_provider) verwendet, um ihre Endkunden z.B. über VDSL aufzuschalten. Ein grosser Vorteil beim Einsatz von PPPoE anstatt eines herkömmlichen Subnetzes mit DHCP-Server: Der ISP Provider kann mindestens 2 IPv4-Adressen einsparen (Einsparung hängt von der grösse der verwendeten Subnets ab. Je kleiner die Subnets, desto grösser die Einsparung).

In diesem Abschnitt werden wir den Router *R2* über PPPoE mit Router *R1* verbinden. Die entsprechende physische Layer2 Verbindung liegt bereits vor (Jeweils ether5 von beiden Router).

## 6.1. Ressourcen
 - Dokumentation: [MikroTik Help - PPPoE](https://help.mikrotik.com/docs/display/ROS/PPPoE)
 - Video: [MikroTik MTCNA - PPP & PPPoE - The Network Berg](https://www.youtube.com/watch?v=HU0szWPK8Xg)

## 6.2. PPPoE-Server
Bevor der PPPoE-Server konfiguriert werden kann, müssen folgende Voraussetzungen erfüllt sein:
 - IP Pool: Aus dem Pool vergibt der PPPoE Server seinen Clients IP-Adressen
 - PPP Profile: In diesem werden diverse PPP relevante Parameter konfiguriert, unter anderem die IP-Konfiguration. 
 - Secrets: Zugangsdaten der "Benutzer" bzw. Clients

**Challenge 2:** Konfiguriere den PPPoE-Server auf Router *R1*. Nutze die Informationen aus *Abbildung* 3 bis 6 als Unterstützung. 

![IP-Pool](media/IPPool.PNG)
<br>*Abbildung 3: IPPool Konfiguration*

![PPP Profile](media/pppprofil.PNG)
<br>*Abbildung 4: PPP Profil, Detailansicht von Profil pppoe*

![PPP Secret](media/pppsecret.PNG)
<br>*Abbildung 5: PPP Secret Detailansicht von user1*

![PPPoE Service](media/pppoeservice.PNG)
<br>*Abbildung 6: PPPoE Service*

# 7. PPPoE-Client
Der PPPoE-Client wird als virtuelles Interface konfiguriert. Im Menü *Interfaces* kann mit dem Plus-Symbol ein PPPoE-Client hinzugefügt werden. 

![PPPoE-Client hinzufügen](media/addpppoeclient.PNG)
<br>*Abbildung 7: Kontextmenü des Plus-Buttons im Fenster interfaces*

**Challenge 3:** Füge auf Router *R2* einen PPPoE-Client hinzu. In den dazugehörigen Einstellungen muss lediglich *Interfaces*, *User* und *Passwort* konfiguriert werden. Zudem empfiehlt es sich den Haken bei *Use Peer DNS* zu setzen, damit der *R2* auch einen gültigen DNS Server hat. Mithilfe der in *Abbildung 8* abgebildeten Methoden kann überprüft werden, ob die PPPoE-Client Verbindung erfolgreich aufgebaut wurde. 

![PPPoE-Client Verbindung überprüfen](media/verifypppoe.PNG)
<br>*Abbildung 8: Vier Methoden um den Zustand der PPPoE-Verbindung auf Clientseite zu überprüfen.*

**Challenge 4:** Wird nun versucht von VPC2 ein Host im Internet anzupingen, wird dies fehlschlagen. Eine Konfiguration fehlt noch. Erinnern wir uns an Labor 2. Aufgabe: Füge eine weitere Konfigurationsregel an der richtigen Stelle hinzu, sodass der PC2 Hosts im Internet anpingen kann. 


![Wireshark PPPoE ICMP](media/wiresharkether5.PNG)
<br>*Abbildung 9: Ein Wireshark-Trace auf der Verbindung zwischen R1 und R2. Ein in PPP eingepacktes ICMP Paket ist ausgewählt und die PPPoE und PPP Informationen in der Detailsansicht ausgeklappt.*

In *Packet-Captures* auf der Leitung zwischen den beiden Routern eines ICMP Pings von *PC2* zu einem Host im Internet, kann beobachtet werden, die der Ping in PPPoE und PPP *encapsulated* ist (Siehe *Abbildung 9*). 

# 8. VPN
MikroTik unterstützt eine Vielzahl von VPN Protokollen (Liste nicht abschliessend):
 - [PPTP](https://help.mikrotik.com/docs/display/ROS/PPTP) - Veraltet, unsicher
 - [SSTP](https://help.mikrotik.com/docs/display/ROS/SSTP)
 - [Wireguard](https://help.mikrotik.com/docs/display/ROS/WireGuard)
 - [OpenVPN](https://help.mikrotik.com/docs/display/ROS/OpenVPN)
 - [IPsec](https://help.mikrotik.com/docs/display/ROS/IPsec)

## 8.1. WireGuard
[WireGuard](https://www.wireguard.com/) gilt aktuell als einer der sichersten, schnellsten und modernsten VPN-Technologien. Seit RouterOS 7.x unterstützt RouterOS WireGuard. 

Die Konfiguration von WireGuard auf RouterOS ist äusserst simpel:
 1. IPv4 Subnetz für WireGuard festlegen. 
 1. WireGuard Interface anlegen im WireGuard Menü.
 2. WireGuard Peers anlegen

Das aufwändigste Arbeit ist es die Konfigurationsdateien für Laptops und Mobilgeräte zu erstellen. Doch das Hinzufügen von neuen Peers kann einfach mit Python oder anderen Tools automatisiert werden. 

**Ziel:** Das Ziel von Challenge 5 und 6 ist es sich mit dem eigenen Laptop mit WireGuard auf den Router *R1* zu verbinden. 

### 8.1.1. WireGuard Konfiguration Verwalten mit CLI Tool
`wg-quick` sowie die WireGuard-GUI verwenden dieselben Konfigurationsdateien. Diese beinhalten die Verbindungs- und Routinginformationen, sowie die Credentials. Für die Erstellung und die Verwaltung empfiehlt es sich ein Tool zu verwenden. Auf Seiten wie [github.com/cedrickchee/awesome-wireguard](https://github.com/cedrickchee/awesome-wireguard) finden sich viele nützliche Tool hierfür. 

Ein nützliches Tool ist [wgcfghelp](https://github.com/muqiuq/wgcfghelp). Dieses generiert nebst den Konfigurationsdateien auch die QR-Codes, welche mit dem WireGuard Android/iOS-App eingelesen werden können. Weiter generiert es MikroTik Befehle mit denen der MikroTik Router konfiguriert werden kann. 

![Wireguard Tool](media/WireguardTool.webm)
<br>*Video 1: Erstellen einer WireGuard RoadWarrior Konfigurationsdatei, WireGuard Konfiguration von MikroTik RouterOS*

**Challenge 5:** Schaue das [Video](media/WireguardTool.webm) an, generiere 5 WireGaurd Client Konfigurationsdateien und konfiguriere *R1*. 

### 8.1.2. WireGuard Client verbinden

**Challenge 6:** Verwende eines der Konfigurationsdateien um dich mit deinem Laptop auf den Router zu verbinden. Verwende *Netwatch*, um zu überprüfen, ob die VPN-Verbindung geklappt hat. (Funktioniert nur, wenn eine OpenVPN Verbindung ins Labor aufgebaut wurde.)

Ob eine WireGuard-Verbindung geklappt hat, kann unter anderem daran erkannt werden, dass Rx und Tx beide kontinuierlich ansteigen. 

![WireGuard Peer](media/wireguard.PNG)
<br>*Abbildung 10: WinBox Wireguard Peer*

## 8.2. SSTP
Das "Secure Socket Tunneling Protocol" (SSTP) wurde ursprünglich von Microsoft entwickelt. SSTP überträgt einen PPP-Tunnel über einen TLS-Kanal, was es ermöglicht, fast alle Firewalls und Proxy-Server zu durchqueren, da es den TCP-Port 443 verwendet. SSTP, eine Form von VPN-Tunnel, nutzt SSL/TLS für sichere Schlüsselverhandlungen, Verschlüsselung und Integritätsprüfung des Datenverkehrs. Durch die Verwendung von SSL/TLS über TCP-Port 443 kann SSTP nahezu alle Firewalls und Proxy-Server passieren, mit Ausnahme von authentifizierten Web-Proxys.

 - Dokumentation [MikroTik Help SSTP](https://help.mikrotik.com/docs/display/ROS/SSTP)
 - Anleitung zu SSTP [MikroTik MTCNA - SSTP - The Network Berg](https://www.youtube.com/watch?v=eFMSTwUMpxY)

### 8.2.1. Key Points zu SSTP
 - Verwendet Port 443/TCP
 - OpenSource Implementation für Linux (Client und Server) vorhanden
 - Da SSTP identisch zu HTTPs Datenverkehr ist, kann SSTP normalerweise problem durch die Firewall gelangen ohne spezielle Konfiguration
 - Bei SSTP Verbindungen zwischen RouterOS Geräten wird kein SSL Zertifikat benötigt
 - Bei einer Verbindung eines Windows Clients muss entweder (A) ein gültiges signiertes Zertifikat in RouterOS eingespielt werden oder (B) der Public Key des CA Zertifikates im Windows Zertifikat abgelegt werden (Windows muss dem Zertifikat vertrauen).


# 9. The Dude
"The Dude network monitor is a new application by MikroTik which can dramatically improve the way you manage your network environment. It will automatically scan all devices within specified subnets, draw and layout a map of your networks, monitor services of your devices and alert you in case some service has problems." (Quelle: [MikroTik - The Dude](https://mikrotik.com/thedude)). 

 - Video: [Mikrotik MTCNA - The Bude - The Network Berg](https://www.youtube.com/watch?v=qfLTKfWrnd0)
 - Anleitung: [MikroTik Wiki- The Dude](https://wiki.mikrotik.com/wiki/Manual:The_Dude)

# 10. Torch
MikroTik Torch ist ein Echtzeit-Datenverkehrüberwachungstool, welches verwendet werden kann, um den Datenverkehr durch eine Schnittstelle zu überwachen. 

 - Video: [MikroTik MTCNA - Basic Troubleshooting - The Network Berg](https://www.youtube.com/watch?v=PRy-5-gF94E)
 - Video: [MikroTik Torch tool](https://www.youtube.com/watch?v=45E2uwI3xhc)
 - Dokumentation: [MikroTik Help - Torch](https://help.mikrotik.com/docs/display/ROS/Torch)

# 11. Traceroute
Traceroute zeigt die Liste der Router an, durch die ein Paket reist, um einen entfernten Host zu erreichen. Das **traceroute** oder **tracepath**-Tool ist auf praktisch allen Unix-ähnlichen Betriebssystemen verfügbar, und **tracert** auf Microsoft Windows-Betriebssystemen.

Die Funktionsweise von Traceroute basiert auf dem TTL-Wert und der ICMP-Nachricht „Time Exceeded“. Der TTL-Wert im IP-Header wird in erster Linie dafür verwendet, um Routing-Schleifen zu vermeiden. Jeder Hop verringert den TTL-Wert um 1. Wenn der TTL-Wert null erreicht, wird das Paket verworfen, und eine ICMP Time Exceeded-Nachricht wird an den Absender zurückgesendet. Dies muss nicht zwangsläufig erfolgen und ist abhängig davon, ob der Router so konfiguriert ist. 

 - Dokumentation: [MikroTik Help - Traceroute](https://help.mikrotik.com/docs/display/ROS/Traceroute)

![Traceroute](media/Traceroute.PNG)
<br>*Abbildung 11: WinBox Traceroute aktiv mit Reverse-DNS-Auflösung*

# 12. SNMP
Simple Network Management Protocol (SNMP) ist ein Standardprotokoll zur Verwaltung von Geräten in IP-Netzwerken. SNMP kann verwendet werden, um verschiedene Daten auszulesen und anschliessend mit Tools wie PRTG, CACTI, MRTG oder The Dude zu visualisieren.

 - Video: [MikroTips: trigger scripts with SNMP](https://www.youtube.com/watch?v=WW7hjy29eiE)
 - Video: [Python SNMP in MikroTik RouterOS](https://www.youtube.com/watch?v=_jZcNJSDh88)
 - Dokumentation: [MikroTik Help - SNMP](https://help.mikrotik.com/docs/display/ROS/SNMP)

# 13. Graphing
Mit *Graphs* kann RouterOS automatisch Metriken Sammeln und in Graphen auswerten.

 - Video: [Graphing stuff with MikroTik](https://www.youtube.com/watch?v=FTQEnDZVHNc)
 - Dokumentation: [MikroTik Help - Graphing](https://help.mikrotik.com/docs/display/ROS/Graphing)