# Laborübung 1: Mikrotik Grundlagen, Bridging, DHCP und DNS  <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Lernziele](#1-lernziele)
- [2. Hinweise zum Vorgehen](#2-hinweise-zum-vorgehen)
- [3. Weitere Hinweise zur Aufgabe](#3-weitere-hinweise-zur-aufgabe)
- [4. MikroTik Dokumentation](#4-mikrotik-dokumentation)
- [5. Vorbereitung](#5-vorbereitung)
- [6. Konnektivität mit DHCP ins Labor](#6-konnektivität-mit-dhcp-ins-labor)
  - [6.1. Intro](#61-intro)
  - [6.2. Bestehender DHCP-Client](#62-bestehender-dhcp-client)
  - [6.3. Neuer DHCP-Client auf ether2 konfigurieren](#63-neuer-dhcp-client-auf-ether2-konfigurieren)
  - [6.4. Ressourcen DHCP](#64-ressourcen-dhcp)
  - [6.5. WinBox](#65-winbox)
    - [6.5.1. Ressourcen WinBox](#651-ressourcen-winbox)
- [7. Konfigurationsbackup \& RouterOS Update](#7-konfigurationsbackup--routeros-update)
  - [7.1. Konfigurationsbackup](#71-konfigurationsbackup)
    - [7.1.1. Konfigurationsbackup (Binär)](#711-konfigurationsbackup-binär)
    - [7.1.2. Konfigurationsbackup (Plain-text)](#712-konfigurationsbackup-plain-text)
  - [7.2. RouterOS Update](#72-routeros-update)
  - [7.3. RouterOS - Upgrade Path](#73-routeros---upgrade-path)
- [8. Sicherheit - Zugriff auf RouterOS Konfiguration absichern](#8-sicherheit---zugriff-auf-routeros-konfiguration-absichern)
  - [8.1. Anleitung www-ssl aktivieren](#81-anleitung-www-ssl-aktivieren)
  - [8.2. SSH Zugriff mit SSH-Keypair](#82-ssh-zugriff-mit-ssh-keypair)
    - [8.2.1. Vertiefungsfragen](#821-vertiefungsfragen)
    - [8.2.2. Ressourcen SSH](#822-ressourcen-ssh)
- [9. ARP](#9-arp)
- [10. Bridge - Switch](#10-bridge---switch)
- [11. Lokales LAN](#11-lokales-lan)
    - [11.0.1. IPv4-Subnetz und IPv4-Adresse des Routers](#1101-ipv4-subnetz-und-ipv4-adresse-des-routers)
    - [11.0.2. DHCP-Server-Setup](#1102-dhcp-server-setup)
    - [11.0.3. NAT-P - Masquerade](#1103-nat-p---masquerade)
    - [11.0.4. DNS-Server aktivieren](#1104-dns-server-aktivieren)
- [12. 💪 Challenge - DNS-Filtering mit PiHole](#12--challenge---dns-filtering-mit-pihole)
  - [12.1. Statische IP Adresse konfigurieren](#121-statische-ip-adresse-konfigurieren)
  - [12.2. MikroTik Lizenz erweitern](#122-mikrotik-lizenz-erweitern)
  - [12.3. Pi-hole installieren](#123-pi-hole-installieren)
  - [12.4. Korrekter DNS Server im DHCP Server eintragen](#124-korrekter-dns-server-im-dhcp-server-eintragen)
  - [12.5. Vertiefungsfragen](#125-vertiefungsfragen)
  - [12.6. Nützliche Debian-Befehle](#126-nützliche-debian-befehle)
- [13. Pi-hole Ad und Telemetry blockierende Funktionen ausprobieren](#13-pi-hole-ad-und-telemetry-blockierende-funktionen-ausprobieren)
- [14. Vorgehen](#14-vorgehen)
- [15. Weiterführende Ressourcen](#15-weiterführende-ressourcen)
- [16. Autor](#16-autor)
- [17. License](#17-license)

# 1. Lernziele
 - Kennt die Grundlagen von RouterOS und die unterschiedlichen Konfigurationsschnittstellen (CLI, WinBox, Web, REST)
 - Kann auf einem MikroTik Router (RouterOS) Bridging, DHCP und DNS Konfigurieren
 - Kennt den prinzipiellen Unterschied zwischen einer dynamischen und einer statischen Zuweisung von IP Adressen und kann aufzeigen, in welchem Anwendungsfall eine dynamische resp. eine statische Adress-Zuweisung sinnvoll ist.
 - Kennt die Einstellungen zur Konfiguration eines DHCP Servers und kann erläutern, wie diese Einstellgrössen die Zuweisung einer IP Adresse, einer Subnet-Maske und allenfalls Angaben zu DNS-Servern und Standard-Gateways bei der Anfrage eines Clients beeinflussen.
 - Kann in einem Netzwerk einen abgesetzten DNS Server integrieren

# 2. Hinweise zum Vorgehen
In dieser Aufgabe gibt es nur teilweise Schritt-für-Schritt-Anleitungen. Es wird erwartet, dass der/die Studierende fehlende Informationen selbstständig recherchiert. Links zu hilfreichen Ressourcen sind an in den jeweiligen Abschnitten oder am Ende des Dokumentes aufgeführt. 

# 3. Weitere Hinweise zur Aufgabe
 - Login MikroTik: *admin* Passwort: *keines gesetzt* ==> Immer Passwort *admin* setzen!

# 4. MikroTik Dokumentation

Die aktuelle MikroTik Dokumentation findet sich auf [help.mikrotik.com](https://help.mikrotik.com/docs/). Sie beinhaltet nebst *Getting started* Guides auch die Dokumentation zu den einzelnen Features.

Seit etwa 2021 veröffentlicht MikroTik regelmässig Youtube-Videos in Ihrem [Youtube-Kanal MikroTik](https://www.youtube.com/@mikrotik). 

---

# 5. Vorbereitung

In GNS3 ist folgendes Laborsetup in einem Projekt aufzubauen (nachfolgende Hinweise beachten):

![Laborsetup 1](media/Laborsetup1.PNG)

Hinweise zum Laborsetup:
 - Bei `Management` handelt es sich um das `Device` bzw. Komponente `Cloud`
 - Bei `Internet` handelt es sich um das `Device` bzw. Komponente `nat`
 - Für `R1` ist die neuste Verfügbare `MikroTik 7.*` Version zu verwenden.
 - Internet `nat` muss zwingend an Port `ether1` angeschlossen werden!

---


# 6. Konnektivität mit DHCP ins Labor

## 6.1. Intro

![RouterOS Services](media/services.png)

MikroTik bietet eine Vielzahl von Konfigurationsschnittstellen an. Nebst den üblichen wie die Konsole, SSH oder WebGUI, bietet MikroTik das leistungsfähige Tool `WinBox` an, dass MikroTik auf der Webseite wie folgt beschreibt: 

> Winbox is a small utility that allows the administration of MikroTik RouterOS using a fast and simple GUI. It is a native Win32/Win64 binary but can be run on Linux and macOS (OSX) using Wine. All Winbox interface functions are as close as possible mirroring the console functions, that is why there are no Winbox sections in the manual.

Damit der Zugriff via WinBox vom eigenen Notebook aus funktioniert, müssen einige Vorbereitungen getroffen werden. 

Mit dem Befehl `/ip service/print` können wir uns auf dem `R1` einen Überblick über die verfügbaren Services bzw. Konfigurationsschnittstellen schaffen. 

Das *Cloud*-Image von MikroTik (MikroTik CHR) unterscheidet sich nur wenig von der Firmware, die wir auf den Geräten finden. Unter anderem fehlen die hardwarespezifischen Konfigurationen (wie z.B. Features von gewissen Switching-Chips). Ein wesentlicher Unterschied ist auch die Standardkonfiguration. Bei der CHR Version ist grundsätzlich nichts konfiguriert, ausser ein DHCP-Client auf dem dem ersten Port (hier `ether1`). 

## 6.2. Bestehender DHCP-Client

Mit `/export` kann die gesamte Konfiguration des MikroTiks angezeigt werden. Dort wird ersichtlich, dass ein DHCP-Client auf dem Interface `ether1` aktiv ist. 

Mit dem Befehl `/ip dhcp-client print` kann überprüft werden, ob der DHCP-Client vom DHCP-Server eine IP-Adresse zugewiesen bekommen hat. 

![DHCP-Client 1](media/dhcpclient1.PNG)

In diesem Output ist ersichtlich, dass der DHCP-Client eine Default-Route für das erlernte Default-Gateway erstellen und auch der DNS-Server hinterlegt werden soll.

Mit dem Befehl `/ip route print` können wir feststellen, dass eine dynamische Default-Route angelegt wurde. 

![Ip Route 1](media/iproute1.PNG)

*Wissensüberprüfung: Weshalb handelt es sich um eine "dynamische" Route? Kommen dynamische Routen nicht ausschliesslich von Routing-Protokollen?*

Mit `/ping cloud.tbz.ch` kann getestet werden, ob ein Uplinks ins Internet besteht. 

## 6.3. Neuer DHCP-Client auf ether2 konfigurieren

Die GNS3-Labore der TBZ sind so konzipiert (siehe [hier](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3/-/tree/main/02_Bedienungsanleitung)), dass sich der Laptop bzw. PC des Studierenden über ein Layer 2 VPN (Ausnahme macOS bzw. bei Zugriff über WireGuard) verbindet. Über diese Verbindung wird einerseits auf den GNS3-Server zugegriffen, andererseits ermöglicht sie den Zugang zum Labornetzwerk. In diesem Netzwerk (br0, 192.168.23.0/24) läuft auch ein DHCP-Server. 

Damit der Router auch in diesem Netzwerk eine IP-Adresse erhält, muss nun eine weitere DHCP-Client Instanz konfiguriert werden. Im Labornetzwerk (192.168.23.0/24) steht kein Gateway für Internet-Uplink zur Verfügung. 

*Verständnisfrage: Weshalb ist es notwendig IP-Konnektivität mit `Internet` (nat) und eine mit `Management` (Cloud, br0) zu konfigurieren?*

**💪 Challenge 1:** DHCP-Client auf ether2 hinzufügen, sodass für dieses Gateway keine Default-Route angelegt wird. Zur Kontrolle, ob die getätigte Konfiguration stimmt, kann der nachfolgende Output verwendet werden. Zudem kann der Router vom lokalen PC angepingt werden. (Siehe [Anleitung MikroTik - DHCP-Client](https://help.mikrotik.com/docs/display/ROS/DHCP#DHCP-DHCPClient) und [Videoanleitung -  Your first DHCP configuration ](https://www.youtube.com/watch?v=kF4b_t6W5fM)). Ob die Konfiguration korrekt vorgenommen werden, kann mit Befehlen wie in Abbildung 2a überprüft werden. 

![DHCP-Client](media/dhcpclient2.PNG)
<br>*Abbildung 2a: Auf beiden Interfaces hat der Router über DHCP jeweils eine IP-Adresse bezogen, jedoch nur eine Default-Route angelegt.*

## 6.4. Ressourcen DHCP
 - 🎥 [Your first DHCP configuration](https://www.youtube.com/watch?v=kF4b_t6W5fM)
 - 📕 [MikroTik Help - DHCP](https://help.mikrotik.com/docs/display/ROS/DHCP)

## 6.5. WinBox

Da der Router nun im selben Layer 2 Netz ist wie der eigene Laptop bzw. PC, kann nun direkt auf diesen zugegriffen werden. Nun ist man nicht mehr nur auf die CLI für die Konfiguration beschränkt. 

**💪 Challenge 2a:** Nutze die nachfolgende Grafik um dich über Winbox mit dem Router `R1` zu verbinden.

![Challange 2a](media/connectwithwinbox.PNG)
<br>*Abbildung 2b: Auf beiden Interfaces hat der Router über DHCP jeweils eine IP-Adresse bezogen, jedoch nur eine Default-Route angelegt.*

### 6.5.1. Ressourcen WinBox
 - 🛠️ [MikroTik Winbox Windows](https://mikrotik.com/download)
 - 🛠️ [MikroTik Winbox macOS](https://github.com/nrlquaker/winbox-mac)
 - 🛠️ [MikroTik Winbox Linux](https://help.mikrotik.com/docs/display/RKB/Winbox+for+mac)
 - 📕 [MikroTik Help - WinBox](https://help.mikrotik.com/docs/display/ROS/WinBox)
 - 🎥 [How to connect to a MikroTik device](https://www.youtube.com/watch?v=13NvZY7sRlY)
 - 🎥 [How to run Winbox on Linux](https://www.youtube.com/watch?v=2jjtK5Me29I)
 - 🎥 [Winbox on MacOS M1 in two steps](https://www.youtube.com/watch?v=TCPhYh9Wajw)

---


# 7. Konfigurationsbackup & RouterOS Update

RouterOS kann direkt ohne Download von Firmware aktualisiert werden. Es wird empfohlen vor einem Update ein Konfigurationsbackup anzulegen. 

**💪 Challenge 2b:** Erstelle ein Backup deiner Router Konfiguration mithilfe der nachfolgenden Anleitung (Siehe [Configuration Management](https://help.mikrotik.com/docs/display/ROS/Configuration%20Management) für allgemeine Informationen zum Konfigurationsmanagement). 

## 7.1. Konfigurationsbackup

Es gibt zwei verschiedene Arten ein Konfigurationsbackup anzuluegen: 
 - Über die Backup Funktion unter Files => Backup
 - Über die `/export` Funktion

### 7.1.1. Konfigurationsbackup (Binär)
Dieses Backup kann nicht mit einem Texteditor bearbeitet werden und ist nur mit dem entsprechenden Router kompatibel. 

![Konfiguration Backup](media/backup.PNG)
<br>*Abbildung 2c: Das Backup kann mit einem Passwort verschlüsselt werden.*

### 7.1.2. Konfigurationsbackup (Plain-text)
Im Terminal mit dem Befehl `/export file=FILENAME` wobei `FILENAME` mit dem gewünschten Dateinamen ersetzt wird. Die Datei kann anschliessend unter `Files` heruntergeladen werden.

**Wichtig:** Credentials werden nicht mit-exportiert. Wenn zumindest Keys (z.B. WireGuard Keys) mitexportiert werden sollen muss das Argument `show-sensitive` angefügt werden. Beispiel: `/export file=backup_r1_20231128 show-sensitive` 

## 7.2. RouterOS Update

![RouterOS Update](media/upgrade.PNG)
<br>*Abbildung 2d: Update über das Internet direkt in WinBox*

RouterOS kann ohne vorhergehendes Herunterladen der Firmware aktuallisiert werden. Einerseits über die GUI (Abbildung 2d: System => Packages => Check for Updates ==> Download and Install) und andererseits über das Terminal mit `/system/package/update/install` 

*Achtung:* Der Router startet sich nach dem Download automatisch neu!

*Hinweis GNS3:* Nach dem Update kann es sein, dass der Router nicht mehr erreichbar ist. Dafür den Router über das GNS3 Interface Neustarten. 

## 7.3. RouterOS - Upgrade Path

 - RouterOS 6.49.x auf RouterOS 7.12.2 aktualisiert werden. Für das Update von RouterOS 6 auf 7 muss dabei der `Channel` `testing` gewählt werden. Nach erfolgreichem Update auf RouterOS 7 sollte wieder auf den Channel `stable` gewechselt werden. 
![RouterOS 7.13](./media/routeros713.png)
<br>*Abbildung 2e: Hinweis zu RouterOS 7.13 im Changelog https://mikrotik.com/download/changelogs*
 - Ein Update auf RouterOS 7.13 oder höher von einer Version die kleiner als 7.12 ist, muss in zwei Schritten erfolgen (Siehe Abbildung 2e)

---


# 8. Sicherheit - Zugriff auf RouterOS Konfiguration absichern

RouterOS erlaubt in der Standardkonfiguration den Zugriff auch mit unverschlüsselten Protokollen. Es empfiehlt sich den Router abzusichern und nicht benötigte Protokolle zu deaktivieren. 

Für den Zugriff auf das Webinterface via HTTPS (`www-ssl`) ist ein entsprechedes Zertifikat bzw. ein Schlüsselpaar erforderlich. Das kann unter `System` => `Certificates` angelegt werden. 

## 8.1. Anleitung www-ssl aktivieren
 1. Mit WinBox auf den Router `R1` verbinden. 
 2. `System` => `Certificate` auf + drücken
 3. Wie folgt ausfüllen (in der Regel reichen 3650 Tage (= 10 Jahre)):
[new certificate](media/newcertificate.PNG)
 4. `Apply` drücken
 5. Schlüsselpaar generieren: Durch auf `Sign` öffnet sich ein neues Fenster. In diesem Auf `Start drücken`. Sobald bei `Progress` `done` steht ist der Vorgang beendet. 
 6. Alle Fenster innerhalb von WinBox schliessen.
 7. `IP` => `Services` => `www-ssl` unter `Certificate` den Namen des gerade erstellten Zertifikates auswählen. 
 8. `Apply` und anschliessend auf `Enable`

**💪 Challenge 3a:** Ziel ist es alle Services zu deaktivieren, die eine unverschlüsselte Kommunikation verwenden (Siehe Abbildung 2e). 

![RouterOS Services](media/secureservices.PNG)
<br>*Abbildung 2f: Nur die benötigten Services sind noch aktiv*

## 8.2. SSH Zugriff mit SSH-Keypair

MikroTik unterstützt wie OpenSSH die Möglichkeit den Zugriff auf SSH auf die Authentifizierungsmethode publickey bzw. mit einem SSH-Schlüsselpaar einzuschränken. 

![RouterOS Services](media/ipsshprint.png)
<br>*Abbildung 2g: ssh konfigurationsparameter im RouterOS CLI*

Standardmässig ist `always-allow-password-login` auf `no` gesetzt. Das hat zur folge, dass sobald ein SSH Public Key hinterlegt wird, das Login via SSH nur noch via *Public-Key-Authentification* möglich ist. 

**💪 Challenge 3b:** Hinterlege deinen Public-Key im Router `R1` und logge dich anschliessend via SSH von deinem Notebook auf `R1` ein. Prüfe anschliessend, ob das Login mit Passwort noch funktioniert. 

### 8.2.1. Vertiefungsfragen
 - Weshalb kann der Router weiterhin über die Konsole (GNS3 GUI => Router R1 => Console) bedient werden, obwohl der SSH Public Key gesetzt wurde?
 - Inwiefern wird mit *Public-Key-Authentication* die Sicherheit erhöht?

### 8.2.2. Ressourcen SSH
 - 📕 [MikroTik Help - SSH](https://help.mikrotik.com/docs/display/ROS/SSH)

---


# 9. ARP
Das Address Resolution Protocol (ARP) ist ein Netzwerkprotokoll, das zur Auflösung von Netzwerkschichtadressen in Verbindungsschichtadressen verwendet wird, indem es die IP-Adresse eines Clients (Layer 3) dynamisch mit seiner MAC-Adresse (Layer 2) verknüpft. Obwohl ARP üblicherweise dynamisch arbeitet, lässt es sich auch manuell konfigurieren, um bestimmte IP-MAC-Adresszuordnungen festzulegen.

Die aktuelle ARP Tabelle kann unter `IP` => `ARP` eingesehen werden. Weiter können dynamische Einträge in statische umgewandelt und manuell statische Einträge hinzugefügt werden. 

![ARP](media/ARP.PNG)
<br>*Abbildung 2h: ARP*

Unter den einzelnen Interfaces kann konfiguriert werden, ob diese selbst ARP requests für unbekannte IPv4-Adressen durchführen, oder nur auf eigene Antworten sollen. Ein mögliches Netzwerkdesign sieht vor, den DHCP-Server anzuweisen ARP Einträge für Leases anzulegen und auf dem Interface das automatische lernen neuer Einträge zu deaktivieren. **Achtung:** Diese Konfiguration soll nur bei bedarf eingesetzt werden. In Rahmen dieser Laborübung muss dieses Feature nicht zwingend ausprobiert werden. 

![ARP](media/dhcpserverarp.PNG)
<br>*Abbildung 2i: Zwinge den DHCP-Server automatisch ARP für Leases einzutragen und stelle auf dem entsprechenden Interface auf reply-only.*

---


# 10. Bridge - Switch

Bridges und Switches sind Netzwerkgeräte mit unterschiedlichen Funktionen, obwohl sie oft synonym verwendet werden. Bridges waren früher weit verbreitet und dienten dazu, zwei Netzwerksegmente auf der Datenverbindungsebene (Layer 2 des OSI-Modells) zu verbinden. Sie konnten Datenpakete basierend auf MAC-Adressen filtern und weiterleiten, waren aber aufgrund ihrer begrenzten Anzahl an Ports (in der Regel 2) und der Verarbeitungskapazität für kleinere Netzwerke geeignet. In der Linux-Welt steht `bridge` für einen virtuelle Switch. 

Für die standartkonfiguration gilt folgende Regel: In einem Router sind alle Ports "geroutet" und in einem Switch sind alle Ports "geswitcht".

Das bedeutet jedoch nicht, dass ein Gerät, dass primär die Funktion primär eines Routers einnimmt, nicht auch Switching Funktionen vorhanden sind. 

Bei MikroTik Geräten können Bridges angelegt werden. Dabei muss bei der Konfiguration immer darauf geachtet werden, ob die angelegt Bridge in der CPU emuliert oder auf einem Hardware Chip konfiguriert wird. Bei MikroTik ändert sich die Konfigurationsart von Gerät zu Gerät (Siehe: [Bridge Hardware Offloading](https://help.mikrotik.com/docs/display/ROS/Bridging+and+Switching#BridgingandSwitching-BridgeHardwareOffloading)). **Hardware Offloading ist aus Performancegründen immer zu bevorzugen.**

Damit Hardware Offloading funktioniert muss das entsprechende Geräte über entsprechende Hardwarekomponenten verfügen. 

![RBD52G-5HacD2HnD-TC](media/RBD52G-5HacD2HnD-TC_180323.png)
<br>*Abbildung 3: Block Diagram MikroTik hAP ac² RBD52G-5HacD2HnD-TC*

![HardwareOffloading](media/HardwareOffloading.PNG)
<br>*Abbildung 4: Unterschied mit oder ohne Hardware Offloading*

In der Abbildung 4 ist der Unterschied zwischen mit und ohne Hardware Offloading dargestellt. Die Konsequenz von fehlendem Hardware Offloading in Beispiel des hAP ac2: Alle Frames werden über die CPU verarbeitet, was zu einer schlechteren Performance führt, als wenn der Switching Chip die Arbeit übernimmt. 

Bei MikroTik RouterOS, welche in virtuellen Umgebungen ausgeführt werden, kann kein Hardware Offloading konfiguriert werden. Das liegt einerseits an der Virtuallisierung und andererseits am fehlenden Switching-Chip. 

![No Hardware-Offloading](media/HardwareOffloading.PNG)
<br>*Abbildung 5: Ist Hardware-Offloading auf einem Port ausgegraut, konnte es nicht aktiviert werden. Das Switching erfolgt nun über die CPU.*

![Logische Darstellung Bridge](media/logicbridge.PNG)
<br>*Abbildung 6: Logische Darstellung mit und ohne Bridge*

In der Abbildung 6 ist abgebildet, was das hinzufügen einer Bridge in RouterOS auf logischer Ebene bewirkt. Es gilt zu beachten, dass IP-Adressen und IP-Services können nur noch auf dem Bridge Interface konfiguriert werden (D.h. auf den Interfaces `ether3`, `ether4` und `ether5` können keine IP-Adressen mehr konfiguriert werden).

**💪 Challenge 4:** Ziel dieses Challenges ist es eine virtuelle Bridge für das LAN anzulegen und alle noch verfügbaren bzw. nicht konfigurierten Ports daran anzuschliessen (d.h. inklusive dem Port an dem der VPC angeschlossen ist, da wir den auch ins LAN nehmen möchten). Die Konfiguration in Winbox sollte am Schluss wie in Abbildung 7 aussehen. 

![Bridge configuration](media/bridgeconfiguration.PNG)
<br>*Abbildung 7: Oben Bridge, unten Bridge Ports*

---

# 11. Lokales LAN

### 11.0.1. IPv4-Subnetz und IPv4-Adresse des Routers

Ein typisches privates Netzwerk wie es in vielen privaten Haushalten und KMUs vorgefunden wird, besteht aus einem einzigen /24 Subnetz mit DHCP und DNS-Server. 

Dafür muss zuerst ein geeignetes privates Subnetz ausgewählt werden. 

**💪 Challenge 5:** Wähle ein privates IPv4-Subnetz (/24) für dein LAN aus. Bedingung: Das gewählte IPv4-Subnetz darf noch nicht verwendet worden sein. Weiter sind folgende IPv4-Subnetze **nicht** erlaubt: `192.168.0.0/24`, `192.168.1.0/24` und `10.0.0.0/24`.

*Wissensüberprüfung: Weshalb wurden drei Subnetze in Challenge 5 explizit ausgeschlossen?*

**💪 Challenge 6:** Konfiguriere die IPv4-Adresse des Routers aus dem gewählten IPv4-Subnetz und weise diese der neu angelegten Bridge zu. 

### 11.0.2. DHCP-Server-Setup

**💪 Challenge 7:** Konfiguriere mithilfe der Setup-Routine `DHCP-Setup` eine DHCP-Server-Service-Instanz auf der angelegten Bridge. Erfüllte dabei folgende Bedingungen:
 - IPv4 Range `.2` bis `.32` werden für statische IPv4 Adressen benötigt.
 - Als DNS-Server soll der Router `R1` fungieren. 

![DHCP-Server](media/dhcpserversetup.PNG)
<br>*Abbildung 8: DHCP-Server Fenster mit sichtbaren DHCP-Setup Button*

Sofern der DHCP-Server korrekt konfiguriert wurde, sollte der VPC1 per DHCP eine IP-Adresse erhalten. Mit dem Befehl `dhcp` kann in der Konsole des VPC1 dieser dazu bewegt werden eine IPv4-Adresse zu beziehen. 

![VPC DHCP Client](media/vpcdhcpclient.PNG)
<br>*Abbildung 9: VPC bezieht erfolgreiche eine IPv4-Adresse *

**💪 Challenge 8:** Zu jedem DHCP-Server gehört auch die Funktion dazu, die Leases zu einzusehen, zu verwalten und insbesondere statische Leases zu hinterlegen. Mithilfe von statischen Leases kann sichergestellt werden, dass ein Host vom DHCP-Server immer diesselbe IPv4-Adresse bekommt. Ziel dieses Challenges ist es für den VPC1 ein statischer Lease zu hinterlegen. 

*Wissensüberprüfung: Weshalb sollte die Client-ID in einem statischen DHCP-Lease nicht gesetzt werden?*

### 11.0.3. NAT-P - Masquerade

Da das private Subnetz den Routern im WAN nicht bekannt sind, müssen wir auf unserem Router `R1` noch das NAT aktivieren. Sonst können unsere Hosts in unserem LAN nicht auf Netzwerke ausserhalb von (bzw. das Internet) zugreifen. Mehr zum Thema NAT wird in einer späteren Übung vermittelt. 

Um das NAT zu Konfigurieren, soll folgender Befehl im Terminal ausgeführt werden: 
```RouterOs
 /ip firewall/nat/add action=masquerade out-interface=ether1 chain=srcnat
```

![Masquerade for ether1](media/masquerade.PNG)
<br>*Abbildung 9: Terminals können direkt in der WinBox geöffnet werden.*

### 11.0.4. DNS-Server aktivieren

Der DNS-Server von RouterOS ist in der CHR Version standardmässig deaktiviert. In der Standardkonfiguration eines als Heim- oder KMU-Router ausgelegten Gerätes ist dieser standardmässig aktiv. 

**💪 Challenge 9:** Erlaubte "remote requests" für den DNS-Server auf dem Router `R1`. Ob das erfolgreich war, kann mithilfe des VPC1 überprüft werden (Siehe Abbildung 10).

![DNS ping vpc1](media/pingdnsvpc1.PNG)
<br>*Abbildung 10: ping mit DNS auf cloud.tbz.ch funktioniert*


---


# 12. 💪 Challenge - DNS-Filtering mit PiHole

Ein sichereres Netzwerk erfordert oft mehr als einen Standard-Router. Die Methoden zur Netzwerksicherung sind vielfältig, von einer grundlegenden Firewall bis hin zu Intrusion Detection Systemen, von Proxies bis zu Content-Filtern. Eine effektive Massnahme zur Netzwerksicherheit ist das Blockieren bekannter gefährlicher DNS-Einträge. Zusätzlich zielen wir darauf ab, nicht nur gefährliche DNS-Einträge, sondern auch Tracker und Werbung zu blockieren.

Im nachfolgenden Challenge erweitern wir unser Labornetzwerk mit einem Pi-Hole DNS Server. 

![PiHole](media/Debian_GNS3_Menu.PNG)
<br>*Abbildung 11: Screenshot GNS3 device menu: Von hier kann eine Debian VM in das Projekt gezogen werden.*

 - Neues Gerät des Typs "Debian 11.x" hinzufügen, mit R1 verbinden, starten und Konsole öffnen
 - Standard Logindaten: `debian` Passwort: `debian`

**Mehr zur Linux CLI/Shell lernen**
Keine Erfahrungen mit Linux CLI oder Shells? Kein Problem! Im GNS3 Labor können Sie sorgenfrei herumspielen und ausprobieren! Wenn eine Maschine kaputt geht: Kein Problem! Einfach herauslöschen und neu einfügen (Tipp: Führe ein Protokoll mit den ausgeführten Befehlen und Links). 

Weiter stehen im Internet zahlreiche tolle Kurse und Tutorial kostenfrei zur Verfügung:

 - https://missing.csail.mit.edu/
 - https://ryanstutorials.net/linuxtutorial/
 - http://www.ee.surrey.ac.uk/Teaching/Unix/
 - https://miteshshah.github.io/linux/basics/linux-ideas-and-history/
 - https://www.learnenough.com/command-line-tutorial/basics


## 12.1. Statische IP Adresse konfigurieren
Es gilt als *common practice* dem DNS Server eine statische IP-Adresse zuzuordnen. Der DNS Server ist eine fundamentale Komponente jedes Netzwerkes und soll auch beim Ausfall des DHCP Servers erreichbar sein. 

Weitere Informationen zur Konfiguration von IP-Adressen mit ifupdown: https://wiki.debian.org/NetworkConfiguration

 - `sudo apt autoremove cloud-init` Cloud-Init entfernen, damit dieses nicht mit der IP-Adressen konfiguration interagiert
 - Ins Verzeich der Netzwerkkonfigurationen wechseln `cd /etc/network/interfaces.d/`
 - Von cloud-init erstellte konfiguration wird als Vorlage verwendet. Um Verwechslungen zu vermeiden benennen wir die Datei um: `sudo mv 50-cloud-init default`
 - Datei bearbeiten: `sudo nano default`:
   - ersetzen Sie in der Zeile `iface ensXY inet dhcp` *dhcp* mit `static`
   - Ergänzen Sie nach der Zeile `iface ensXY inet static` anschliessend die statische IP Konfiguration. Zum Beispiel für das Subnet: 192.168.11.0/24 IP: 192.168.11.10 Maske: 255.255.255.0 Gateway: 192.168.11.1. DNS Nameserver: 192.168.11.1 <br>Die dafür benötiten *statements* finden Sie im oben aufgeführten Link. 
 - Debian neustarten, um die IP Konfiguration anzuwenden. 
 - Testen: Erfolgreiches Anwenden der Konfiguration prüfen und ping test durchführen.


## 12.2. MikroTik Lizenz erweitern

Das MikroTik CHR Image hat ein anderes Lizenzmodell als die das Image, dass auf MikroTik Hardware ausgeliefert wird. Ohne Lizenz hat das CHR Image ein Speedlimit von 1 Mbit/s (Vergleiche [CHR Licensing auf MikroTik Help](https://help.mikrotik.com/docs/pages/viewpage.action?pageId=18350234#CloudHostedRouter,CHR-CHRLicensing)).

Damit die Pi-hole Installation, welche alle ihre *Depedencies* von im Internet gelegenen Repositories herunterlädt, nicht ewigs dauert, empfiehlt es sich deshalb ein MikroTik Account anzulegen und anschliessend auf dem Router `R1` eine kostenlose Testlizenz zu aktivieren. 

Falls noch nicht vorhanden, dann ein MikroTik Account [hier](https://mikrotik.com/client/) angelegt werden.

Die nachfolgende Methode zur MikroTik Lizenz Erweiterung bzw. Aktivierung erfordert IP-Konnektivität ins Internet. Diese haben wir ganz zu Beginn dieser Laborübung bereits hergestellt. 

**Sub-Challenge:** Aktiviere mithilfe der Abbildung 12 eine Testlizenz auf Router `R1`. 

![Testlizenz](media/testlizenz.PNG)
<br>*Abbildung 12: Über System => License => Renew License kann eine zeitlich beschränkte Testlizenz oder auch eine bereits bezahlte zeitlich unbeschränkte Lizenz aktiviert werden.* 

## 12.3. Pi-hole installieren
Pi-hole Dokumentation: https://docs.pi-hole.net/

 - Installation gemäss https://github.com/pi-hole/pi-hole#one-step-automated-install durchführen.
 - *Hinweise zur Installation:* 
   - Passwort merken (wird am Ende des Installation-Dialoges angezeigt)
   - Alle Installationsparameter auf Standard lassen
 - DNSSEC aktivieren

## 12.4. Korrekter DNS Server im DHCP Server eintragen
Wenn wir im VPCS die IP-Konfiguration prüfen, stellen wir fest, dass der DNS Server immer noch auf den Router zeigt.

*Frage*: Welches Gerät ist im Netzwerk der DHCP-Server? Wo muss der DNS-Server angepasst werden, sodass alle Client Hosts im Netzwerk den richtigen DNS Server ansprechen?

 - Identifizieren Sie den DHCP-Server
 - Loggen Sie sich auf dem entsprechenden Gerät ein. 
 - Passen Sie die Konfiguration an. 
 - Testen Sie die Konifgurationsänderung

<details>
<summary>Lösung</summary>
Auf *R1* DNS Nameserver in der DHCP-Server-Konfiguration auf Pi-hole IP-Adresse ändern
</details>
<br/>

Nach erfolgter Konfiguration: 
 - VPC1 neustarten
 - mit VPC1 DNS Anfrage auf isc.org absetzen. 
 - Verifizieren Sie mit geeigneten Mittel folgendes und dokumentiere die Ergebnisse:
   - VPC macht DNS Anfrage über Pi-hole
   - Die weitergeleiteten Pi-hole Anfragen verwenden DNSSEC (Wireshark Dump)
   - Pi-hole loggt/protokolliert DNS Anfragen.



 


## 12.5. Vertiefungsfragen
 - Wie teilt der Client dem Server mit, dass er DNSSEC verwenden möchte?
 - Wie unterscheidet sich eine Query Response mit DNSSEC gegenüber einer ohne?

## 12.6. Nützliche Debian-Befehle
 - `ip address`: Konfigurierte IP-Adressen pro *interface* anzeigen
 - `ip route`: Konfigurierte Routen 

---

# 13. Pi-hole Ad und Telemetry blockierende Funktionen ausprobieren
Es gibt fast keine Hersteller mehr, die keine Telemetry auf den Benutzergeräten sammeln. Die kommerziellen Betriebssysteme wie Windows, macOS, Android, iOS sammeln fleissig Daten und schicken diese "nach Hause": Angeblich um die "Produkte zu verbessern".
Ob das Fluch oder Segen ist, sei für den Moment dahingestellt. Eines ist jedoch sicher: Die Übermittlung dieser Daten frisst Bandbreite und Energie. 

Eines der Hauptfunktionen von pi-hole ist "Block in-app advertisements" und "Improve network performance". 

Ziel dieses Abschnittes ist das auszuprobieren. 

# 14. Vorgehen
 - UbuntuDesktopGuest einfügen und mit R1 verbinden
 - Login Passwort: `osboxes.org`, 
 - Beliebige Webseite besuchen (,die sicher Werbung und/oder Tracking enthält)
 - Pi-hole Query Log anschauen. Was für Domains werden geblockt?


# 15. Weiterführende Ressourcen
 - MikroTik CLI: https://help.mikrotik.com/docs/display/ROS/Command+Line+Interface
 - MikroTik DHCP Server Konfigurationsbeispiel: https://help.mikrotik.com/docs/display/ROS/DHCP#DHCP-ConfigurationExamples.2
 - MikroTik Befehle Cheat-Sheet: http://www.mkesolutions.net/pdf/routeros-cheat-sheet-v1.1.pdf
 - 🎥 *Video* MikroTik CLI Einführung: https://www.youtube.com/watch?v=EYCjuvTd3dY
 - 🎥 *Video* Mikrotik Your first DHCP configuration: https://www.youtube.com/watch?v=kF4b_t6W5fM


# 16. Autor

Philipp Albrecht <philipp.albrecht@tbz.ch> - https://github.com/alptbz


# 17. License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a> and [GNU GENERAL PUBLIC LICENSE version 3](https://www.gnu.org/licenses/gpl-3.0.en.html). If there are any contradictions between the two licenses, the Attribution-NonCommercial-ShareAlike 4.0 International license governs. 