# Unterrichtsressourcen 

| Modul | Semester | Inhalte                                                                                                 |
|-------|----------|---------------------------------------------------------------------------------------------------------|
| [NWA 1](./NWA1_MTCNA/) | 1        | MTCNA, MikroTik, DHCP, Bridging, IPv4-Routing, Wireless, IPv4-Firewall, NAT, QoS, PPP, PPPoE, WireGuard |
| [NWA 2](./NWA2/) | 2        | Dynamische Routing Protokolle, OSPF, IPv6                                                               |
| [NWA 3](./NWA3/) | 3        | VRRP, Classic Campus Design, VPN                                                                        |

