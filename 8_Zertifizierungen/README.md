# Zertifizierungen

Da der Kursinhalt sich den den nachfolgenden Zertifizierungen orientiert, bietet es sich an diese Prüfungen zu absolvieren. 

 - [Cisco CCNA](https://learningnetwork.cisco.com/s/ccna-exam-topics)
 - [MikroTik MTCNA](https://i.mt.lv/cdn/training_pdf/mtcna_outline_2019181161836.pdf)
